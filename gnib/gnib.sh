#!/bin/bash

############
## REPORT ##
############
REPORT="logs/php.txt"
TARGET="./php*"

## Get source directories
FIND_DIRS=$(find $TARGET -maxdepth 0 -type d | sort -d)
DIRS=$FIND_DIRS

for dir in $DIRS; do
    echo "Analyzing $dir ..."

    ## Structural Debt
    SD1=$(find $dir -type f -name \*.c | wc -l)
    SD1_R=$SD1
    SD2=$(find $dir -type f -name \*.h | wc -l)
    SD2_R=$SD2
    SD3=$(find $dir -type f -name \*.cpp | wc -l)
    SD3_R=$SD3
    SD4=$(find $dir -type d | wc -l)
    SD4_R=$SD4

    ## Dependency Debt
    DD1=$(find $dir -type f -name \*.c | xargs grep "# include " | wc -l)
    DD1_R=$DD1
    DD2=$(find $dir -type f -name \*.h | xargs grep "# include " | wc -l)
    DD2_R=$DD2

    ## Add to the report
    if [ "$dir" == "./php" ]; then
        SIZE=$(ls -s ./php-108.tar.gz)
    else
        SIZE=$(ls -s "$dir".tar.gz)
    fi 
    
    if [ "$SIZE" -eq 0 ]; then
        SIZE=$(ls -s "$dir".tar.bz2)
    fi    
    SIZE_R=$SIZE

    TRUE_SIZE=$(echo $SIZE | cut -d ' ' -f 1)
    TRUE_SIZE_R=$TRUE_SIZE
    ARTIFACT=$(echo $SIZE | cut -d ' ' -f 2)
    ARTIFACT_R=$ARTIFACT

    echo "$ARTIFACT_R" >> $REPORT
    echo "====================" >> $REPORT
    echo "SIZE        C CODE        HEADER        CPP CODE        DIRS        C INCLUDES        H INCLUDES" >> $REPORT
    echo "$TRUE_SIZE_R        $SD1_R        $SD2_R        $SD3_R        $SD4_R        $DD1_R        $DD2_R" >> $REPORT
    echo "" >> $REPORT
    echo "" >> $REPORT
done
