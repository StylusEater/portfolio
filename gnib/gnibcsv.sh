#!/bin/bash

## NOTES
## 1) Had to unpack php-4.2.0RC3.tar.gz by hand

############
## REPORT ##
############
REPORT="logs/gnib.csv"
TARGET="./php*"

## Clear report
echo -n "" > $REPORT

## Print heading
echo "ARTIFACT,SIZE,C CODE FILES,HEADER FILES,CPP CODE FILES,DIRECTORIES,C INCLUDE FILES,H INCLUDE FILES" >> $REPORT

## Get source directories
FIND_DIRS=$(find $TARGET -maxdepth 0 -type d | sort -d)
DIRS=$FIND_DIRS

for dir in $DIRS; do
    old_dir=$dir
    dir=`echo $dir | tr \_ \.`
    echo "Analyzing $dir ..."

    ## Structural Debt
    SD1=$(find $dir -type f -name \*.c | wc -l)
    SD1_R=$SD1
    SD2=$(find $dir -type f -name \*.h | wc -l)
    SD2_R=$SD2
    SD3=$(find $dir -type f -name \*.cpp | wc -l)
    SD3_R=$SD3
    SD4=$(find $dir -type d | wc -l)
    SD4_R=$SD4

    ## Dependency Debt
    DD1=$(find $dir -type f -name \*.c | xargs grep "# include " | wc -l)
    DD1_R=$DD1
    DD2=$(find $dir -type f -name \*.h | xargs grep "# include " | wc -l)
    DD2_R=$DD2

    ## Add to the report
    if [ "$dir" == "./php" ]; then
        SIZE=$(ls -s ./php-108.tar.gz)
    else
        SIZE=$(ls -s "$dir".tar.gz)
        if [ "$?" -ne "0" ]; then
            SIZE=$(ls -s "$dir".tar.bz2)
        fi
    fi 

    SIZE_R=$SIZE
    TRUE_SIZE=$(echo $SIZE | cut -d ' ' -f 1)
    TRUE_SIZE_R=$TRUE_SIZE
    ARTIFACT=$(echo $SIZE | cut -d ' ' -f 2)
    ARTIFACT_R=$ARTIFACT

    echo $ARTIFACT" is "$TRUE_SIZE_R" kB"
     
    if [ "$ARTIFACT_R" != "" ]; then
        echo "$ARTIFACT_R,$TRUE_SIZE_R,$SD1_R,$SD2_R,$SD3_R,$SD4_R,$DD1_R,$DD2_R" >> $REPORT
    fi

    ## Generate function data
    FUNCTION_FILE="logs/"$dir".functions"
    echo "ctags --languages=c,c++ --sort=yes --recurse=yes -x $old_dir"
    dir=$old_dir"/"
    FUNCTION_INFO=$(ctags --languages=c,c++ --sort=yes --recurse=yes -x $old_dir)
    echo -e "$FUNCTION_INFO" > $FUNCTION_FILE
 
done
