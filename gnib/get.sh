#!/bin/bash
BASE="http://museum.php.net/php"
FALSE=0

#while [ $FALSE -gt 0 ]; do

## One
wget $BASE"1/php-108.tar.gz"
tar -xzf php-108.tar.gz

## Two
wget $BASE"2/php-1.99s.tar.gz"
tar -xzf php-1.99s.tar.gz
wget $BASE"2/php-2.0.1.tar.gz"
tar -xzf php-2.0.1.tar.gz
wget $BASE"2/php-2.0.tar.gz"
tar -xzf php-2.0.tar.gz
wget $BASE"2/php-2.0b10.tar.gz"
tar -xzf php-2.0b10.tar.gz
wget $BASE"2/php-2.0b11.tar.gz"
tar -xzf php-2.0b11.tar.gz
wget $BASE"2/php-2.0b12.tar.gz"
tar -xzf php-2.0b12.tar.gz
wget $BASE"2/php-2.0b5.tar.gz"
tar -xzf php-2.0b5.tar.gz
wget $BASE"2/php-2.0b6.tar.gz"
tar -xzf php-2.0b6.tar.gz
wget $BASE"2/php-2.0b7.tar.gz"
tar -xzf php-2.0b7.tar.gz

## Three
wget $BASE"3/php-3.0.tar.gz"
tar -xzf php-3.0.tar.gz
wget $BASE"3/php-3.0b4.tar.gz"
tar -xzf php-3.0b4.tar.gz
wget $BASE"3/php-3.0b6.tar.gz"
tar -xzf php-3.0b6.tar.gz

THREES="1 3 4 5 6 7 9 10 11 12 13 14 15 16 17 17RC1 17RC2 18 18RC1"
for each in $THREES; do
    wget $BASE"3/php-3.0."$each".tar.gz"
    tar -xzf "php-3.0."$each".tar.gz"
done

FOUR_ZERO="0 0RC1 0RC2 0RC3 0RC4 0RC5 1 1RC 1RC2 1pl2 2 3 3RC1 3pl1 4 4RC3 4RC4 4RC5 4RC6 5pl1 4pl1RC1 4pl1RC2 5 5RC1 5RC2 5RC3 5RC4 5RC5 5RC7 5RC8 6 7 7RC1 7RC2 7RC3"
for each in $FOUR_ZERO; do
    wget $BASE"4/php-4.0."$each".tar.gz"
    tar -xzf "php-4.0."$each".tar.gz"
done

FOUR_ONE="0 0RC1 0RC2 0RC3 0RC4 0RC5 1 2"
for each in $FOUR_ONE; do
    wget $BASE"4/php-4.1."$each".tar.gz"
    tar -xzf "php-4.1."$each".tar.gz"
done

wget $BASE"4/php-4.2.1RC1.tar.bz2"
tar -xjf php-4.2.1RC1.tar.bz2
wget $BASE"4/php-4.2.1RC2.tar.bz2"
tar -xjf php-4.2.1RC2.tar.bz2

## Other archives exist in tar.bz2 format
FOUR_TWO="0 0RC1 0RC2 0RC3 0RC4 0RC5 1 2 3 3RC1 3RC2"
for each in $FOUR_TWO; do
    wget $BASE"4/php-4.2."$each".tar.gz"
    tar -xzf "php-4.2."$each".tar.gz"
done

wget $BASE"4/php-4.3.6RC1.tar.bz2"
tar -xjf php-4.3.6RC1.tar.bz2
wget $BASE"4/php-4.3.6RC2.tar.bz2"
tar -xjf php-4.3.6RC2.tar.bz2
wget $BASE"4/php-4.3.6RC3.tar.bz2"
tar -xjf php-4.3.6RC3.tar.bz2
wget $BASE"4/php-4.3.7RC1.tar.bz2"
tar -xjf php-4.3.7RC1.tar.bz2
wget $BASE"4/php-4.3.9RC1.tar.bz2"
tar -xjf php-4.3.9RC1.tar.bz2

## Other archives exist in tar.bz2 format
FOUR_THREE="0 1 10 11 2 2RC1 2RC2 2RC3 3 3RC1 3RC2 3RC3 3RC4 4 4RC1 4RC2 4RC3 5 5RC1 5RC2 5RC3 5RC4 6 7 8 9"
for each in $FOUR_THREE; do
    wget $BASE"4/php-4.3."$each".tar.gz"
    tar -xzf "php-4.3."$each".tar.gz"
done

## do we now have .tar.gz and .tar.bz2 consistently?
## also seem to have a Win32 zip option?
FOUR_FOUR="0 1 2 3 4 5 6 7 8 9"
for each in $FOUR_FOUR; do
    wget $BASE"4/php-4.4."$each".tar.gz"
    tar -xzf "php-4.4."$each".tar.gz"
done


## ships Win32 zip files, exe files and tar.bz2 files
FIVE_ZERO="0 0RC1 0b1 0b4 1 2 3 4 5"
for each in $FIVE_ZERO; do
    wget $BASE"5/php-5.0."$each".tar.gz"
    tar -xzf "php-5.0."$each".tar.gz"
done

FIVE_ONE="0 1 2 3 4 5 6"
for each in $FIVE_ONE; do
    wget $BASE"5/php-5.1."$each".tar.gz"
    tar -xzf "php-5.1."$each".tar.gz"
done

FIVE_TWO="0 1 10 11 12 13 14 15 16 17 2 3 4 5 6 8 9"
for each in $FIVE_TWO; do
    wget $BASE"5/php-5.2."$each".tar.gz"
    tar -xzf "php-5.2."$each".tar.gz"
done

FIVE_THREE="0 1 2 3 4 5 6"
for each in $FIVE_THREE; do
    wget $BASE"5/php-5.3."$each".tar.gz"
    tar -xzf "php-5.3."$each".tar.gz"
done
