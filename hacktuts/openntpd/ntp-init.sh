#!/bin/bash

NTPD_CONF=/opt/openntpd/etc/ntpd.conf

if [ ! -f $NTPD_CONF ]
  then
   echo "Could not find $NTPD_CONF"
   exit 1
fi

# -s : Set the time immediately at startup if the
#      local clock is off by more than 180 seconds.
PARAMS="-s"

PID=`pidof -o %PPID /opt/openntpd/sbin/ntpd`
case "$1" in
  start)
    echo "Starting OpenNTPD"
    [ -z "$PID" ] && /opt/openntpd/sbin/ntpd $PARAMS
    if [ $? -gt 0 ]; then
      echo "Failed"
    else
      PID=`pidof -o %PPID /opt/openntpd/sbin/ntpd`
      echo $PID >/var/run/openntpd.pid
      echo "Done"
    fi
    ;;
  stop)
    echo "Stopping OpenNTPD"
    [ ! -z "$PID" ]  && kill $PID &>/dev/null
    if [ $? -gt 0 ]; then
      echo "Failed"
    else
      echo "Done"
    fi
    ;;
  restart)
    $0 stop
    $0 start
    ;;
  *)
    echo "usage: $0 {start|stop|restart}"  
esac
exit 0
