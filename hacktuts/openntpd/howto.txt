1) Download the software from the OpenNTPD site:
   http://www.openntpd.org/portable.html 
2) Install the software into the /opt directory.
   mkdir /opt/openntpd
   cd /opt/openntpd
   wget $CHOSENURL
   tar -xzf openntpd-3.9p1.tar.gz
   cd openntpd-3.9p1
   ./configure --prefix=/opt/openntpd
   make
   make install
  
   NOTE:
       Need to add a proper group and user:
          groupadd _ntp
          useradd -g _ntp -s /sbin/nologin -d /var/empty -c 'OpenNTP daemon' _ntp 

       *An error like -->
           useradd: warning: the home directory already exists.

           Not copying any file from skel directory into it.

        might be thrown, but no worries b/c openssh creates the /var/empty directory.

3) Adjust the configuration file in /opt/openntpd/etc/.

4) Start the process through the ntp init script by adding it to /etc/init.d and chmod +x.

5) Verify connections to time servers through a 'netstat -u -p' and make sure iptables is open on port 123.


USEFUL LINK: http://www.basicallytech.com/blog/index.php?/archives/3-OpenNTPD-on-Linux.html
