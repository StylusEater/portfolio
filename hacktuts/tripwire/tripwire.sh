#!/bin/bash

##############################
## AUTHOR: Adam M. Dutko    ## 
## LICENSE: GPL v3 or Later ##
## CREATED: 10/16/09        ##
## UPDATED: 10/19/09        ##
##############################

HOSTNAME=`hostname`
DATE=`date +%Y%m%d`
ADMIN=`echo Please set me!`
ROOT_DIR=/usr/local/lib/report/
REPORT_SRC=${HOSTNAME}-${DATE}-*.twr
REPORT_DST=${HOSTNAME}.twr
TWREPORT='/usr/local/sbin/twprint -m r --twrfile '

if [ -e ${ROOT_DIR}${REPORT_SRC} ]; then
{
   # Our file exists so let's copy it for e-mail
   #`cp ${ROOT_DIR}${REPORT_SRC} ${ROOT_DIR}${REPORT_DST}`

   # Did the copy succeed?
   if [ -e ${ROOT_DIR}${REPORT_DST} ]; then
   {
      ${TWREPORT}${ROOT_DIR}${REPORT_DST} | mail -s TripWire Report ${ADMIN}
   }
   else
   {
      echo "There was a problem copying the daily TripWire report." | mail -s 'ERROR: TripWire Report' ${ADMIN}
   }
   fi
}
else
{
   # Ohh no! for some reason tripwire didn't run.
   # Send an e-mail to the admin.
   echo "There was a problem running the report." | mail -s 'ERROR: TripWire Report' ${ADMIN}
}
fi

