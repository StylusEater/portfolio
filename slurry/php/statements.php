<?php

########################################################
# Author: Adam M. Dutko                                #
# License: GPL (v3 or Later)                           #
#                                                      #
# Purpose:                                             #
#                                                      #
########################################################

################
## HELPFUL TIPS
##

   // Select Statements
   // Should probably create a generic wrapper for all this stuff...
   function select_db($dbname,$connection) {

       $status = FALSE;

       if (!$connection) {

           die("Sorry but you didn't pass a valid connection string.");

       } else {
       
           if (!$dbname) {

               die("Sorry but you didn't give me a database to use...");
               $status = mysql_select_db($dbname,$connection);

           }

       }

       return $status;

   }

   function build_get_node_by_ip($ipaddr) {

       $squery =  "select hostname,inet_ntoa(ip) as ip from maps where inet_ntoa(ip) = '$ipaddr';";
       return $squery;

   }

   function build_get_node_by_hostname($hostname) {

       die("YOU NEED TO MAKE THE DOMAINNAME CONFIGURABLE!");
       $hostname = "%" . $hostname . "%.FIXME.com";
       $squery = "select inet_ntoa(ip) as ip,hostname from maps where hostname like '$hostname';";
       return $squery;

   }

   function build_get_all_nodes_in_subnet($network) {
   
       $squery = "select hostname,inet_ntoa(network),inet_ntoa(ip) from maps where inet_ntoa(network) = '$network';";
       return $squery;
       
   }

   function build_check_if_node_exists_by_ip($ipaddr) {
       
       $squery = "select inet_ntoa(ip) as ip from maps where inet_ntoa(ip) = '$ipaddr' limit 1;";
       return $squery;

   }
   
   function build_check_if_node_exists_by_hostname($hostname) {
       
       $squery = "select hostname from maps where hostname = '$hostname' limit 1;";
       return $squery;

   }

   function build_get_all_addresses_between($startIP,$stopIP) {

       $squery = "select hostname,inet_ntoa(ip) as ip from maps where ip between inet_aton('$startIP') and inet_aton('$stopIP')";
       return $squery;

   }

   // Update Statements
   function build_node_update_query($qvars,$tbl,$fld) {

       $network = $qvars['network'];
       $ip = $qvars['ip'];
       $hostname = $qvars['hostname'];
       
       $uquery = "UPDATE $tbl set $fld='$qvars[$fld]' where ip = inet_aton('$ip') and network = inet_aton('$network');";
       return $uquery;

   }

   // Insert Statements
   function build_node_insert_query($qvars,$tbl) {

       $network = $qvars['network'];
       $ip = $qvars['ip'];
       $hostname = $qvars['hostname'];
       
       $iquery = "INSERT INTO $tbl (network, ip, hostname) VALUES (inet_aton('$network'),inet_aton('$ip'),'$hostname');";
       return $iquery;

   }


?>
