<!-- ########################################################
     # Author: Adam M. Dutko                                #
     # License: GPL (v3 or Later)                           #
     #                                                      #
     # Purpose:                                             #
     #                                                      #
     ########################################################

     ################
     ## HELPFUL TIPS
     ##
-->

<html>
<head>
   <title>Slurry - A Network Discovery Tool</title>
   <link rel="stylesheet" href="../css/default.css" media="all" type="text/css"/>
   <script src="../js/jquery-1.3.2.min.js" type="text/javascript"></script>
   <script src="../js/default.js" type="text/javascript"></script>
</head>
<body>
    <div id="nav">
        <table>
            <th>
                <p class="big_title">Menu</p>
            </th>
            <tr>
                <td>
                    <p class="landing_link"><a href="#" onclick="loadSearch();">Search Block</a></p>
                </td>
            </tr>
            <tr>
                <td>
                    <p class="landing_link"><a href="#" onclick="loadDisplay();">Display Block</a></p>
                </td>
            </tr>
        </table>
    </div>
    <div id="form">
    </div>
</body>
</html>
