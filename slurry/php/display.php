<?php

########################################################
# Author: Adam M. Dutko                                #
# License: GPL (v3 or Later)                           #
#                                                      #
# Purpose:                                             #
#                                                      #
########################################################

################
## HELPFUL TIPS
##

require_once 'config.php';
require_once 'statements.php';
require_once 'display.php';
require_once 'export.php';

if ($_POST['start'] == "")
{
    echo "<p>You forgot to enter a starting address.";
    exit;
}

if ($_POST['stop'] == "")
{
    echo "<p>You forgot to enter an ending address.";
    exit;
}

$conn = connect_db();
$retrieveNode = NULL;
$result = NULL;
$db = "Slurry";

// Cleanup input
$start = strtoupper($_POST['start']);
$start = strip_tags($start);
$start = trim($start);

$stop = strtoupper($_POST['stop']);
$stop = strip_tags($stop);
$stop = trim($stop);

if (!$conn) {

    die('Could not connect: ' . mysql_error());

} else {

    $retrieveNodes = build_get_all_addresses_between($start,$stop);
  
}

if ($retrieveNodes == NULL) {

    echo "There was a problem constructing the query.";
    die;

} else {

    $dbsel = mysql_select_db($db,$conn);
    $queryResult = mysql_query($retrieveNodes,$conn);
}

// First check if the results are empty then
// check to see if we want it in pdf format
// or xls format or finally if you just
// want to output it to the browser.

if ($queryResult == NULL) {

    echo "<html>";
    echo "<body>";
    echo "<p>Sorry, but we can not find an entries to match your query.</p>";
    echo "<b>You asked to start listing addresses at:</b> " . $start;
    echo "</p>";
    echo "<b>and to stop listing addresses at:</b> " . $stop;
    echo "</body>";
    echo "</html>";

    // Free result set
    mysql_free_result($queryResult);
    
    // Close DB Connection
    mysql_close($conn);

} else {

    echo "<html>";
    echo "<body>";
    echo "<p><b>All hostnames between </b>" . $start . " and " . $stop . "</p>";
    echo "<table border=1px><tr><td><b>HOST</b></td><td><b>IP</b></td></tr>";
    while ($entry = mysql_fetch_array($queryResult) ) {
        echo "<tr><td>" . $entry['hostname'] . "</td><td>" . $entry['ip'] . "</td></tr></p>";
    }
    echo "</table>";
    echo "</body>";
    echo "</html>";

    // Free result set
    mysql_free_result($queryResult);

    // Close DB Connection
    mysql_close($conn);
}

?>
