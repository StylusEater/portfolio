<?php

########################################################
# Author: Adam M. Dutko                                #
# License: GPL (v3 or Later)                           #
#                                                      #
# Purpose:                                             #
#                                                      #
########################################################

################
## HELPFUL TIPS
##


require_once 'config.php';
require_once 'statements.php';
require_once 'helperFunctions.php';

if ($_POST['data'] == "")
{
    echo "<p>You forgot to enter a search term.";
    exit;
}

$conn = connect_db();
$retrieveNode = NULL;
$result = NULL;
$db = "Slurry";

// Cleanup input
$data = strtoupper($_POST['data']);
$data = strip_tags($data);
$data = trim($data);
$type = $_POST['type'];
$resultType = NULL;

if (!$conn) {

    die('Could not connect: ' . mysql_error());

} else {

    switch ($type) {
        case 'hostname':
            $retrieveNode = build_get_node_by_hostname($data);
            $resultType = 'ip';
            break;
        case 'ip':
            $retrieveNode = build_get_node_by_ip($data);
            $resultType = 'hostname';
            break;
    }

}

if ($retrieveNode == NULL) {

    echo "There was a problem constructing the query.";
    die;

} else {

    $dbsel = mysql_select_db($db,$conn);
    $queryResult = mysql_query($retrieveNode,$conn);
}

if (mysql_num_rows($queryResult) == 0) {

    echo "<html>";
    echo "<body>";
    echo "<p>Sorry, but we can not find an entry to match your query.</p>";
    echo "<b>Searched For:</b> " . $data;
    echo "</body>";
    echo "</html>";

} else {

    echo "<html>";
    echo "<body>";
    echo "<p><b>Searched For:</b> " . $data . "</p>";
    echo "<table class=\"searchResultsTable\">";
    echo "<tr class=\"searchResultsRowTitle\">";
    echo "<td class=\"searchResultsCellTitle\">Hostname</td>";
    echo "<td class=\"searchResultsCellTitle\">Address</td>";
    echo "<td class=\"searchResultsCellTitle\">Status</td>";
    while ($eachResult = mysql_fetch_array($queryResult)) {
        echo "<tr class=\"searchResultsRowValue\">";
        
        $hostStatus = ping($eachResult['ip']);
        if ($hostStatus != 0) {
            $color = 'red';
            $status = 'DOWN';
        }
        if ($hostStatus == 0) {
            $color = 'green';
            $status = 'UP';
        }

        echo "<td class=\"searchResultsCellValue\"><p style=\"color: " . $color . "\"> " . $eachResult['hostname'] . "</p></td>";
        echo "<td class=\"searchResultsCellValue\"><p style=\"color: " . $color . "\"> " . $eachResult['ip'] . "</p></td>";
        echo "<td class=\"searchResultsCellValue\"><p style=\"color: " . $color . "\"> " . $status . "</a></p></td>";

        echo "</tr>";
    }
    echo "</table>";
    echo "</body>";
    echo "</html>";
}

?>
