<?php

########################################################
# Author: Adam M. Dutko                                #
# License: GPL (v3 or Later)                           #
#                                                      #
# Purpose: To provide a wrappper for generating export #
#          data for PDF and XML.                       #
#                                                      #
########################################################

################
## HELPFUL TIPS
##
## 1) apt-get install php5-dev
##    apt-get install php-fpdf
## 2) Code refactored from examples at http://www.fpdf.org/
## 3) XLS code refactored from:
##    http://www.appservnetwork.com/modules.php?name=News&file=article&sid=8

require('fpdf/fpdf.php');
#require_once 'config.php';
#require_once 'statements.php';

// Basic algorithm
// 1. Get result handle from calling application/script.
// 2. Create header for table.
// 3. Iterate over result set and write x number records per page.
// 4. Free result set and output values to browser.

class exportAsPDF {
    
    function __construct($resultSet) {

        $this->results = $resultSet;
        
    }
    
    function __destruct() {

        // Cleanup after query
        mysql_free_result($this->results);

    }

    function getResultSetPointer() {

        return $this->results;
    
    }

    function writeResults() {

        // Some useful globals
        $numberNodes = mysql_numrows($this->results);
        $column_hostname = "";
        $column_ip = "";
        $rowCount = 0;

        // Create an array entry for each row with all pertinent column values
        $allNodeEntries = array();
        while($row = mysql_fetch_array($this->results)) {
            $host = $row["hostname"];
            $ip = $row["ip"];

            $allNodeEntries[$rowCount]["hostname"] = $host;
            $allNodeEntries[$rowCount]["ip"] = $ip;

            $rowCount++;

        }


        //Create a new PDF file
        $pdf=new FPDF();
        
        $entryCount = 0;
        $X_Column_Base_Width = 40;
        $Y_Column_Incrementor = 6;
        $Y_Column_Headers_Position = 6;

        // Sets how many records to display per page before page breaking
        $pgRowCount = 40;

        while ($entryCount < $rowCount) {

            if ( $internalCount % $pgRowCount == 0 ) {
                
                // Add page break
                $pdf->AddPage();

                if ( $entryCount < 45 ) {

                    // Print out Header information
                    $pdf->SetFillColor(232,232,232);
                    $pdf->SetFont('Arial','B',12);
                    $pdf->SetY($Y_Column_Headers_Position);
                    $pdf->SetX($X_Column_Base_Width);
                    $pdf->Cell($X_Column_Base_Width * 2,6,'HOSTNAME',1,0,'C',1);
                    $pdf->SetX($X_Column_Base_Width * 3);
                    $pdf->Cell($X_Column_Base_Width,6,'IP',1,0,'C',1);

                    // Reset fonts and fills
                    $pdf->SetFillColor(255,255,255);
                    $pdf->SetFont('Arial','',12);
                
                
                }

                $Y_Column_Headers_Position = 6;
                $internalCount = 0;
            }

            $Y_Column_Headers_Position = ($Y_Column_Incrementor * $internalCount) + ($Y_Column_Incrementor * 2);
            $pdf->SetY($Y_Column_Headers_Position);
            $pdf->SetX($X_Column_Base_Width);
            $pdf->Cell($X_Column_Base_Width * 2,6,$allNodeEntries[$entryCount]["hostname"],1,0,'C',1);
            $pdf->SetX($X_Column_Base_Width * 3);
            $pdf->Cell($X_Column_Base_Width,6,$allNodeEntries[$entryCount]["ip"],1,0,'C',1);
            $internalCount++;
            $entryCount++;

        }

        $pdf->Output('blockMap.pdf','I');

    }
}

class exportAsXLS {

    function __construct($resultSet) {

        // Create an internal reference for our result set
        $this->results = $resultSet;

    }

    function __destruct() {

        // Cleanup after query
        mysql_free_result($this->results);

    }

    function getResultSetPointer() {

        // Getter for the result set from MySQL
        return $this->results;

    }

    function xlsBOF() {

        // Declare the beginning of an Excel Workbook
        echo pack("ssssss", 0x809, 0x8, 0x0, 0x10, 0x0, 0x0);
        return;


    }

    function xlsEOF() {

        // Declare the end of an Excel Workbook
        echo pack("ss", 0x0A, 0x00);
        return;

    }

    function xlsWriteLabel($Row, $Col, $Value ) {

        // Do some crazy magic with generating cells for Excel
        $L = strlen($Value);
        echo pack("ssssss", 0x204, 8 + $L, $Row, $Col, 0x0, $L);
        echo $Value;
        return;

    }

    function writeResults() {

        // Send Header Content to Browser
        header("Pragma: public");
        header("Expires: 0");
        header("Cache-Control: must-revalidate, post-check=0, pre-check=0");
        header("Content-Type: application/force-download");
        header("Content-Type: application/octet-stream");
        header("Content-Type: application/download");;
        header("Content-Disposition: attachment;filename=networkMap.xls ");
        header("Content-Transfer-Encoding: binary ");

        // XLS Data Cell
        $this->xlsBOF();
        $this->xlsWriteLabel(1,0,"HOSTNAME");
        $this->xlsWriteLabel(1,1,"IP");
        $xlsRowCount = 2;

        while(list($hostname,$ip)=mysql_fetch_row($this->results)) {
            $this->xlsWriteLabel($xlsRowCount,0,"$hostname");
            $this->xlsWriteLabel($xlsRowCount,1,"$ip");
            $xlsRowCount++;
        }

        $this->xlsEOF();
        exit();

    }

}

?>