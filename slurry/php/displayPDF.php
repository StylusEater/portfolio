<?php

########################################################
# Author: Adam M. Dutko                                #
# License: GPL (v3 or Later)                           #
#                                                      #
# Purpose:                                             #
#                                                      #
########################################################

################
## HELPFUL TIPS
##

require_once 'config.php';
require_once 'statements.php';
require_once 'export.php';

if ($_GET['start'] == "")
{
    echo "<p>You forgot to enter a starting address. START: " . $_GET['start'];
    exit;
}

if ($_GET['stop'] == "")
{
    echo "<p>You forgot to enter an ending address.";
    exit;
}

$conn = connect_db();
$retrieveNode = NULL;
$result = NULL;
$db = "Slurry";

// Cleanup input
$start = strtoupper($_GET['start']);
$start = strip_tags($start);
$start = trim($start);

$stop = strtoupper($_GET['stop']);
$stop = strip_tags($stop);
$stop = trim($stop);

$pdf = $_GET['pdf'];

if (!$conn) {

    die('Could not connect: ' . mysql_error());

} else {

    $retrieveNodes = build_get_all_addresses_between($start,$stop);
  
}

if ($retrieveNodes == NULL) {

    echo "There was a problem constructing the query.";
    die;

} else {

    $dbsel = mysql_select_db($db,$conn);
    $queryResult = mysql_query($retrieveNodes,$conn);
}

// First check if the results are empty then
// check to see if we want it in pdf format
// or xls format or finally if you just
// want to output it to the browser.

if ($queryResult == NULL) {

    echo "<html>";
    echo "<body>";
    echo "<p>Sorry, but we can not find an entries to match your query.</p>";
    echo "<b>You asked to start listing addresses at:</b> " . $start;
    echo "</p>";
    echo "<b>and to stop listing addresses at:</b> " . $stop;
    echo "</body>";
    echo "</html>";

    // Free result set
    mysql_free_result($queryResult);
    
    // Close DB Connection
    mysql_close($conn);

} else if ($pdf == 'on') {

    // Get new export PDF class
    $createdPDF = new exportAsPDF($queryResult);
    // Write results to the browser
    $createdPDF->writeResults();

}

?>
