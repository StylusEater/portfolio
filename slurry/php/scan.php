#!/usr/bin/php
<?php

########################################################
# Author: Adam M. Dutko                                #
# License: GPL (v3 or Later)                           #
#                                                      #
# Purpose:                                             #
#                                                      #
########################################################

################
## HELPFUL TIPS
##
## 1) You need to have a few packages installed before using this script.
##    On Debian: 
##       apt-get install php-pear
##       apt-get install php5-mhash
##       pear install Net_DNS-1.0.0
##    
## 2) TYPE => PTR response:
##
##    10.1.1.010.1.1.1Net_DNS_RR_PTR Object
##    (
##        [name] => 1.1.1.10.in-addr.arpa
##        [type] => PTR
##        [class] => IN
##        [ttl] => 3600
##        [rdlength] => 23
##        [rdata] => magicdragon
##    burningriversoftwarecom
##        [ptrdname] => magicdragon.burningriversoftware.com
##    )
##    
## 3) The debug option is very helpful.
##
## 4) The following link might be helpful:
##    http://articles.techrepublic.com.com/5100-10878_11-5234651.html
##

// include DNS class file
include("Net/DNS.php");

require_once 'config.php';
require_once 'statements.php';

#############################
### GLOBAL CONFIG OPTIONS ###
#############################

    // Scanner Specific
    $results = "";
    $stopTime = NULL;
    $args = $argv;
    
    // Database Specific
    $conn = connect_db();
    $db = 'Slurry';
    
    ## Valid address blocks
    #"10.1.5" => NULL,
    $addressBlockList = array(
        "10.1.1" => NULL,
        "10.1.2" => NULL,
        "10.1.3" => NULL,
        "10.1.4" => NULL,
        "10.1.6" => NULL,
    );

    ##############################
    # First check if we have CLI #
    # arguments.                 #
    ##############################
    array_shift($args);
    if (count($args) > 1)
    {
        print "You can only enter a logfile as an option.  Nothing more.\n";
        die;
    } else if (count($args) == 1) {
        global $args;
        $argsSize = count($argv);
        while ($argsSize >= 0) {
            $args[$argsSize] = $argv[$argsSize];
            $argsSize--;
        }
    }

    // Check database connectivity before proceeding
    if (!$conn) {

        die('Could not connect to backend: ' . mysql_error());

    }
    
    ## Options
    #
    class ScriptOptions
    {

        function __construct($cliArgs)
        {
            $this->scriptArgs = $cliArgs;
        }

        function __destruct()
        {
            $this->scriptArgs = NULL;
        }

        function getResultsFileName()
        {
            return $this->scriptArgs[1];
        }
 
    }

    ## Messages
    #
    class CLIMessages
    {
        function startScanning()
        {
            echo "Starting the address scanner...\n";
        }

        function stopScanning()
        {
            echo "Stopping the address scanner...\n";
        }

        function failedScanning()
        {
            echo "FAILED: Something interrupted the address scanner.\n";
        }

        function errorGatheringOptions()
        {
            echo "ERROR: Did not get a results file...\n";
        }

    }

    ## DNS Query
    #
    class DNSQuery
    {

        function __construct($addressBlock)
        {
            $this->block = $addressBlock;
            $this->start = "0";
            $this->stop = "255";
            $this->queryEngine = new Net_DNS_Resolver();
            $this->fullAddress = NULL;
            $this->response = NULL;
        }

        function buildAddress($lastOctet)
        {
            $this->fullAddress = $this->block . "." . $lastOctet;
        }

        function sendQuery($address,$type)
        {
            // USEFUL OPTION --> $this->queryEngine->debug = 1;
            $this->response = $this->queryEngine->search($address,$type);
        }

        function getAddress()
        {
            return $this->fullAddress;
        }

        function getResponse()
        {
            // We could have multiple responses ...
            if (count($this->response->answer) == 1)
            {
                return $this->response->answer[0]->ptrdname;
            } else {
                return "UNKNOWN";
            }
            
        }

    }

## Results File
    #
    class ResultsFile
    {

        function __construct($logFile)
        {
            $startTime = date('Y:m:d::H:m:s');
            $this->file = $logFile . "-" . $startTime . ".log";
            $this->handle = NULL;
        }

        function open()
        {
            if (file_exists($this->file))
            {
                die ("FILE EXISTS: Error reusing " . $this->file . "\n");
            } else {
                $this->handle = fopen($this->file, "x");
            }

        }

        function close()
        {
            fclose($this->handle);
        }

        function write($contents)
        {
            // We dumb write all contents to filehandle.
            // Make sure you pass newlines with all information.
            fwrite($this->handle,$contents);
        }

        function getFileName()
        {
            return $this->file;
        }

    }

##################
# RUN THE SCRIPT #
##################

###############################
# Populate globals with options
$messages = new CLIMessages();
$options = new scriptOptions($args);
$results = $options->getResultsFileName();
$options->__destruct();

###############################
# Are all options present?
if ($results) {
    
    #######################################################
    ## 1) Iterate over each address block and save results.
    ## 2) Write results to database.
    ## 3) Write results to log file.
    ## 4) Cleanup
    
    ## 1)
    $resultsFile = new ResultsFile($results);
    $resultsFileHandle = $resultsFile->open();
    $resultsFile->write($messages->startScanning());
    $resultsFile->write("Using " . $resultsFile->getFileName() . " for a log file.\n");
    
    ## 2)
    // Go through each block, starting at 0 and stopping at 255.
    $validBlocks = array_keys($addressBlockList);

    foreach ($validBlocks as $block) {

        $network = $block;

        $resultsFile->write("Started querying the " . $block . " address block at " . date('D:M:Y::h:m:s') . ".\n");
        $dnsQuery = new DNSQuery($block);
        $startHostNum = $dnsQuery->start;
        $stopHostNum = $dnsQuery->stop;
        $blockArray = array();

        for ($startHostNum; $startHostNum <= $stopHostNum; $startHostNum++) {
            $dnsQuery->buildAddress($startHostNum);
            $fullAddress = $dnsQuery->getAddress();
            $dnsQuery->sendQuery($fullAddress,"PTR");
            $queryResult = $dnsQuery->getResponse();
            $blockArray[$fullAddress] = $queryResult;
            $resultsFile->write($fullAddress . " resolves to " . $queryResult . "\n");

            // MySQL Components
            $ip = $fullAddress;
            $hostname = $queryResult;
            $tbl = "maps";
            $sqlvars = NULL;
            $nodeQuery = NULL;
            $ipExist = FALSE;
            $myMessage = NULL;
            $dbsel = mysql_select_db($db,$conn);

            // Check if node exists by IP first
            $sqlvars = array(
                        'network' => $network . '.0',
                        'ip' => $ip,
                        'hostname' => $hostname);
            $ipExistQuery = build_check_if_node_exists_by_ip($ip);
            $ipExistResultSet = mysql_query($ipExistQuery,$conn);
            $ipExistResult = mysql_fetch_assoc($ipExistResultSet);
            $knownAddress = $ipExistResult['ip'];
            
            if ($ip == $knownAddress) {

                $nodeQuery = build_node_update_query($sqlvars,$tbl,"hostname");
                $myMessage = "The node was successfully updated.\n";

            } else {

                $nodeQuery = build_node_insert_query($sqlvars,$tbl);
                $myMessage = "The node was successfully added to your database.\n";
               
            }

            if ($nodeQuery == NULL) {

                echo "ERROR: There was a problem constructing the query.\n";
                
            } else {

                $qryResult = mysql_query($nodeQuery,$conn);

                if (!$qryResult) {

                    echo "ERROR: " . mysql_error() . "\n";

                
                } else {

                    echo $myMessage;

                }
            }


        }

        $addressBlockList[$block] = $blockArray;
        $resultsFile->write("Done querying the " . $block . " address block at " . date('D:M:Y::h:m:s') . ".\n");
    }


    ## 3)
    $stopTime = date('Y:m:d::H:m:s');
    $resultsFile->write("Finished querying all blocks at " . $stopTime . "\n");


    ## 4)
    $resultsFile->close();
    mysql_close($conn);

} else {
    $messages->errorGatheringOptions();
}

?>
