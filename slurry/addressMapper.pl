#!/bin/perl

########################################################
# Author: Adam M. Dutko                                #
# License: GPL (v3 or Later)                           #
#                                                      #
# Purpose: To regularly scan address ranges on our     #
#          network and catalog all the information.    #
#                                                      #
########################################################

my $reportIN = $ARGV[0];
my $reportOUT = $ARGV[1];
$DEBUG = 0;
my $lineCount = 0;

if (DEBUG) {
   print "REPORT INPUT: " . $reportIN . "\n";
};

if (-e $reportOUT) {
   die "Oops! The old report file '$reportOUT' already exists.\n" . 
       "Please rename it before continuing.\n";
};

####################################
# Start reading file after checking
# for existence.
####################################
if (-r $reportIN) {
   open(INPUT, $reportIN);
   while(<INPUT>) {
      if ($DEBUG) {
         print "WORKING ON LINE #" . $lineCount . ".\n";
      };
      next if ($lineCount < 7);
      $inLine = $_;
   } continue {
      chomp($inLine);
      $inLine =~ tr/[a-z]/[A-Z]/;
      if ($DEBUG) {
         print "LINE: $lineCount --> $inLine\n";
      };
      $lineCount++;
      @logLine = split(',',$inLine);

      my $validIP = qr/^
      (
         (?:                              # first 3 octets:
         (?: 2(?:5[0-5]|[0-4][0-9])\. )   # 200 - 255
         |                                # or
         (?: 1[0-9][0-9]\. )              # 100 - 199
         |                                # or
         (?: (?:[1-9][0-9]?|[0-9])\. )    # 0 - 99
      )
        {3}                               # above: three times
         (?:                              # 4th octet:
         (?: 2(?:5[0-5]|[0-4][0-9]) )     # 200 - 255
         |                                # or
         (?: 1[0-9][0-9] )                # 100 - 199
         |                                # or
         (?: [1-9][0-9]?|[0-9] )          # 0 - 99
      )
     $)
     /x;
 
     if (@logLine > 2) { 
        if ($logLine[1] =~ /$range/) {
           $lookup = "host $logLine[1]"; 
           $hostINFO = `$lookup`;
        };  
     };

     @hostSTUFF = split(' ',$hostINFO);
     if ($DEBUG) {
        print $inLine . "\n";
        print "HOST --> " . $logLine[1] . "\n";
     };
     $logLine[1] = $hostSTUFF[-1];
 
     if (@logLine > 2) { 
        open(OUTPUT, ">>$reportOUT");
        print OUTPUT "@logLine \n";
     };

   };   
} else {
   print "Problem opening the input file: " . $reportIN . "\n";
}; 

close(OUTPUT);
close(INPUT);

print "REPORT OUTPUT: $reportOUT\n";
