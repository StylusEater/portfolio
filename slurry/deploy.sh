# Adam M. Dutko
# GPL (v3 or later)

rsync -av --exclude=deploy.sh --exclude=logs/ --exclude=sql/ --exclude=*.pl --exclude=.gitignore --exclude=nbproject/ . /var/www/slurry/