// Adam M. Dutko
// GPL (v3 or later)

$(document).ready(function() {
    $("div.nav").attr("display","display");
    $("div.form").html = "";
    $("div.form").attr("display","");    
});

function loadSearch() {
    $("div.form").value = "";
    $.ajax({
        url: "../html/search.html",
        cache: false,
        success: function(html){
            $("div#form").html(html);
        }
    });
}

function loadDisplay() {
    $("div.form").value = "";
    $.ajax({
        url: "../html/display.html",
        cache: false,
        success: function(html){
            $("div#form").html(html);
        }
    });
}

function displaySearchResults() {
    $.ajax({
        type: "POST",
        url: "../php/search.php",
        data: ({type: $('#type').val(),
                data: $('#data').val()}),
        success: function(html){
            $("div#form").html(html);
        }
    });
}

function filterDisplayResultsCall() {
    if ($('#pdf:checked').val() == 'on') {
        var pdf = $('#pdf:checked').val();
        var startPDF = $('#start').val();
        var stopPDF = $('#stop').val();
        var PDFlocation = '../php/displayPDF.php?pdf=' + pdf + '&start=' + startPDF + '&stop=' + stopPDF;
        window.location.replace(PDFlocation);
    } else if ($('#xls:checked').val() == 'on') {
        var xls = $('#xls:checked').val();
        var startXLS = $('#start').val();
        var stopXLS = $('#stop').val();
        var XLSlocation = '../php/displayXLS.php?xls=' + xls + '&start=' + startXLS + '&stop=' + stopXLS;
        window.location.replace(XLSlocation);
    } else {
        displayPlainDisplayResults();
    }
}

function displayPlainDisplayResults() {
    // There is some weird behavior where the values are considered 'on' because
    // they're defined but not really "checked."  To get around it we just need
    // to be careful to check the actual value being 'on' in the php code, which
    // we do now.
    $.ajax({
        type: "POST",
        url: "../php/display.php",
        data: ({start: $('#start').val(),
                stop: $('#stop').val()}),
        success: function(html){
            $("div#form").html(html);
        }
    });
}