Adam M. Dutko
GPL (v3 or later)

mysql> create database Slurry;
Query OK, 1 row affected (0.00 sec)

mysql> use Slurry;
Database changed

mysql> create table maps (id int unsigned auto_increment primary key, network int unsigned, ip int unsigned, hostname varchar(255)); 
Query OK, 0 rows affected (0.01 sec)

mysql> select inet_ntoa(network) as network, inet_ntoa(ip) as ip, hostname from maps;
