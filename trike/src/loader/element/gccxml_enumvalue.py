from element import Element

class GccxmlEnumValue(Element):
    def __init__(self, attributes, db, connection, xmlfile_id, name='gccxml_enumvalue', table='gccxml_enumvalue'):
        self.attributes = attributes
        self.db = db
        self.connection = connection
        self.name = name
        self.table = table
        self.xmlfile_id = xmlfile_id

    def save(self):
        super(GccxmlEnumValue, self).save(self.attributes, self.db, self.connection, self.table, self.xmlfile_id)