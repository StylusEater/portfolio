from element import Element

class GccxmlFunction(Element):
    def __init__(self, attributes, db, connection, xmlfile_id, name='gccxml_function', table='gccxml_function'):
        self.attributes = attributes
        self.db = db
        self.connection = connection
        self.name = name
        self.table = table
        self.xmlfile_id = xmlfile_id

    def save(self):
        super(GccxmlFunction, self).save(self.attributes, self.db, self.connection, self.table, self.xmlfile_id)