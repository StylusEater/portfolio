from element import Element

class GccxmlNamespaceAlias(Element):
    def __init__(self, attributes, db, connection, xmlfile_id, name='gccxml_namespacealias', table='gccxml_namespacealias'):
        self.attributes = attributes
        self.db = db
        self.connection = connection
        self.name = name
        self.table = table
        self.xmlfile_id = xmlfile_id

    def save(self):
        super(GccxmlNamespaceAlias, self).save(self.attributes, self.db, self.connection, self.table, self.xmlfile_id)