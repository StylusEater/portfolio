from element import Element

class GccxmlMethod(Element):
    def __init__(self, attributes, db, connection, xmlfile_id, name='gccxml_method', table='gccxml_method'):
        self.attributes = attributes
        self.db = db
        self.connection = connection
        self.name = name
        self.table = table
        self.xmlfile_id = xmlfile_id

    def save(self):
        super(GccxmlMethod, self).save(self.attributes, self.db, self.connection, self.table, self.xmlfile_id)