from element import Element

class Gccxml(Element):
    def __init__(self, attributes, db, connection, xmlfile_id, name='gccxml', table='gccxml'):
        self.attributes = attributes
        self.db = db
        self.connection = connection
        self.name = name
        self.table = table
        self.xmlfile_id = xmlfile_id

    def save(self):
        super(Gccxml, self).save(self.attributes, self.db, self.connection, self.table, self.xmlfile_id)