from element import Element

class GccxmlOperatorFunction(Element):
    def __init__(self, attributes, db, connection, xmlfile_id, name='gccxml_operatorfunction', table='gccxml_operatorfunction'):
        self.attributes = attributes
        self.db = db
        self.connection = connection
        self.name = name
        self.table = table
        self.xmlfile_id = xmlfile_id

    def save(self):
        super(GccxmlOperatorFunction, self).save(self.attributes, self.db, self.connection, self.table, self.xmlfile_id)