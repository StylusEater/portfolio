from xml.sax import ContentHandler, make_parser
from element.gccxml import Gccxml
from element.gccxml_argument import GccxmlArgument
from element.gccxml_arraytype import GccxmlArrayType
from element.gccxml_base import GccxmlBase
from element.gccxml_class import GccxmlClass
from element.gccxml_constructor import GccxmlConstructor
from element.gccxml_converter import GccxmlConverter
from element.gccxml_cvqualifiedtype import GccxmlCvQualifiedType
from element.gccxml_destructor import GccxmlDestructor
from element.gccxml_enumeration import GccxmlEnumeration
from element.gccxml_enumvalue import GccxmlEnumValue
from element.gccxml_field import GccxmlField
from element.gccxml_file import GccxmlFile
from element.gccxml_function import GccxmlFunction
from element.gccxml_functiontype import GccxmlFunctionType
from element.gccxml_fundamentaltype import GccxmlFundamentalType
from element.gccxml_method import GccxmlMethod
from element.gccxml_methodtype import GccxmlMethodType
from element.gccxml_namespace import GccxmlNamespace
from element.gccxml_namespacealias import GccxmlNamespaceAlias
from element.gccxml_offsettype import GccxmlOffsetType
from element.gccxml_operatorfunction import GccxmlOperatorFunction
from element.gccxml_operatormethod import GccxmlOperatorMethod
from element.gccxml_pointertype import GccxmlPointerType
from element.gccxml_referencetype import GccxmlReferenceType
from element.gccxml_struct import GccxmlStruct
from element.gccxml_typedef import GccxmlTypeDef
from element.gccxml_unimplemented import GccxmlUnimplemented
from element.gccxml_union import GccxmlUnion
from element.gccxml_variable import GccxmlVariable

# - GCCXmlHandler (add to handlers.py file)
class GCCXmlHandler(ContentHandler):
    """
    A handler to deal with C++ code in XML.
    """
    def __init__(self, db, conn):
        self.parser = make_parser( )
        self.xmlfile_id = 0
        self.xmlfile = ""
        self.database = db
        self.connection = conn

    def startElement(self, name, attrs):
        ## based on element name get instance
        if name == "GCC_XML":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = Gccxml(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Argument":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlArgument(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "ArrayType":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlArrayType(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Base":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlBase(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Class":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlClass(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Constructor":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlConstructor(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Converter":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlConverter(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "CvQualifiedType":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlCvQualifiedType(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Destructor":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlDestructor(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Enumeration":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlEnumeration(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "EnumValue":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlEnumValue(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Field":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlField(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "File":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))

            get_fid_query = "select id from file where path = '" + \
                            str(attrs.getValue('name')) + "'"
            try:
                self.database.execute(get_fid_query)
                file_id = self.database.fetchone( )[0]
                element_attrs["fid"] = str(file_id)
            except Exception:
                print "WARNING: gccxml_file.fid reference missing."

            element = GccxmlFile(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Function":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlFunction(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "FunctionType":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlFunctionType(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "FundamentalType":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlFundamentalType(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Method":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlMethod(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "MethodType":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlMethodType(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Namespace":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlNamespace(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "NamespaceAlias":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlNamespaceAlias(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "OffsetType":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlOffsetType(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "OperatorFunction":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlOperatorFunction(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "OperatorMethod":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlOperatorMethod(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "PointerType":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlPointerType(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "ReferencType":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlReferenceType(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Struct":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlStruct(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "TypeDef":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlTypeDef(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Unimplemented":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlUnimplemented(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Union":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlUnion(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )
        elif name == "Variable":
            element_attrs = {}
            attributes = attrs.getNames()
            for attribute in attributes:
                element_attrs[str(attribute)] = str(attrs.getValue(attribute))
            element = GccxmlVariable(element_attrs, self.database, self.connection, self.xmlfile_id)
            element.save( )