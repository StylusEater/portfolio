def process(processlog, db, conn, files=[]):
    ## Setup XML Parsing Parts
    from handlers import GCCXmlHandler
    handler = GCCXmlHandler(db, conn)
    saxparser = handler.parser
    saxparser.setContentHandler(handler)
    ## Error Counters
    errors = 0
    numerrors = 0

    for xmlfile in files:
        ## Make the file temporarily sane...
        name = xmlfile.split('/')[-1]
        ## NOTE: I removed escape_string because it was making the query fail.
        file_query = "select id from file f where f.name = '" + name + "';"
        db.execute(file_query)
        file_id = db.fetchone( )

        if file_id is None:
            print "Unable to process file: " + name
            continue
        else:
            fid = file_id[0]
            xml_query = "select id from xml where file_id = '" + str(fid) + "';"
            db.execute(xml_query)
            xmlfile_id = db.fetchone( )

            if xmlfile_id is None:
                print "Unable to process file: " + name
                continue
            else:
                xml_id = xmlfile_id[0]
       
        try:
            ## parse file
            handler.xmlfile_id = xml_id
            handler.xmlfile = name
            saxparser.parse(xmlfile)
        except IOError as FileException:
            errors = 1
            numerrors = numerrors + 1
            processlog.info(FileException)
            continue
        except Exception as UnknownException:
            errors = 1
            numerrors = numerrors + 1
            processlog.info(UnknownException)
            continue

    if errors:
        print str(numerrors) + " errors were encountered during this run."
        print "Please check the log for more information."