import logging
import sys

from os import listdir
from os.path import isdir,isfile,normpath

def get_dir_contents(curdir, excludeddirs, files=[]):
    logging.info("Generating directory content listing.")
    normdir = normpath(curdir)
            
    if isdir(curdir):
        for item in listdir(normdir):
            if item in excludeddirs:
                logging.info("Excluding Directory: " + curdir + '/' + item)
                print "Excluding Directory: " + curdir + '/' + item
                continue

            nextfile = curdir + '/' + item
            if isfile(nextfile):
                if nextfile not in files:
                    files.append(curdir + '/' + item)
            else:
                if isdir(curdir + '/' + item):
                    logging.info("Found Directory: " + curdir + '/' + item )
                    print "Found Directory: " + curdir + '/' + item
                    get_dir_contents(curdir + '/' + item, excludeddirs, files)
        
        logging.info("Discovered " + str(len(files)) + " file(s).")
        print "Discovered " + str(len(files)) + " file(s)."
        return files
    else:
        logging.info("ERROR: Failed on " + curdir)
        print "ERROR: Failed on " + curdir
        sys.exit(-1)
