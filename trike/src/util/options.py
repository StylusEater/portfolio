class TrikeOptions(object):
    def __init__(self, cfgfile='scanner.cfg'):
        import ConfigParser
        config = ConfigParser.SafeConfigParser()
        try:
            config.readfp(open(cfgfile))

            ## Application Options
            self.configure = config.get("main","runconfig")
            self.excludes = config.get("main","excludes").split(",")
            self.extensions = config.get("main","extensions").split(",")
            self.gccxml = config.get("main","gccxml")
            self.includes = config.get("main","includes").split(",")
            self.logfile = config.get("log", "file")
            self.logname = config.get("log", "name")
            self.project = config.get("main","project")
            
            ## Database Options
            self.db = config.get("database","db")
            self.host = config.get("database","host")
            self.name = config.get("main","name")
            self.overwrite = config.get("database", "overwrite")
            self.password = config.get("database","password")
            self.port = config.getint("database","port")
            self.sqlfile = config.get("database", "sqlfile")
            self.type = config.get("database","type")
            self.user = config.get("database","user")

        except IOError as FileException:
            print FileException
        except Exception as UnknownException:
            print UnknownException