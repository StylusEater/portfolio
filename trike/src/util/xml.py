import datetime
import logging
import os

from _mysql import escape_string
from os.path import isfile
from shlex import split
from string import rsplit
from subprocess import PIPE, Popen
from time import gmtime, strftime

from database import Connection

def generate(gccxml, project, includes, existingxmllist, program_id, files=[]):
    logging.info("Generating xml from identified files.")
    include = " "
    filecount = 0
    maxfiles = len(files)
    xmlfiles = []

    ## Remove the last item in the list which is blank in this case
    if includes.pop() == '':
        ## Rely on array starting at index 0
        includes = includes[:len(includes)]
    ## Unroll includes
    if len(includes) == 0:
        include = ""  
    else:
        for eachInclude in includes:
            include = include + " -I" + eachInclude
    logging.info("Using include list: " + include)
    starttime = datetime.datetime.now()

    ## Open Database Connection
    database = Connection( )
    conn = database.get( )
    db = conn.cursor( )

    for file in files:
        filecount = filecount + 1
        if isfile(file):
            ## Generate the new filename
            xmlfile = rsplit(file,".",1)[0] + ".xml"

            ## Exclude discovered xml files
            if xmlfile in existingxmllist:
                logging.info("Excluding File: " + file)
                print "Excluding Already Generated File: " + file
            else:
                ## Change directories
                os.chdir(project)
                ## Generate the xml
                timestamp = strftime("%a, %d %b %Y %H:%M:%S +0000", \
                                     gmtime())
                logging.info(timestamp + ": Generating File #" + \
                             str(filecount) + " out of " + str(maxfiles))
                print timestamp + ": Generating File #" + str(filecount) + \
                      " out of " + str(maxfiles)
                command = gccxml + " " + file + " -fxml=" + xmlfile + include
                args = split(command)
                xml = Popen(args, stderr=PIPE)
                while xml.returncode == None:
                    xml.poll()

                ## Handle Errors
                if xml.returncode == 1:
                    ## We don't do anything special with stdout
                    (stdout, stderr) = xml.communicate( )
                    if stdout != None:
                        print "STDOUT: " + stdout
                    ## Log the error to the database
                    error_query = "insert into errors (program_id, message) " + \
                        "values ('" + str(program_id) + "','" + \
                        escape_string(''.join(stderr)) + "')"
                    db.execute(error_query)
                    conn.commit( )
                else:
                    ## Add generated XML files to the list
                    xmlfiles.append(xmlfile)

            ## Generate meaningful shortname
            name = xmlfile.split('/')[-1]

            ## Catalog file in database if it isn't already in the database.
            check_file_query = "select id from file where name = '" + name + "';"
            db.execute(check_file_query)
            file_exists = db.fetchone( )
            if file_exists is None:
                file_query = "insert into file (program_id, type, name, path) " + \
                             "values ('" + str(program_id) + "','xml','" + \
                             name + "','" + xmlfile + "')"
                db.execute(file_query)
                conn.commit( )

                ## Add Contents of XML to Database
                xml_query = "select id from file where program_id = '" + \
                            str(program_id) + "' and name = '" + name + "'"
                db.execute(xml_query)
                xml_id = db.fetchone( )[0]
                try:
                    xml_file = open(xmlfile, 'r')
                    xml_contents = xml_file.readlines( )
                    xml_file.close( )
                    source_query = "insert into xml (file_id, contents) " + \
                                   " values('" + str(xml_id) + "','" + \
                                   escape_string(''.join(xml_contents)) + "')"
                    db.execute(source_query)
                    conn.commit( )
                except IOError as fileerror:
                    print xmlfile + " failed to generate."
                    print fileerror
                    logging.info("ERROR: Skipping invalid file. Check the Error" + \
                                  "table for more information.")
                    logging.info("ERROR: " + str(fileerror))
        else:
            logging.info("ERROR: " + file + " is not a valid file.")

    endtime = datetime.datetime.now()
    runtime = endtime - starttime
    print ""
    print "XML Generation Times   "
    print "-----------------------"
    print "STARTED: " + str(starttime)
    print "ENDED:   " + str(endtime)
    print "-----------------------"
    print "Total Time Generating: "
    print "-----------------------"
    print runtime 
    print "-----------------------"
    print ""
    return xmlfiles
