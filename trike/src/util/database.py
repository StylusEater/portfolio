from util.options import TrikeOptions

class DatabaseOptions(TrikeOptions):
    def __init__(self):
        TrikeOptions.__init__(self, 'scanner.cfg')
        self.version = '0.1'


class Connection(object):
    def __init__(self):
        self.options = DatabaseOptions( )

    def get(self):
        if self.options.type == "mariadb":
            from MySQLdb import connect
            return connect(host=self.options.host,user=self.options.user,
                           passwd=self.options.password,db=self.options.db,
                           port=self.options.port)
        else:
            raise Exception("We only support MariaDB/MySQL at this time.")
            from os import exit as fail
            fail(-1)