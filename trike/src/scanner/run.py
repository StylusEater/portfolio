def detect(base, ext, excludeddirs):
    import logging
    logging.info("Detecting files with specified extensions: " + str(ext) + ".")
    basepath = ""

    ## Validate Path
    from os.path import isdir,normpath
    if isdir(base):
        ## Continue
        logging.info("Using " + base + " as the search path.")
        basepath = normpath(base) 
    else:
        logging.error(base + " is not a directory.")
        raise OSError(base + " is not a directory.")
        from sys import exit as fail
        fail(-1)
   
    ## Open Directory
    from util.search import get_dir_contents
    validfiles = []
    files = get_dir_contents(basepath, excludeddirs, [])
    for file in files:
        try: 
            if file.endswith(tuple(ext)):
                logging.info("Adding " + file)
                validfiles.append(file)
            else:
                logging.info("Excluding " + file)
        except Exception as weirdError:
            logging.error("ERROR: " + str(weirdError))
            print "ERROR: " + str(weirdError)
            continue

    return validfiles
