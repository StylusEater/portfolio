def record(files, program_id, database, type=''):
    import logging
    if type is None:
        logging.info("ERROR: Need type of file to record.")
        print "ERROR: Need type of file to record."
        from sys import exit as fail
        fail(-1)
    
    logging.info("Recording files in the database.")
    print "Recording files in the database."

    ## Open Database Connection
    conn = database.get()
    db = conn.cursor()

    for file in files:
        ## Chop the file into a useful name.
        name = file.split('/')[-1]
        ## Catalog File in Database
        file_query = "insert into file (program_id, type, name, path)" + \
            " values ('" + str(program_id) + "','source','" + name + \
            "','" + file + "');"
        db.execute(file_query)
        conn.commit( )

        ## Add Contents of File to Database
        file_query = "select id from file where program_id = '" + \
                     str(program_id) + "' and name = '" + name + "';"
        try:
            db.execute(file_query)
            file_id = db.fetchone( )[0]
        except Exception as dbException:
            print "ERROR: tools.py " + str(dbException)
            from sys import exit as fail
            fail(-1)

        file_handle = open(file, 'r')
        file_contents = file_handle.readlines( )
        file_handle.close( )

        from _mysql import escape_string
        if type == 'source':
            source_query = "insert into source (file_id, contents) " + \
                           " values ('" + str(file_id) + "','" + \
                           escape_string(''.join(file_contents)) + "')"
        elif type == 'xml':
            source_query = "insert into xml (file_id, contents) " + \
                           " values ('" + str(file_id) + "','" + \
                           escape_string(''.join(file_contents)) + "')"

        db.execute(source_query)
        conn.commit( )
