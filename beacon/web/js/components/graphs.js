function viewGraphs() {
    new Ajax.Request('php/graph/list.php',
    {
        method:'get',
        onLoading: function() {
            $('tabContent3').value = "Generate and view graphs...";
        },
        onSuccess: function(transport) {
            var response = transport.responseText || "There was an error retrieving the graph form.";
            $('tabContent3').update(response);
        }
    })
}