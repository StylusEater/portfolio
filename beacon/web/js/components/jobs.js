// GET THE VIEW JOBS FORM
function viewJobs() {
    isLoggedIn();
    new Ajax.Request('/php/jobs/index.php',
        {
            method:'get',
            onLoading: function() {
                $('tabContent2').update("Retrieving jobs information...");
            },
            onSuccess: function(transport) {
                var response = transport.responseText || "There was an error retrieving job information.";
                $('tabContent2').update(response);

            }
        })
}

