function viewSearch() {
    new Ajax.Request('static/html/search/new.html',
        {
            method:'get',
            onLoading: function() {
                $('tabContent2').value = "Search your projects...";
            },
            onSuccess: function(transport) {
                var response = transport.responseText || "There was an error retrieving the search form.";
                $('tabContent2').update(response);
            }
        })
}
