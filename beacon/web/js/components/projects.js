// GET THE PROJECT INDEX PAGE
function viewProjects() {
    isLoggedIn();
    new Ajax.Request('/php/project/index.php',
        {
            method:'get',
            onLoading: function() {
                $('tabContent1').value = "Populating project list...";
            },
            onSuccess: function(transport) {
                var response = transport.responseText || "There was an error retrieving your projects.";
                $('tabContent1').update(response);
                $('projectMgmt').update("<input id=\"createProjectButton\" type=\"submit\" onmouseover=\"function highlightMe() { this.style.background.color = white; this.style.color = black; return true;} highlightMe();\" value=\"NEW\" onClick=\"javascript:createProject()\" />");
                $('projectMgmt').show();
                updateProjects();
            }
        })
}

// GET THE FILES CORRESPONDING TO A PROJECT
function getProjectFiles(projid,projsname,page) {
    isLoggedIn();
    var pageList = page;
    if (pageList == '') {
        pageList = '/php/file/list.php';
    }
    new Ajax.Request(pageList,
        {
            method: 'get',
            parameters: {projnum:projid,projshortname:projsname},
            onSuccess: function(transport) {
                var response = transport.responseText;
                if ($('projArchiveList')) {
                    $('projArchiveList').update(response);
                }
            }
        })
}

// LOAD THE PROJECT LIST PAGE
function updateProjects() {
    new Ajax.Request('php/project/list.php',
        {
            method: 'get',
            onSuccess: function(transport) {
                var projects = transport.responseText;
                $('projectsDiv').update(projects);
            }
        });
}

// ADJUST THE FILE UPLOAD TARGET FOR PROJECTS TO THE HIDDEN IFRAME
function modifyUploadTarget() {
    $('attachArchive').target = 'projfileUpload';
}


function createProject() {
    isLoggedIn();
    new Ajax.Request('static/html/project/new.html',
        {
            method:'get',
            onLoading: function() {
                $('projectMgmt').value = "Getting new project form...";
            },
            onSuccess: function(transport) {
                var response = transport.responseText || "There was an error retrieving the project create page.";
                $('projectMgmtStatus').style.display = 'none';
                $('projectMgmt').update(response);
            }
        })
}

function addProject() {
    var newProjForm = $('newProjectForm');
    var projNameVal = newProjForm['projname'];
    var projTypeVal = newProjForm['projtype'];

    $('projectMgmtStatus').update("<p>Attempting to add project...</p>");
    if ($(projNameVal).getValue() == "") {
        $('projectMgmtStatus').style.display = 'block';
        $('projectMgmtStatus').update("<p>Project name cannot be blank.");
    } else if ($(projNameVal).getValue() != "") {
    new Ajax.Request('php/project/new.php',
        {
            method:'post',
            parameters: {projname: $(projNameVal).getValue(), projtype: $(projTypeVal).getValue()},
            onComplete: function(transport) {
                $('projectMgmtStatus').style.display = 'block';
                $('projectMgmtStatus').update(transport.responseText);
                updateProjects();

            },
            onFailure: function(transport) {
                $('projectMgmtStatus').style.display = 'block';
                $('projectMgmtStatus').update(transport.responseText);
            }
        })
    }
}

function viewProject(projid,projname,projtype) {
    isLoggedIn();
    if ($('projectMgmtStatus')) {
        $('projectMgmtStatus').update("<p>Attempting to edit project #" + projid + ".</p>");
    }
    var page = ''
    if (projtype == "archive") {
        page = '/static/html/project/archive/view.html';
    }
    if (projtype == "repository") {
        page = '/static/html/project/repository/view.html';
    }
    new Ajax.Request(page,
       {
        method:'get',
        onSuccess: function(transport) {
            // NOTE: Do not move the next four lines!!!
            var response = transport.responseText;
            $('projTitle').update(projname);
            $('projectMgmt').update(response);

            if ($('projectMgmtStatus')) {
                $('projectMgmtStatus').style.display = 'none';
            }
            if ($('projects')) {
                $('projects').style.display = 'none';
            }

            // elements both have in common
            $('projid').update(projid);
            $('projname').update(projname);
            $('projtype').update(projtype);

            if (projtype == "archive") {
                // Need to set hidden field values
                $('projidFile').value = projid;
                $('projnameFile').value = projname;
                $('projtypeValueFile').value = projtype;
                getProjectFiles(projid,projname,'');
            }
            if (projtype == "repository") {
                $$('tr.projrepo').each(function(item) {
                    item.style.display = 'block';
                });
            }
        },
        onError: function(transport) {
            var error = transport.responseText;
            $('projectMgmtStatus').update("There was an error starting the project edit wizard: " + error);
        },
        onFailure: function(transport) {
            var failure = transport.responseText;
            $('projectMgmtStatus').update("There was an error retrieving the project edit page: " + failure);
        }
    })
}

function editProject(projid,projname,projtype) {
    isLoggedIn();
    $('projectMgmtStatus').update("<p>Attempting to edit project #" + projid + ".</p>");
    new Ajax.Request('static/html/project/edit.html',
        {
            method:'get',
            onSuccess: function(transport) {
                var response = transport.responseText;
                $('projectMgmt').update(response);
                $('projid').value = projid;
                $('projname').value = projname;
                $('projtype').value = projtype;
                $('projtypeValue').value = projtype;
                if (projtype == "archive") {
                    $('projfilerow').style.display = 'block';
                    getProjectFiles(projid,projname,'');
                }
                if (projtype == "repository") {
                    $$('tr.projrepo').each(function(item) {
                        item.style.display = 'block';
                    });
                }
                $('projectMgmtStatus').hide();
                $('projects').hide();
            },
            onError: function(transport) {
                var error = transport.responseText;
                $('projectMgmtStatus').update("There was an error starting the project edit wizard: " + error);
            },
            onFailure: function(transport) {
                var failure = transport.responseText;
                $('projectMgmtStatus').update("There was an error retrieving the project edit page: " + failure);
            }
        })
}

function deleteProject(projid,projsname) {
    $('projectMgmtStatus').update("<span class=\"message\">Attempting to delete " + projsname + ".</span>");
    new Ajax.Request('/php/project/delete.php',
        {
            method:'post',
            parameters:{projectid: projid, shortname: projsname},
            onSuccess: function(transport) {
                $('projectMgmtStatus').update(transport.responseText);
                updateProjects();
            },
            onFailure: function(transport) {
                $('projectMgmtStatus').style.display = 'block';
                $('projectMgmtStatus').update(transport.responseText);
            },
            onError: function(transport) {
                $('projectMgmtStatus').style.display = 'block';
                $('projectMgmtStatus').update(transport.responseText);
            }
        })
}

function cancelCreateProject() {
    viewProjects();
}

function cancelEditProject() {
    // BUG: CHROME - I think the issue is we're not truly reloading the list so
    //               the DOM is f'ed.  Need to fix this to rid ourselves of the
    //               edit button doesn't work bug.  The solution right now is
    //               to reload the page.  There HAS TO BE a reactivation tool
    //               for the element targets to be valid again???
    window.location = "/";
}

function unlinkFile(projid,revision,projname,projsname) {
    new Ajax.Request('/php/file/unlink.php',
        {
            method:'post',
            parameters:{id: projid, rev: revision, name: projname, shortname: projsname},
            onSuccess: function(transport) {
                var message = transport.responseText;
                getProjectFiles(projid,projsname,'');
                updateArchiveStatus(message);
            },
            onFailure: function(transport) {
                updateArchiveStatus(transport.responseText);
            }
        })
}

function updateNoArchivesMessage(message) {
    $('noArchivesMessage').style.display = 'block';
    $('noArchivesMessage').update(message);
}

// SHOW STATUS OF ARCHIVE OPERATION
function updateArchiveStatus(msg) {
    $('projFileStatus').style.display = 'block';
    $('projFileStatus').update(msg);
}

function startAnimation(eName,message) {
    $(eName).style.display = 'block';
    $(eName).update(message).pulsate({pulses: 3600, duration: 3600 });
}

function stopAnimation(eName,message) {
    $(eName).pulsate({stop: true});
    if (message != "") {
        $(eName).update(message);
    }
}

function showError(eName,message) {
    $(eName).style.display = 'block';
    $(eName).update(message);
}

function hideNoArchivesMessage() {
    $('noArchivesMessage').style.display = 'none';
}

function monitorCatalogProcess(projid, pid) {
    var result = 0;
    new Ajax.Request('/php/jobs/monitor.php',
        {
            method:'get',
            parameters:{id: projid, pid: pid},
            asynchronous: false,
            onSuccess: function(transport) {
                var message = transport.responseText;
                if (message == 0) {
                    result = 0;
                } else if (message == 1) {
                    result = 1;
                } else if (message == 2) {
                    result = 2;
                } else {
                    result = 2;
                }
            },
            onError: function(transport) {
                var message = transport.responseText;
                showError('projFileCatalogAnimation',message);
            }
        })
    return result;
}

function catalogFile(projid,revid) {

    new Ajax.Request('/php/file/catalog.php',
        {
            method:'get',
            parameters:{id: projid, rev: revid},
            onSuccess: function(transport) {
                var message = transport.responseText;
                var messageParts = message.split(": ",2);
                var msg = messageParts[0];
                var pid = messageParts[1];
                var cleanPID = pid.split("\n",2);
                pid = cleanPID[0];
                
                // Monitor until one of three states is reached
                // 0 - running
                // 1 - success
                // 2 - error
                var initialStatus = 0;
                initialStatus = monitorCatalogProcess(projid,pid);

                if (initialStatus == 0) {
                    $('projFileCatalogAnimation').update(message);
                    startAnimation('projFileCatalogAnimation',msg);
                } else if (initialStatus == 2) {
                    showError('projFileCatalogAnimation',"We encountered an error (" + initialStatus + ") in cataloging.<br />" + message);
                }
            },
            onError: function(transport) {
                var message = transport.responseText;
                showError('projFileCatalogAnimation',message);
            }
        })
}