// HIDE THE SIGNUP PAGE AND RETURN TO THE LOGIN PORTAL
function cancelSignup() {
    displayLogin();
}

// SHOW THE SIGNUP FORM
function getSignupForm() {
    new Ajax.Request('/static/html/signup.html',
    {
        method:'get',
        onSuccess: function(transport) {
            var response = transport.responseText || "Please contact the administrator.";
            $('portal').update(response);
        }
    });
}

function getSignupFormAgain() {
    window.location = '/';
}

function loginAfterSignup() {
    window.location = '/';
}
