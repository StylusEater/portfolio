// AUTH THE USER AND LOG THEM IN
function login(email,password) {
    new Ajax.Request('/php/account/login.php',
        {
            method:'post',
            parameters: {login:email,password:password},
            onSuccess: function(transport) {
                var response = transport.responseText;
                // you've logged in'
                if (response == 'True') {
                    hideLogin();
                    displayTabs();
                    toggleTab(1,5);
                    adjustTitleSize("2em");
                    viewProjects();
                } else {
                    // Failed to login
                    alert(response);
                    displayLogin();
                }
            }
        })
}

// ADJUST THE SIZE OF THE TITLE
function adjustTitleSize(size) {
    $('title').style.font.size = size;
}

// CONFIRM LOGOUT THEN CLEANUP
function logout() {
    var userIsLoggedIn = 'False';
    var logoutUser = 'False';
    new Ajax.Request('/php/account/isLoggedIn.php',
        {
            method:'get',
            onSuccess: function(transport) {
                userIsLoggedIn = transport.responseText;
                if (userIsLoggedIn == 'True') {
                    // Log the user out
                    new Ajax.Request('php/account/logout.php',
                    {
                        method:'get',
                        onSuccess: function(transport) {
                            logoutUser = transport.responseText;
                            if (logoutUser == 'True') {
                                hideTabs();
                                adjustTitleSize("10em");
                                displayLogin();
                            }
                        }
                    });
                }
            }});
}

// GENERIC SESSION CHECKER
function isLoggedIn() {

    new Ajax.Request('/php/account/isLoggedIn.php',
        {
            method:'get',
            onSuccess: function(transport) {
                var response = transport.responseText;
                if (response == 'True') {
                    // do nothing
                } else {
                    // redirect to front page -- don't save context for now
                    window.location = "/";
                }
            }
        })
}

// LOGIN PIECES
function checkIfLoggedIn() {

    new Ajax.Request('/php/account/isLoggedIn.php',
        {
            method:'get',
            onSuccess: function(transport) {
                var response = transport.responseText;
                if (response == 'True') {
                    hideLogin();
                    displayTabs();
                    toggleTab(1,5);
                    viewProjects();
                } else {
                    displayLogin();
                }
            }
        });

}

// HIDE THE LOGIN PORTAL
function hideLogin() {
    $('portal').style.display = 'none';
}

// DISPLAY THE LOGIN PORTAL
function displayLogin() {
    new Ajax.Request('/static/html/login.html',
        {
            method:'get',
            onSuccess: function(transport) {
                var response = transport.responseText || "Please contact the administrator.";
                $('portal').update(response);
                $('email').value = "your@email";
                $('pass').value = "beacon";
                $('portal').style.display = "block";
                $('email').style.background.color = "#cccccc";
                $('email').style.color = "#000000";
                $('email').style.font.weight = "bold";
                $('pass').style.background.color = "#cccccc";
                $('pass').style.color = "#000000";
                $('pass').style.font.weight = "bold";
            },
            onError: function(transport) {
                var response = transport.responseText || "Please contact the administrator.";
                $('portal').update(response);
                $('portal').style.display = "block";
                $('portal').style.background.color = "#cccccc";
            }

        });
}

// GET THE PASSWORD RESET FORM
function getForgotPassForm() {
        new Ajax.Request('/static/html/account/reset.html',
        {
            method:'get',
            onSuccess: function(transport) {
                var response = transport.responseText || "Please contact the administrator.";
                $('portal').update(response);
            }
        });
}

// GET THE MODIFY ACCOUNT FORM
function modifyAccount() {
    isLoggedIn();
    new Ajax.Request('/static/html/account/modify.html',
        {
            method:'get',
            onLoading: function() {
                $('tabContent3').value = "Retrieving account information...";
            },
            onSuccess: function(transport) {
                var response = transport.responseText || "There was an error retrieving your account information.";
                $('tabContent3').update(response);
                
                new Ajax.Request('/php/account/modify.php',
                    {
                        method:'get',
                        onSuccess: function(transport) {
                            var response = transport.responseText;
                            // IceWeasel in Debian 5.0.8 doesn't seem to support JSON ... :-(
                            try {
                                // use it!
                                var JSONResponse = JSON.parse(response);
                                $('login').value = JSONResponse.userLogin;
                                $('login').update(JSONResponse.userLogin);
                                $('fname').value = JSONResponse.userFname;
                                $('fname').update(JSONResponse.userFname);
                                $('lname').value = JSONResponse.userLname;
                                $('lname').update(JSONResponse.userLname);
                            } catch(err) {
                                // let's skip JSON parse
                                // we'll get a "ReferenceError" because JSON is
                                // not a supported Object before Gecko 3.1
                                console.log(err);
                                response = response.replace("{","").replace("}","");
                                response = response.replace("userLogin","");
                                response = response.replace("userFname","");
                                response = response.replace("userLname","");
                                response = response.replace("\"\":","");
                                response = response.replace("\"\":","");
                                response = response.replace("\"\":","");
                                response = response.replace("\"","");
                                response = response.replace("\"","");
                                response = response.replace("\"","");
                                response = response.replace("\"","");
                                response = response.replace("\"","");
                                response = response.replace("\"","");
                                params = response.split(",",3);
                                login = params[0];
                                fname = params[1];
                                lname = params[2];
                                $('login').update(login);
                                $('login').value = login;
                                $('fname').update(fname);
                                $('fname').value = fname;
                                $('lname').update(lname);
                                $('lname').value = lname;
                            }
                        }
                    })
            }
        })
}

// APPLY CHANGES TO A USERS ACCOUNT
function updateAccount() {
    var modAcctForm = $('modifyAccount');
    var loginVal = modAcctForm['login'];
    var fnameVal = modAcctForm['fname'];
    var lnameVal = modAcctForm['lname'];
    var passVal = modAcctForm['pass'];
    var passConfirmVal = modAcctForm['passConfirm'];

    if (($(passVal).getValue() == "") && ($(passConfirmVal).getValue()) == "") {
        $('confirmUpdateAccount').style.display = 'block';
        $('confirmUpdateAccount').update("<p id=\"message\">Please enter your current password to change your information.</p>");
    }  else if ($(passVal).getValue() != "") {
        if ($(passConfirmVal).getValue() != "") {
            if ($(passConfirmVal).getValue() == $(passVal).getValue()) {
                new Ajax.Request('../php/account/update.php',
                {
                    method:'post',
                    parameters: {login: $(loginVal).getValue(), fname: $(fnameVal).getValue(), lname: $(lnameVal).getValue(), pass: $(passVal).getValue(), passConfirm: $(passConfirmVal).getValue()},
                    onSuccess: function(transport) {
                        var updateResponse = transport.responseText;
                        $('confirmUpdateAccount').style.display = 'block';
                        $('confirmUpdateAccount').update(updateResponse);
                        $('confirmUpdateAccount').style.color = 'white';
                        $('confirmUpdateAccount').style.font.weight = 'bold';
                        $('confirmUpdateAccount').style.padding = '1em';
                        $('confirmUpdateAccount').style.text.align = 'center';
                        $('confirmUpdateAccount').style.font.size = 'large';
                    }
                })
            } else {
                $('confirmUpdateAccount').style.display = 'block';
                $('confirmUpdateAccount').update("<p id=\"message\">The passwords you entered didn\'t match.</p>");
            }
        } else {
                new Ajax.Request('../php/account/update.php',
                {
                    method:'post',
                    parameters: {login: $(loginVal).getValue(), fname: $(fnameVal).getValue(), lname: $(lnameVal).getValue(), pass: $(passVal).getValue(), passConfirm: $(passConfirmVal).getValue()},
                    onSuccess: function(transport) {
                        var updateResponse = transport.responseText;
                        $('confirmUpdateAccount').style.display = 'block';
                        $('confirmUpdateAccount').update(updateResponse);
                    }
                })
        }
    }
}

// CANCEL THE ACCOUNT SIGNUP REQUEST
function cancelAccountReset() {
    displayLogin();
}