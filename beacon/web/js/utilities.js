// GENERAL ROUTINES
// http://www.sean.co.uk/a/webdesign/javascriptdelay.shtm
function wait(millisecs) {
    var date = new Date();
    var curDate = null;

    do { curDate = new Date(); }
        while(curDate-date < millisecs);
}

// SUBMIT WHEN USER PRESSES ENTER
function filterKeyPress(method) {
    var key = event.which || event.keyCode || event.charCode;
    switch (key) {
        case Event.KEY_RETURN:
            if (method == "login") {
                login($('login')['email'].value,$('login')['pass'].value);
            }
            if (method == "signup") {

            }
            break;
    }
}

function displayTabs() {
    $('tabs').style.display = 'block';
}

function hideTabs() {
    $('tabs').style.display = 'none';
}

// LOGIN FORM
function highlightRow(theRow) {
    $(theRow).addClassName('highlight');
}

function unhighlightRow(theRow) {
    $(theRow).removeClassName('highlight');
}

function enterKeyCode(keyEvent) {
    var keycode;
    // Grab the key used to signal this event
    keycode = keyEvent.which;

    // If the person pressed enter
    if (keycode == 13) {
        // They pressed enter
        return true;
    } else {
        // Otherwise ignore the key press
        return false;
    }
}

// TABS
function toggleDisp() {
    for (var i=0;i<arguments.length;i++){
        var d = $(arguments[i]);
        if (d.style.display == 'none')
            d.style.display = 'block';
        else
            d.style.display = 'none';
    }
}

function toggleTab(num,numelems,opennum) {
    if ($('tabContent'+num).style.display == 'none'){
        for (var i=1;i<=numelems;i++){
            if ((opennum == null) || (opennum != i)){
                var temph = 'tabHeader'+i;
                var h = $(temph);
                if (!h){
                    var h = $('tabHeaderActive');
                    h.id = temph;
                }
                var tempc = 'tabContent'+i;
                var c = $(tempc);
                if(c.style.display != 'none'){
                    toggleDisp(tempc);
                }
            }
        }
        var h = $('tabHeader'+num);
        if (h)
            h.id = 'tabHeaderActive';
        h.blur();
        var c = $('tabContent'+num);
        c.style.marginTop = '2px';
        toggleDisp('tabContent'+num);
    }
}

function colorize(projID) {
    $(projID).style.color = 'orange';
    $(projID).style.cursor = 'hand';
}

function uncolorize(projID) {
    $(projID).style.color = 'black';
    $(projID).style.cursor = 'default';
}

function clearInput(fld) {
    $(fld).value = "";
}

function changeColor(fld,color) {
    $(fld).style.color = color;
}

function activateField(fldName) {
    $(fldName).style.disabled = "";
}

function getFileSize(filename) {
    isLoggedIn();
    alert(filename);
    new Ajax.Request('/php/file/getSize.php',
        {
            method:'get',
            parameters: {filename: filename},
            onSuccess: function(transport) {
                var filesize = transport.responseText;
                var oldFileSize = $('projectFileUploadAnimation').size.value;
                alert(oldFileSize);
                $('projectFileUploadAnimation').size.value = filesize;
            }
        });
}