<?php
require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

// Logger
$listJobLog = Logger::getRootLogger();
$DEBUG = 1;
$noJobs = FALSE;

// Variables given
$user = $_SESSION["user"];

// Database modification handle
$projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$projectBackend->setup();

// We're connected to the data store
$handle = $projectBackend->getHandle();

// Get jobs for all user projects
$jobList = $getUserJobsBegin . $getUserJobsEnd;

if ($DEBUG) {
    $listJobLog->debug($jobList);
}

$jobCount = 0;
$noJobs = "<tr id=\"projArchive\"></tr>";
$fileResults = "";

if ($handle) {

    $fileCount = 0;
    $preparedFiles = $handle->prepare($fileList);
    $preparedFiles->execute();
    $files = $preparedFiles->fetchAll();
    $fileCount = count($files);
    if ($fileCount == 0) {
        $noArchives = TRUE;
    } else {
        foreach ($files as $file)
        {
            $nextFile = "<tr id=\"projArchive" . $fileCount . "\">" .
                    "<td class=\"projArchiveName\">" . $file['name'] . "</td>" .
                    "<td class=\"projArchiveRev\">" . $file['revision'] . "</td>" .
                    "<td class=\"projArchiveFileOperations\">
                        <a class=\"unlinkArchive\" onclick=\"unlinkFile('" . $projid . "','" . $file['revision'] . "','" . $file['name'] . "','" . $projShortName . "');\">UNLINK</a>
                        <a class=\"catalogArchive\" onclick=\"catalogFile('" . $projid . "','" . $file['revision'] . "');\">CATALOG</a>" .
                    "</td>" .
                "</tr>";
            $fileResults = $fileResults . $nextFile;
            $fileCount++;
        }

        if ($DEBUG) {
            $listFileLog->debug($fileResults);
        }
    }

    // Always remember to cleanup our database connection
    $projectBackend->teardown();

    // Really make sure cleanup is done
    $projectBackend = NULL;

}

if ($noJobs) {
    if ($DEBUG) {
        $listJobLog->debug("No jobs for " . $user . ".");
    }
    echo $noJobs;
    echo "<script type=\"text/javascript\">
          updateNoArchivesMessage(\"No archives attached to this project.\");
     </script>";
} else {
    if ($DEBUG) {
        $listJobLog->debug($jobResults);
    }
    echo $jobsResults;
    echo "<script type=\"text/javascript\">
          hideNoArchivesMessage();
     </script>";
}


?>
