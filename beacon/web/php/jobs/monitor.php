<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');
require('../utilities/systemOperations.php');

// Logger
$monitorCatalogFileLog = Logger::getRootLogger();
$DEBUG = 1;

// Variables given
$projid = $_GET['id'];
$pid = $_GET['pid'];

// Database modification handle
$projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$projectBackend->setup();

// We're connected to the data store
$handle = $projectBackend->getHandle();

if ($handle) {

    // NOTE: In the future we might want to limit by time too just because PIDs
    //       roll over...
    //       
    // get status
    $getCatalogJobStatusQuery = $getCatalogJobStatusBegin . $projid .
                                $getCatalogJobStatusPID . $pid .
                                $getCatalogJobStatusEnd;

    if ($DEBUG) {
        $monitorCatalogFileLog->debug($getCatalogJobStatusQuery);
    }

    $catalogJobStatusPrepared = $handle->prepare($getCatalogJobStatusQuery);
    $catalogJobStatusPrepared->execute();
    $catalogJobStatus = $catalogJobStatusPrepared->fetchAll();

    if ($DEBUG) {
        $monitorCatalogFileLog->debug($catalogJobStatus);
    }

    $jobStatus = $catalogJobStatus[0]["status"];
    $jobStatus = (int) $jobStatus;

    if ($jobStatus === 0) {
        
        // RUNNING
        if ($DEBUG) {
            $monitorCatalogFileLog->debug("PROJECT #" . $projid . ": JOB STATUS: RUNNING (" . $jobStatus . ") " . $pid);
        }

        echo "0";

    } else if ($jobStatus === 1) {
        
        // FINISHED
        if ($DEBUG) {
            $monitorCatalogFileLog->debug("PROJECT #" . $projid . ": JOB STATUS: FINISHED (" . $jobStatus . ") " . $pid);
        }

        echo "1";
        
    } else {
        
        // ERROR
        if ($DEBUG) {
            $monitorCatalogFileLog->debug("PROJECT #" . $projid . ": JOB STATUS: ERROR (" . $jobStatus . ") " . $pid);
        }
        echo "2";
    }

}

?>
