<?php

// Thanks --> http://blogulate.com/content/php-function-for-javascript-redirect/
function js_redirect($url, $seconds=5) {
    echo "<script language=\"JavaScript\">\n";
    echo "function redirect() {\n";
    echo "window.location = \"" . $url . "\";\n";
    echo "}\n\n";
    echo "timer = setTimeout('redirect()', '" . ($seconds*1000) . "');\n\n";
    echo "</script>\n";

    return true;

}

function load_js_include($include,$all=FALSE,$prefix='') {
    if ($all) {
        echo "<!-- FRAMEWORK --> \n";
        echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/framework/prototype.js\"></script> \n";
        echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/framework/scriptaculous.js\"></script> \n";
        echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/prettify/prettify.js\"></script> \n";
        echo "<!-- BEGIN CUSTOM --> \n";
        echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/components/account.js\"></script> \n";
        echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/components/graphs.js\"></script> \n";
        echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/components/projects.js\"></script> \n";
        echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/components/signup.js\"></script> \n";
        echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/utilities.js\"></script> \n";
        echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/index.js\"></script> \n";
        echo "<!-- END CUSTOM --> \n";
    } else {
        switch ($include) {
            case 'prototype':
                echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/framework/prototype.js\"></script> \n";
                break;
            case 'scriptaculous':
                echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/framework/scriptaculous.js\"></script> \n";
                break;
            case 'account':
                echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/components/account.js\"></script> \n";
                break;
            case 'graphs';
                echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/components/graphs.js\"></script> \n";
                break;
            case 'projects':
                echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/components/projects.js\"></script> \n";
                break;
            case 'signup':
                echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/components/signup.js\"></script> \n";
                break;
            case 'utilities':
                echo "<script type=\"text/javascript\" src=\"" . $prefix . "js/utilities.js\"></script> \n";
                break;
        }
    }

}

function recursivelyRemoveDir($base) {

    if (is_dir($base)) {
        if ($base == "/") {
            break;
        }
        if ($base == "/root") {
            break;
        }
        $contents = scandir($base);
        foreach ($contents as $item) {
            // first make sure we exclude up/current dir links
            if ($item != "." && $item != "..") {
                if (filetype($base."/".$item) == "dir") {
                    recursivelyRemoveDir($base . "/" . $item);
                } else {
                    if ($DEBUG) {
                        $genericLog->debug("Deleting... " + $base . "/" . $item);
                    }
                    unlink($base . "/" . $item);
                }
            }
        }
        // rewind content list
        reset($contents);
        rmdir($base);
    }
}

?>
