<?php

class Database {

    var $conn;
    private $dbConn;
    private $db;
    private $dbUser;
    private $dbType;
    private $dbHost = "localhost";
    private $dbPass;
    private $dbPath = "/tmp/beacon.db";
    private $dbMessage = "";

    function __construct($setDB,$setDBUser,$setDBPass,$setDBType) {

        // pycrawnx
        $this->db = $setDB;
        // pycrawnx
        $this->dbUser = $setDBUser;
        // password
        $this->dbPass = $setDBPass;
        // MySQL
        $this->dbType = $setDBType;

    }

    function __destruct() {

        // Make sure we close the open handle
        if ($this->dbType == "MySQL") {
            // Check to see if we are connected
            if ($this->conn) {
                $this->conn = NULL;
            }
        }

    }

    private function setDB($dbName) {

        $this->db = $dbName;

    }

    private function setDBUser($dbUserName) {

        $this->dbUser = $dbUserName;

    }

    private function setDBPassword($dbPassword) {

        $this->dbPass = $dbPassword;

    }

    private function setDBType($dbTypeName) {

        $this->dbType = $dbTypeName;

    }

    function getHandle() {

        return $this->conn;

    }

    function setup() {
        switch($this->dbType){
            case "MySQL":
                $this->dbConn = "mysql:host=$this->dbHost;dbname=$this->db";
                break;
            case "SQLite":
                //$this->dbConn = "sqlite:$this->dbPath";
                $this->dbConn = NULL;
                $this->dbMessage = "SQLITE: Not implemented.";
                break;
            case "PostgreSQL":
                //$this->dbConn = "pgsql:host=$this->dbHost dbname=$this->db";
                $this->dbConn = NULL;
                $this->dbMessage = "POSTGRESQL: Not implemented.";
                break;
            default:
                $this->dbConn = NULL;
                $this->dbMessage = "You must have set an invalid type of backend.";
                break;
        }

        try {
            $this->conn = new PDO($this->dbConn,$this->dbUser,$this->dbPass);
        } catch (PDOException $connectException) {
            echo('CONNECT ERROR: ' . $connectException->getMessage());
            // AMD - 021210 - Removed so error processing in login.php works.
            //die($this->dbMessage);
        }
        
    }

    function teardown() {

        if ($this->dbType == "MySQL") {
            if ($this->conn) {
                $this->conn = NULL;
            }
        }

    }

}

?>