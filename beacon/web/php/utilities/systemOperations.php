<?php

function get_latest_file_count($filename,$dir) {

    $revisionNumber = 0;
    $handle = opendir($dir);
    if ($handle) {
        while (false !== ($file = readdir($handle))) {
            $match_statement = '/^[0-9]*-' . $filename . '/';
            if (preg_match($match_statement, $file)) {
                    $revisionNumber++;
                }
        }
        closedir($handle);
    }
    return $revisionNumber;
}

function process_is_running($pid) {
    exec("ps $pid", $state);
    if (count($state) >= 2) {
        return (bool) TRUE;
    } else {
        return (bool) "";
    }
}

?>