<?php

class File {

    public $fileName;
    public $type;
    private $fileDirPath;
    private $fullFileName;
    private $tmpFileName;
    
    function __construct($nameFile,$tmpNameFile,$destDir) {

        $this->setName($nameFile);
        // Validate path
        if (is_dir($destDir)) {
            $this->setDirPath($destDir);
        } else {
            $error = "$destDir is not a valid directory!";
            Throw new Exception($error);
        }

        // set full path
        $checkIfSlash = substr($this->getName(),-1);
        if ($checkIfSlash == "/") {
            $this->fullFileName = $this->getName() + $this->getDirPath();
        } else {
            $this->fullFileName = $this->getName() + "/" + $this->getDirPath();
        }

        $this->tmpFileName = $tmpNameFile;

    }

    function __destruct() {

    }

    private function setName($fname) {

        $this->fileName = $fname;
        return True;

    }

    public function getName() {

        return $this->fileName || NULL;

    }

    public function getDirPath() {

        $dirPath = $this->fullFileName;
        return  $dirPath || NULL;

    }

    private function setDirPath($dpath) {

        $this->fileDirPath = $dpath;

    }

    public function save() {

        if (file_exists($this->fullFileName) ) {
            $nextFileName = $this->getNextFileName();
            try {
                rename($this->fullFileName, $nextFileName);
            } catch (Exception $moveFileException) {
                echo "ERROR in file.php: $moveFileException->getMessage()";
            }
        }

        try {
            move_uploaded_file($this->tmpFileName,$this->fullFileName);
            echo "$this->fullFileName saved!";
        } catch (Exception $fileUploadError) {
            echo "ERROR in file.php: $fileUploadError->getMessage()";
        }

    }

    private function getNextFileName() {

        try {
            $dirHandle = opendir($this->getDirPath());
            $matchingFiles = array();

            while (false !== ($fileInDir = readdir($dirHandle))) {
                if (preg_match("/$this->fileName/i",$fileInDir)) {
                    array_push($matchingFiles,$fileInDir);
                }
            }

            $numFiles = count($matchingFiles);

            if (numFiles == 1) {
                return $this->fullFileName + "_0";
            } else {
                $fileCount = $numFiles + 1;
                return $this->fullFileName + "_" + $fileCount;
            }
        } catch (Exception $fileUploadError) {
            echo "ERROR in file.php: $fileUploadError->getMessage()";
        }

    }

    public function getMimeType() {

        return mime_content_type($this->fullFileName);

    }

}

?>
