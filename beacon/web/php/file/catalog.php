<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');
require('../utilities/systemOperations.php');

// Logger
$catalogFileLog = Logger::getRootLogger();
$DEBUG = 1;

// Variables given
$projid = $_GET['id'];
$projrev = $_GET['rev'];

// Database modification handle
$projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$projectBackend->setup();

// We're connected to the data store
$handle = $projectBackend->getHandle();

if ($handle) {

    // get name,path,module
    $getFileInfoQuery = $getFileForProjectBegin . $projid . $getFileForProjectEnd;

    if ($DEBUG) {
        $catalogFileLog->debug($getFileInfoQuery);
    }

    $fileInfoPrepared = $handle->prepare($getFileInfoQuery);
    $fileInfoPrepared->execute();
    $fileInfo = $fileInfoPrepared->fetchAll();

    if ($DEBUG) {
        $catalogFileLog->debug($fileInfo);
    }

    $fileName = $fileInfo[0]["name"];
    $fileModule = $fileInfo[0]["module"];
    $filePath = $fileInfo[0]["path"];

    if ($DEBUG) {
        $catalogFileLog->debug("NAME: " . $fileName . ", MODULE: " . $fileModule . ", PATH: " . $filePath );
    }

    $srcPathParts = explode($fileName,$filePath);
    $srcPathParts = explode("/" . $projrev . "-",$srcPathParts[0]);
    $srcPath = $srcPathParts[0];

    // TODO: Make src and dest the proper options based on our config file
    $destPath = str_replace("src","dest",$srcPath) . "/" . $projrev;

    if ($DEBUG) {
        $catalogFileLog->debug("SOURCE PATH: " . $srcPath);
        $catalogFileLog->debug("DEST PATH: " . $destPath);
    }

    // We get the following from our query:
    // | linux-1.0.tar.gz | Linux  | /opt/beacon/src/dutko.adam@gmail.com/Linux/1-linux-1.0.tar.gz |
    //
    // Let's simply remove the 1-linux-1.0.tar.gz ... $rev-$file ... then sub src with dest and append
    // the rev to the final path then expand into that directory
    //
    // We can keep expand over and over again just fine.
    //python beacon.py -x --file=1-linux-1.0.tar.gz --baseDir=/opt/beacon --owner=dutko.adam@gmail.com --project=Linux

    // TODO: Need to have a config option for script base
    $scriptBase = "/opt/projects/beacon/";
    $schema = "/opt/projects/beacon/db/schemata.xml";
    
    $expandArchive = "python " . $scriptBase . "beacon.py -x --file=" .
                     $projrev . "-" . $fileName . " --baseDir=" . "/opt/beacon --owner=" .
                     $_SESSION["user"] . " --project=" . $fileModule .
                     " --schema=" . $schema;
    $escapedCommand = escapeshellcmd($expandArchive);
    $commandPID = shell_exec("nohup $escapedCommand > /dev/null 2> /dev/null & echo $!");

    if ($DEBUG) {
        $catalogFileLog->debug($escapedCommand);
        $catalogFileLog->debug("PID: " . $commandPID);
    }

    // because this is async we could have a super fast command complete before
    // we can check its' state
    
    // status, projectid, desc, file, pid
    $jobCreateQuery = $createJobStatusStart . "0" . $createJobStatusProjectId .
                      $projid . $createJobStatusDesc . $fileModule .
                      $createJobStatusFile . $projrev . "-" . $fileName .
                      $createJobStatusPid . $commandPID . $createJobStatusEnd;

    $processStartedStatus = process_is_running($commandPID);
    if ($processStartedStatus) {
        // update the job table

        if ($DEBUG) {
            $catalogFileLog->debug("QUERY: " . $jobCreateQuery);
            $catalogFileLog->debug("PROCESS STATUS: " . $processStartedStatus);
        }

        $prepareJobCreate = $handle->prepare($jobCreateQuery);
        $prepareJobCreate->execute();

        echo("Started Archive Processing<br /> PID: $commandPID <br />");

    } else {
        if ($DEBUG) {
            $catalogFileLog->debug($processStatus);
            $catalogFileLog->debug($jobCreateQuery);
        }

        echo("Processing failed.");
    }
    
}

?>
