<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

// Logger
$listFileLog = Logger::getRootLogger();
$DEBUG = 1;
$noArchives = FALSE;

// Variables given
$projid = $_GET['projnum'];
$projShortName = $_GET['projshortname'];

// Database modification handle
$projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$projectBackend->setup();

// We're connected to the data store
$handle = $projectBackend->getHandle();

// Get files for a particular project
$fileList = $getFilesForProjectBegin . $projid . $getFilesForProjectEnd;

if ($DEBUG) {
    $listFileLog->debug($fileList);
}

$fileCount = 0;
$noFiles = "<tr id=\"projArchive\"></tr>";
$fileResults = "";

if ($handle) {

    $fileCount = 0;
    $preparedFiles = $handle->prepare($fileList);
    $preparedFiles->execute();
    $files = $preparedFiles->fetchAll();
    $fileCount = count($files);
    if ($fileCount == 0) {
        $noArchives = TRUE;
    } else {
        foreach ($files as $file)
        {
            $nextFile = "<tr id=\"projArchive" . $fileCount . "\">" .
                    "<td class=\"projArchiveName\">" . $file['name'] . "</td>" .
                    "<td class=\"projArchiveRev\">" . $file['revision'] . "</td>" .
                    "<td class=\"projArchiveFileOperations\">
                        <a class=\"unlinkArchive\" onclick=\"unlinkFile('" . $projid . "','" . $file['revision'] . "','" . $file['name'] . "','" . $projShortName . "');\">UNLINK</a>
                        <a class=\"catalogArchive\" onclick=\"catalogFile('" . $projid . "','" . $file['revision'] . "');\">CATALOG</a>" .
                    "</td>" .
                "</tr>";
            $fileResults = $fileResults . $nextFile;
            $fileCount++;
        }

        if ($DEBUG) {
            $listFileLog->debug($fileResults);
        }
    }
    
    // Always remember to cleanup our database connection
    $projectBackend->teardown();

    // Really make sure cleanup is done
    $projectBackend = NULL;

}

if ($noArchives) {
    if ($DEBUG) {
        $listFileLog->debug("No attached archives for Project #" . $projid . ".");
    }
    echo $noFiles;
    echo "<script type=\"text/javascript\">
          updateNoArchivesMessage(\"No archives attached to this project.\");
     </script>";
} else {
    if ($DEBUG) {
        $listFileLog->debug($fileResults);
    }
    echo $fileResults;
    echo "<script type=\"text/javascript\">
          hideNoArchivesMessage();
     </script>";
}

?>
