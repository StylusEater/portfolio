<?php

require('../utilities/connection.php');
require('../settings/config.php');

// 0 = off, 1 = on
$DEBUG = 1;
// Log instance
$getSizeLog = LOGGER::getRootLogger();

// POSTED DATA
$filename = $_GET["filename"];

$size = exec("ls -s " . $filename . " | awk '{ print $1 }'");

if ($DEBUG) {
    $getSizeLog->debug($filename . " has a size of " . $size . ".");
}

echo $size;

?>
