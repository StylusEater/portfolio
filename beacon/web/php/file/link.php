<?php

require('../settings/config.php');
require('../utilities/connection.php');
require('../utilities/queries.php');
require('../utilities/file.php');
require('../utilities/systemOperations.php');
require('../utilities/generic.php');

// 0 = off, 1 = on
$DEBUG = 1;
// Log instance
$linkFileLog = LOGGER::getRootLogger();

// POSTED DATA
$name = $_POST["projnameFile"];
$id = $_POST["projidFile"];
$type = $_POST["projtypeValueFile"];
$file = $_FILES["projfile"];
$projOwner = $_SESSION["user"];

if ($DEBUG) {
    $linkFileLog->debug($name . " " . $id . " " . $type . " " . $file . " " . $projOwner);
    $linkFileLog->debug($_POST);
    $linkFileLog->debug($_FILES);
}

$uploadError = FALSE;

// Verify a file was passed, if not we send an error message.
if (!$file['name']) {
    // NOTE: If you find yourself debugging why files aren't uploading, it's
    //       most likely that you didn't adjust your php.ini file to accommodate
    //       large file size uploads.
    $linkFileLog->error("No file specified. Received: " . count($_FILES));
    $status = "<span class=\"message\">You didn't specify a file.</span><br />";
    $uploadError = TRUE;
} else {
    // Verify we have a valid filetype
    // Don't rely on $file["type"]
    $type = exec("file -bi " . $file["tmp_name"]);
}

if (!in_array($type,$FILETYPES) && !$uploadError) {
    $linkFileLog->info($projOwner . " attempted to upload an invalid file type :: " . $file["type"]);
    $status = $status . "<span class=\"message\">Sorry but you specified an invalid filetype.</span><br />";
    $uploadError = TRUE;
}

if (!$uploadError) {
    // If we need information about the file enable DEBUG...
    if ($DEBUG) {
        $linkFileLog->debug("NEW UPLOAD::: ");
        $linkFileLog->debug("PROJECT NAME: " . $name);
        $linkFileLog->debug("PROJECT ID: " . $id);
        $linkFileLog->debug("PROJECT FILENAME: " . $file["name"]);
        $linkFileLog->debug("PROJECT FILETYPE: " . $type);
        $linkFileLog->debug("PROJECT FILETMPNAME: " . $file["tmp_name"]);
        $linkFileLog->debug("PROJECT FILE SIZE: " . $file["size"]);
        $linkFileLog->debug("PROJECT FILE ERROR: " . $file["error"]);
        $linkFileLog->debug("PROJECT OWNER: " . $projOwner);
        $linkFileLog->debug("END UPLOAD::: ");
    }

    // Project database handle
    $projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

    // Setup backend connection
    $projectBackend->setup();

    // We're connected to the data store
    $handle = $projectBackend->getHandle();

    if ($handle) {

        if ($projOwner != '') {

            // create the project owner upload directory
            $userSrcDir = $baseFileDir . "/" . $fileSrcDir . "/" . $projOwner;
            if (!is_dir($userSrcDir)) {
                // TODO: Need to lock this down more.
                if (!mkdir($userSrcDir,0777)) {
                    $linkFileLog->info(error_get_last());
                    $status = error_get_last();
                } else {
                    $linkFileLog->info("Created user directory: " . $userSrcDir);
                }
            }

            $projDir = $userSrcDir . "/" . $name;
            if (!is_dir($projDir)) {
                // TODO: Need to lock this down more.
                if (!mkdir($projDir,0777)) {
                    $linkFileLog->info(error_get_last());
                    $status = error_get_last();
                } else {
                    $linkFileLog->info("Created user project directory: " . $projDir);
                }
            }

            $getUserIdQuery = $getUserIdBegin . $projOwner . $getUserIdEnd;
            $getUserId = $handle->prepare($getUserIdQuery);
            if ($getUserId->execute()) {
                $developer = $getUserId->fetch();
                if (count($developer) > 2) {
                    $linkFileLog->error($getUserId->errorInfo());
                    $linkFileLog->error("Many developers returned for " . $projOwner . ".  FOUND: " . count($developer));
                    $status = "Many developers returned for " . $projOwner . ".";
                } else {
                    $devid = $developer["id"];
                }
            } else {
                $linkFileLog->error($getUserId->errorInfo());
                $status = $getUserId->errorInfo();
            }

            $getHighestRevQuery = $getFilesForProjectMaxRevBegin . $id .
                                  $getFilesForProjectMaxRevMod . $name .
                                  $getFilesForProjectMaxRevEnd;

            $getHighestRev = $handle->prepare($getHighestRevQuery);
            if ($getHighestRev->execute()) {
                $last = $getHighestRev->fetch();
                if (count($last) > 0) {
                    $revision = $last["last"] + 1;
                } else {
                    $revision = 0;
                }
            } else {
                $linkFileLog->debug("Cannot determine the latest revision. " . $getHighestRevQuery);
                throw new Exception("<span class=\"message\">Failed to get latest revision.</span>");
            }

            
            $path = $projDir . "/" . $revision . "-" . $file["name"];

            $addFileQuery = $insertFileProjectBegin . $file["name"] . "','" . $id . "','" . $devid . "','" . $name . "','" . $path . "','" . $revision . $insertFileProjectEnd;

            // Update Project
            $updateProjectQuery = $updateProjectUpdatedStart . $id . $updateProjectUpdatedEnd;

            if ($DEBUG) {
                $linkFileLog->debug($addFileQuery);
            }

            try {
                // Add File
                $handle->beginTransaction();
                $addFile = $handle->prepare($addFileQuery);
                $addFile->execute();

                // Update Project
                $updateProject = $handle->prepare($updateProjectQuery);
                $updateProject->execute();

                if (move_uploaded_file($file["tmp_name"], $path)) {
                    $handle->commit();
                    $status = $status . "<span class=\"message\">Successfully attached archive.</span><br />";
                } else {
                    throw new Exception("<span class=\"message\">Failed uploading " . $path . ". " . $addFile->errorInfo() . "</span>");
                }
                if ($DEBUG) {
                    $linkFileLog->debug("Committed file and wrote to filesystem.");
                }
            } catch (Exception $fileuploadException) {
                $handle->rollback();
                $linkFileLog->error("Failed uploading " . $path);
                $linkFileLog->error($addFile->errorInfo());
                $status = "<span class=\"message\">Failed uploading " . $path . ". " . $addFile->errorInfo() . "</span>";
            }

        } else {
            $linkFilelog->error("The project owner is not set but somehow they were
                able to upload a file???");
            $status = $status . "<span class=\"message\">Project owner is not set!</span><br />";
        }

        // Always remember to cleanup our database connection
        $projectBackend->teardown();

        // Really make sure cleanup is done
        $projectBackend = NULL;

    } else {
        $status = $status . "<span class=\"message\">Unable to connect to the backend.</span><br />";
    }
}

echo $status;

?>

