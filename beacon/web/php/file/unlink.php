<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

// Logger
$unlinkLog = Logger::getRootLogger();
$DEBUG = 1;

// Variables given
$projid = $_POST['id'];
$projrev = $_POST['rev'];
$projname = $_POST['name'];
$projShortName = $_POST['shortname'];
$user = $_SESSION['user'];
$userid = $_SESSION['userid'];

// Database modification handle
$projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$projectBackend->setup();

// We're connected to the data store
$handle = $projectBackend->getHandle();

// Get files for a particular project
$unlinkFileQuery = $unlinkFilesForProjectBegin . $projid . $unlinkFilesForProjectRevision . $projrev . $unlinkFilesForProjectDev . $userid . $unlinkFilesForProjectName . $projname . $unlinkFilesForProjectEnd;
$updateProjectQuery = $updateProjectUpdatedStart . $projid . $updateProjectUpdatedEnd;

if ($DEBUG) {
    $unlinkLog->debug($unlinkFileQuery);
}

if ($handle) {

    try {
        // File Query
        $handle->beginTransaction();
        $preparedUnlink = $handle->prepare($unlinkFileQuery);
        $preparedUnlink->execute();
        $unlinkStatus = $preparedUnlink->fetchAll();
        $unlinkCount = count($unlinkStatus);

        // Project Query
        $preparedProject = $handle->prepare($updateProjectQuery);
        $preparedProject->execute();
        
        $path = $baseFileDir . "/" . $fileSrcDir . "/" . $user . "/" . $projShortName . "/" . $projrev . "-" . $projname;

        if ($DEBUG) {
           $unlinkLog->debug("Unlinked File Status: " . $unlinkCount);
        }

        if (($unlinkCount == 0) && unlink($path)) {

            $status = "<span class=\"message\">Unlinked " . $projname . " revision #" . $projrev . " successfully.</span>";
            if ($DEBUG) {
                $unlinkLog->debug($status);
            }
            $handle->commit();
        }
       
    } catch (Exception $unlinkException) {
        $handle->rollback();
        $unlinklog->error($preparedUnlink->errorInfo());
        $status = "<span class=\"message\">Error unlinking: " . $unlinkCount . "</span>";
        if ($DEBUG) {
           $unlinkLog->debug($status);
        }
    }
    
    // Always remember to cleanup our database connection
    $projectBackend->teardown();

    // Really make sure cleanup is done
    $projectBackend = NULL;

}

echo $status;

?>