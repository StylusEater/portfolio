<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN"
    "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<title>Beacon - Sign Up</title>
<link type="text/css" rel="stylesheet" href="/static/css/signup.css" />
<!-- BEGIN FRAMEWORK JAVASCRIPT -->
<script src="/js/framework/prototype.js" type="text/javascript"></script>
<script src="/js/framework/scriptaculous.js" type="text/javascript"></script>
<!-- END FRAMEWORK JAVASCRIPT -->
<!-- BEGIN CUSTOM JAVASCRIPT -->
<script src="/js/components/signup.js" type="text/javascript"></script>
<!-- END CUSTOM JAVASCRIPT -->

<!-- Some code adapted from: http://wiki.github.com/madrobby/scriptaculous/tabs -->
</head>
<body>

<!-- Signup Results -->
<table class="signupresults">
    <tr class="signupresults">
        <td class="signupresults" id="signuptitle">Beacon Sign Up Results</td>
    </tr>
    <tr class="signupresults">
        <td id="result">

<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

$fname = trim($_POST['firstname']);
$lname = trim($_POST['lastname']);
$login = trim($_POST['login']);

// Basic form validation
if ($fname == '') {
    echo('<p class="signupresutls message">Please enter a value for <strong>First Name: </strong></p>');
    echo('<p class="signupresults message"><a class="redo" onclick="getSignupFormAgain();">Try again?</a></p>');
    die();
}
if ($lname == '') {
    echo('<p class="signupresutls message">Please enter a value for <strong>Last Name: </strong></p>');
    echo('<p class="signupresults message"><a class="redo" onclick="getSignupFormAgain();">Try again?</a></p>');
    die();
}
if ($login == '') {
    echo('<p class="signupresutls message">Please enter a value for <strong>Login: </strong></p>');
    echo('<p class="signupresults message"><a class="redo" onclick="getSignupFormAgain();">Try again?</a></p>');
    die();
}

// Global signup database handle
$signupBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$signupBackend->setup();

// We're connected to the data store
$handle = $signupBackend->getHandle();

if ($handle) {

    // Use as temporary password for the new user
    $tempPass = substr(md5(time()),0,6);
    //echo('<p id="message">' . $tempPass . '</p>');

    // Remote address of user
    $userIP = $_SERVER['REMOTE_ADDR'];

    // Check if desired login already exists
    $userCheck = $defaultCheckUserExistsLogin . $login . "'";
    $userCheckQuery = $handle->query($userCheck);
    $userCheckQueryFetch = $userCheckQuery->fetch();
    $userCheckCount = $userCheckQueryFetch['cnt'];

    if ($userCheckCount != 0) {
        echo('<p class="signupresults message">A user with the name ' . $login . ' already exists.</p>');
        echo('<p class="signupresults message"><a class="redo" onclick="getSignupFormAgain();">Try again?</a></p>');
    } else {
        // Attempt to register the user
        $createUserQuery = $defaultCreateUserPartOne . $login .
                           $defaultCreateUserPartTwo . $tempPass .
                           $defaultCreateUserPartThree . $fname .
                           $defaultCreateUserPartFour . $lname .
                           $defaultCreateUserPartFive . "INET_ATON('" . $userIP . "')";

        //echo('<p id="message">' . $createUserQuery . '</p>');
        $createUserStatus = $handle->exec($createUserQuery);

        //echo('<p id="message">' . $createUserStatus . '</p');
        //NOTE: There is a problem with IPV6 address being used that causes the
        //      IP column to be null -- silent error.
        //      The solution is to use http://<system's IP> instead of localhost.
        if ($createUserStatus == 0) {
            //mail('beacon@oktud.com',"Beacon: Account Signup Failure",$createUserQuery, "From: Beacon Support <beacon@oktud.com>");
            mail('dutko.adam@gmail.com',"Beacon: Account Signup Failure",$createUserQuery, "From: Beacon Support <beacon@oktud.com>");
            echo('<p class="signupresults message">Error creating account, please contact support at beacon@oktud.com.</p>');
        }

        if ($createUserStatus == 1) {
            // Email the new password to the person.
            $message = $signupSuccessMessagePartOne . $login .
                       $signupSuccessMessagePartTwo . $tempPass .
                       $signupSuccessMessagePartThree;

            echo('<p class="signupresults message">' . $inPlaceSignupSuccess . '</p>');

           // mail('beacon@oktud.com',"Beacon: New Account","Please enable $login's account.", "From: Beacon Support <beacon@oktud.com>");
             mail('dutko.adam@gmail.com',"Beacon: New Account","Please enable $login's account.", "From: Beacon Support <beacon@oktud.com>");
            mail($login,"Beacon: Account Information",
                 $message, "From: Beacon Support <beacon@oktud.com>");
        }

    }

    // Always remember to cleanup our database connection
    $signupBackend->teardown();

    // Really make sure cleanup is done
    $signupBackend = NULL;

} else {

    // Couldn't connect for some reason.
    echo('<p class="signupresults" id="message">Cannot connect to the backend datastore.  Please contact the administrator.</p>');
}

?>

        </td>
    </tr>
</table>

</body>
</html>
