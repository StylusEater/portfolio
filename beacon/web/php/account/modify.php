<?php

session_start();

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

// Database modification handle
$modifyBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$modifyBackend->setup();

// We're connected to the data store
$handle = $modifyBackend->getHandle();

if ($handle) {

    // Construct query to retrieve user data and display it
    $getUserData = $retrieveUserDataBeginOfQuery . $_SESSION['user'] . $retrieveUserDataEndOfQuery;

    // DEBUG
    //echo($getUserData);

    // TODO
    // Need some better checks to ensure only one user's data are returned.
    foreach ($handle->query($getUserData) as $accountDetails)
    {

        $userLogin = $accountDetails['login'];
        $userFName = $accountDetails['fname'];
        $userLName = $accountDetails['lname'];

    };

    // Always remember to cleanup our database connection
    $modifyBackend->teardown();

    // Really make sure cleanup is done
    $modifyBackend = NULL;

} else {

    // Couldn't connect for some reason.
    echo('Cannot connect to the backend datastore.  Please contact the administrator (beacon@dsnrg.org).');
}

echo json_encode(array("userLogin" => $userLogin, "userFname" => $userFName, "userLname" => $userLName));

?>