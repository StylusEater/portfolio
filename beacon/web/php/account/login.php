<?php

include_once('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');


// Establish a connection to the backend
$backendLogin = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);
$backendLogin->setup();
// We're connected to the data store
$handle = $backendLogin->getHandle();

$login = $_POST['login'];
$pass = $_POST['password'];

if ($handle) {

    $userLoginCheckQuery = $defaultCheckUserExistsLogin . $login . $defaultCheckUserExistsPassword . $pass . $defaultCheckUserExistsSalt;
    $loginCheck = $handle->query($userLoginCheckQuery);
    if ($loginCheck) {
        $loginCheckResult = $loginCheck->fetch();
        $loginResponse = '';
    } else {
        $loginCheckResult = array();
    }

    // Debug
    //echo("Number of users --> " . count($loginCheckResult['cnt']));

    if ($loginCheckResult['cnt']) {

        if ($loginCheckResult['verified']) {

            $loginResponse = 'True';
            session_start();
            $_SESSION['user'] = $login;
            $_SESSION['userid'] = $loginCheckResult['cnt'];

            // Debug
            //echo("USER: " . $loginCheckResult['login']);

            // Need to generate a routine to populate on load of INDEX tab then I
            // need to generate routines to retrieve content for the user on click
            // of each tab.  Need to figure out what the deal is with persistent
            // sessions in a JS environment... I suspect once we set the $_SESSION
            // variable we should be good if we refresh the page, but we're trying
            // to avoid that after the page is first loaded...we could reload just
            // once after a successful login but not on a failure...
            
        } else {
            $loginResponse = "Your account is not yet activated.  Contact beacon@dsnrg.org for help.";
        }
    } else {
        $loginResponse = "Invalid login or password!";
    }

    // Always remember to cleanup
    $backendLogin->teardown();

    // Really make sure cleanup is done
    $backendLogin = NULL;

    // Used for sending data to the ajax call for login checks
    echo($loginResponse);
    
} else {
    echo("ERROR: Please contact beacon@dsnrg.org.");
}

?>
