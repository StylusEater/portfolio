<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

// 0 = off, 1 = on
$DEBUG = 0;
// Log instance
$accountUpdateLog = LOGGER::getRootLogger();

$loginVal = $_POST["login"];
$fnameVal = $_POST["fname"];
$lnameVal = $_POST["lname"];
$passVal = $_POST["pass"];
$passConfirmVal = $_POST["passConfirm"];

// Global signup database handle
$updateAccountBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$updateAccountBackend->setup();

// We're connected to the data store
$handle = $updateAccountBackend->getHandle();

if ($handle) {

    if ($passVal != '') {

       if($passConfirmVal != '') {

            $updateAccountQuery = $updateAccountFname . $fnameVal . $updateAccountLname . $lnameVal . $updateAccountPass . $passVal . "') where login='" . $loginVal . "';";

        } else {

            $updateAccountQuery = $updateAccountFname . $fnameVal . $updateAccountLname . $lnameVal . $updateWhereClause . $loginVal . "';";

        }

        // DEBUG
        if ($DEBUG) {
            $accountUpdateLog->debug("QUERY --> " . $updateAccountQuery);
        }

        $updateUserStatus = $handle->prepare($updateAccountQuery);
        $updateUserStatus->execute();
        $updateAccountResult = $updateUserStatus->fetchAll();

        // DEBUG
        if ($DEBUG) {
            $accountUpdateLog->debug($updateAccountResult);
        }

        if (count($updateAccountResult) == 0) {

            // DEBUG
            if ($DEBUG) {
                $accountUpdateLog->debug("STATUS --> " . $updateAccountResult);
                $accountUpdateLog->debug("COUNT --> " . count($updateAccountResult));
            }

            echo('Your profile was updated successfully!');

        } else if (count($updateAccountResult) != 0) {

            // DEBUG
            if ($DEBUG) {
                $accountUpdateLog->debug("STATUS --> " . $updateAccountResult);
                $accountUpdateLog->debug("COUNT --> " . count($updateAccountResult));
            }

            echo('There was a problem updating your user profile.  <br />');
            
            // DEBUG
            if ($DEBUG) {
                $accountUpdateLog->debug("ERROR --> " . $updateUserStatus->errorInfo());
            }

        }

    }
    
    // Always remember to cleanup our database connection
    $updateAccountBackend->teardown();

    // Really make sure cleanup is done
    $updateAccountBackend = NULL;

} else {

    // Couldn't connect for some reason.
    echo('<p id="message">Cannot connect to the backend datastore.  Please contact the administrator.</p>');
}

?>