<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');
require('../utilities/generic.php');

// Logger
$deleteProjectLog = Logger::getRootLogger();
$DEBUG = 1;

$projID = $_POST["projectid"];
$projOwner = $_SESSION["user"];
$projShortName = $_POST['shortname'];
$user = $_SESSION['user'];

// Project database handle
$projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$projectBackend->setup();

// We're connected to the data store
$handle = $projectBackend->getHandle();

// Get files for a particular project
$deleteProjectQuery = $deleteProjectBegin . $projOwner . $deleteProjectAnd . $projID . $deleteProjectEnd;

if ($DEBUG) {
    $deleteProjectLog->debug($deleteProjectQuery);
}

if ($handle) {

    if ($projID != '') {

        try {
            $handle->beginTransaction();
            $preparedDeleteProject = $handle->prepare($deleteProjectQuery);
            $preparedDeleteProject->execute();
            $deleteProjectStatus = $preparedDeleteProject->fetchAll();
            $deleteCount = count($deleteProjectStatus);

            $path = $baseFileDir . "/" . $fileSrcDir . "/" . $user . "/" . $projShortName;

            if ($DEBUG) {
               $deleteProjectLog->debug("Delete Project Status: " . $deleteCount);
               $deleteProjectLog->debug("Attempting to delete " . $path);
            }

            if ($deleteCount == 0) {

                $status = "<span class=\"message\">Deletion of " . $projShortName . " successful.</span>";
                if ($DEBUG) {
                    $deleteProjectLog->debug($status);
                }
                $handle->commit();
                recursivelyRemoveDir($path);
            }

        } catch (Exception $unlinkException) {
            $handle->rollback();
            $deleteProjectLog->error($preparedDeleteProject->errorInfo());
            $status = "<span class=\"message\">Error deleting project #" . $projShortName . ".</span>";
            if ($DEBUG) {
               $deleteProjectLog->debug($status);
            }
        }
    } else {
        $status = $status . "<span class=\"message\">Project ID is not set!</span>";
    }

    // Always remember to cleanup our database connection
    $projectBackend->teardown();

    // Really make sure cleanup is done
    $projectBackend = NULL;

}

echo $status;

?>
