<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

$DEBUG = 1;

// Log instance
$newProjLog = LOGGER::getRootLogger();

$projName = $_POST["projname"];
$projType = $_POST["projtype"];
$projOwner = $_SESSION["user"];

// Various file information
//$fileName = $_FILES['newProject']['name'];
//$tmpFileName = $_FILES['newProject']['tmp_name'];
//$baseFileDir = $beaconDir;
//$fileSrcDir = $sdir;
//$fileDestDir = $ddir;

// Project database handle
$projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$projectBackend->setup();

// We're connected to the data store
$handle = $projectBackend->getHandle();

$status = "<p>Project addition failed.</p>";

if ($handle) {

    if ($projOwner != '') {

        $createProjectQuery = $insertProjectBegin . $projOwner . "','" . $projName . "','" . $projType . $insertProjectEnd;

        if ($DEBUG) {
            $newProjLog->debug($createProjectQuery);
        }

        $createProjectStatus = $handle->prepare($createProjectQuery);
        if ($createProjectStatus->execute()) {
             $status = "<p>Project created successfully!</p>";
        } else {
             // [0] -> MYSQL ERROR #
             // [1] -> PDO ERROR #
             // [2] -> PDO ERROR MESSAGE
             $PDOErrorInfo = $createProjectStatus->errorInfo();
             $status = "<p>" . $PDOErrorInfo[2] . "</p>"; 
        }

    } else {
        $status = $status . "<p>Project owner is not set!</p>";
        $newProjLog->debug($status);
    }

    // Always remember to cleanup our database connection
    $projectBackend->teardown();

    // Really make sure cleanup is done
    $projectBackend = NULL;


} else {
    $status = $status . "<p><b>Unable to connect to the backend.</b></p>";
}

echo $status;

?>
