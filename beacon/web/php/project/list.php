<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

$DEBUG = 1;

// Log instance
$listProjLog = LOGGER::getRootLogger();

// Database modification handle
$projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$projectBackend->setup();

// We're connected to the data store
$handle = $projectBackend->getHandle();

if ($handle) {

    // query for project information
    $getProjects = $getUsersProjectsBegin . $_SESSION['user'] . $getUsersProjectsEnd;
    $projectListQuery = $handle->query($getProjects);
    $projectList = $projectListQuery->fetchAll();
    $projCount = count($projectList);

    // DEBUG
    if ($DEBUG) {
        $listProjLog->debug($getProjects);
        if ($projCount == 0) {
            $projCountMsg = "NO PROJECTS FOR " . $_SESSION['user'];
        } else {
            $projCountMsg = "NUMBER PROJECTS FOR " . $_SESSION['user'] . " is " . $projCount;
        }
        $listProjLog->debug($projCountMsg);
    }

    if ($projCount > 0) {

        // Keep Track of What Project We're on
        $workingProj = 0;

        // DEBUG
        if ($DEBUG) {
            $listProjLog->debug("Started project iteration ...");
        }

        //DISPLAY PROJECTS
        echo("<fieldset id=\"projects\">");
        echo("<legend accesskey=\"I\">Projects</legend>");
        echo("<table id=\"projects\">");
        echo("<thead>");
        echo("<tr id=\"projectTitleBar\">");
        echo("<th class=\"heading deleteProjectHeading\"><label></label></td>");
        echo("<th class=\"heading\"><label></label></td>");
        echo("<th class=\"heading\"><label>ID</label></td>");
        echo("<th class=\"heading\" id=\"name\"><label>Name</label></td>");
        echo("<th class=\"heading\" id=\"update\"><label>Last Updated</label></td>");
        echo("<th class=\"heading\" id=\"archives\"><label>Archives</label></td>");
        echo("<th class=\"heading\" id=\"repostate\"><label>Repository State</label></td>");
        echo("</tr>");
        echo("</thead>");
        echo("<tbody>");

        foreach ($projectList as $project)
        {

            $projOwner = $project['owner'];
            $projCreated = $project['created'];
            $projUpdated = $project['updated'];
            $projName = $project['name'];
            $projID = $project['id'];
            $projType = $project['type'];
            $repoStatus = "IMPLEMENT ME";

            // query for project count
            $getProjectFileCount = $getFilesForProjectCountBegin . $projID . $getFilesForProjectCountEnd;
            $getProjectFileCountPrepared = $handle->prepare($getProjectFileCount);
            $getProjectFileCountPrepared->execute();
            $projectFileCountResult = $getProjectFileCountPrepared->fetch();
            $projectFileCount = $projectFileCountResult['total'];

            // Get files for a particular project
            $fileList = $getFilesForProjectBegin . $projID . $getFilesForProjectEnd;
            $preparedFiles = $handle->prepare($fileList);
            $preparedFiles->execute();
            $files = $preparedFiles->fetchAll();
            $fileCount = count($files);

            // DEBUG
            if ($DEBUG) {
                $listProjLog->debug("Working on Project #" . $workingProj . ".");
            }

            $workingProj++;

            echo("<tr id=\"projectInfo$workingProj\">");
            echo("<td class=\"deleteProjectBox\"><input type=\"button\" name=\"delectProject\" id=\"" . $projID . "\" value=\"DELETE\" onclick=\"deleteProject($(this).id,'" . $projName . "');\" />");
            echo("<td class=\"projectsMenu\">");
            echo("</td>");
            echo("<td class=\"data\" id=\"view" . $projID . "\" onclick=\"viewProject('" . $projID . "','" . $projName . "','" . $projType . "');\" onmouseover=\"colorize('view" . $projID . "','projectInfo" . $workingProj . "');\" onmouseout=\"uncolorize('view" . $projID . "','projectInfo" . $workingProj . "');\">$projID</td>");
            echo("<td class=\"data\">$projName</td>");
            echo("<td class=\"data\">$projUpdated</td>");
            if ($projType == "archive") {
                echo("<td class=\"data projectFileCount\">$projectFileCount</td>");
            } else {
                echo("<td class=\"data projectFileCount\">Not Applicable</td>");
            }
            if ($projType == "repository") {
                echo("<td class=\"data\">$repoStatus</td>");
            } else {
                echo("<td class=\"data\">Not Applicable</td>");
            }

            echo("</tr>");

        };
        echo("</tbody>");
        echo("</table>");
        echo("<br />");
        echo("</fieldset>");
        echo("</div>");

        // DEBUG
        if ($DEBUG) {
            $listProjLog->debug("Ended project iteration ...");
        }
    };

    // Always remember to cleanup our database connection
    $projectBackend->teardown();

    // Really make sure cleanup is done
    $projectBackend = NULL;

} else {

    // Couldn't connect for some reason.
    echo('<p id="message">Cannot connect to the backend datastore.  Please contact the administrator.</p>');
}

?>
