<?php

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');
require('../utilities/file.php');
require('../utilities/systemOperations.php');
require_once('../utilities/generic.php');

// 0 = off, 1 = on
$DEBUG = 1;
// Log instance
$modifylog = LOGGER::getRootLogger();

// POSTED DATA
$name = $_POST["projname"];
$id = $_POST["projid"];
$type = $_POST["projtypeValue"];
$file = $_FILES["projfile"];
$projOwner = $_SESSION["user"];

// Verify a file was passed, if not we send an error message.
if (!$file['name']) {
    $status = "<p>No file specified.</p>";
}

// Verify we have a valid filetype
if (!in_array($file["type"],$FILETYPES)) {
    $modifylog->info($projOwner . " attempted to upload an invalid file type :: " . $file["type"]);
    $status ="<p>Sorry but you specified an invalid filetype.</p>";
}

// If we need information about the file enable DEBUG...
if ($DEBUG) {
    $modifylog->debug("NEW UPLOAD::: ");
    $modifylog->debug("PROJECT NAME: " . $name);
    $modifylog->debug("PROJECT ID: " . $id);
    $modifylog->debug("PROJECT FILENAME: " . $file["name"]);
    $modifylog->debug("PROJECT FILETYPE: " . $file["type"]);
    $modifylog->debug("PROJECT FILETMPNAME: " . $file["tmp_name"]);
    $modifylog->debug("PROJECT FILE SIZE: " . $file["size"]);
    $modifylog->debug("PROJECT FILE ERROR: " . $file["error"]);
    $modifylog->debug("PROJECT OWNER: " . $projOwner);
    $modifylog->debug("END UPLOAD::: ");
}

// Project database handle
$projectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$projectBackend->setup();

// We're connected to the data store
$handle = $projectBackend->getHandle();

if ($handle) {

    if ($projOwner != '') {

        // create the project owner upload directory
        $userSrcDir = $baseFileDir . "/" . $fileSrcDir . "/" . $projOwner;
        if (!is_dir($userSrcDir)) {
            if (!mkdir($userSrcDir,0700)) {
                $modifylog->info(error_get_last());
            } else {
                $modifylog->info("Created user directory: " . $userSrcDir);
            }
        }

        $projDir = $userSrcDir . "/" . $name;
        if (!is_dir($projDir)) {
            if (!mkdir($projDir,0700)) {
                $modifylog->info(error_get_last());
            } else {
                $modifylog->info("Created user project directory: " . $projDir);
            }
        }

        $getUserIdQuery = $getUserIdBegin . $projOwner . $getUserIdEnd;
        $getUserId = $handle->prepare($getUserIdQuery);
        if ($getUserId->execute()) {
            $developer = $getUserId->fetch();
            if (count($devid) > 1) {
                $modifylog->error("Many developers returned for " . $projOwner . ".");
            } else {
                $devid = $developer[0];
            }
        } else {
            $modifylog->error($getUserId->errorInfo());
        }

        $revision = get_latest_revision($file["name"],$projDir);
        $path = $projDir . "/" . $revision . "-" . $file["name"];
        
        $addFileQuery = $insertFileProjectBegin . $file["name"] . "','" . $id . "','" . $devid . "','" . $name . "','" . $path . "','" . $revision . $insertFileProjectEnd;

        if ($DEBUG) {
            $modifylog->debug($addFileQuery);
        }

        try {
            $handle->beginTransaction();
            $addFile = $handle->prepare($addFileQuery);
            $addFile->execute();
            move_uploaded_file($file["tmp_name"], $path);
            $handle->commit();
        } catch (Exception $fileuploadException) {
            $addFile->rollback();
            $modifylog->error("Failed uploading " . $path);
            $modifylog->error($addFile->errorInfo());
        }

    } else {
        $modifylog->error("The project owner is not set but somehow they were
            able to upload a file???");
        $status = $status . "<p>Project owner is not set!</p>";
    }

    // Always remember to cleanup our database connection
    $projectBackend->teardown();

    // Really make sure cleanup is done
    $projectBackend = NULL;

} else {
    $status = $status . "<p><b>Unable to connect to the backend.</b></p>";
}

// status of the modification operation
//echo $status;

// refresh the project modification page
//load_js_include('',TRUE,'/');

echo "<script type='text/javascript'>";
echo "    refreshProject('" . $id . "','" . $name . "','" . $type . "');";
echo "</script>";

?>
