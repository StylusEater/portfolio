<?php
// Python Config File
$pyConfigFile = "/opt/beacon/config.xml";
if (file_exists($pyConfigFile)) {
    $pyXML = simplexml_load_file($pyConfigFile);

    foreach($pyXML->xpath('//config') as $globalPyConfig) {

        $pyConfig = simplexml_load_string($globalPyConfig->asXML());
        $beaconDir = $pyConfig->beaconDir["value"];
        $sdir = $pyConfig->bdir["value"];
        $ddir = $pyConfig->ddir["value"];
        $logdest = $pyConfig->logdir["value"] . $pyConfig->logfile["value"];
        $logpropdir = $pyConfig->logpropdir["value"];

    }
} else {
    print "ERROR: Cannot read the configuration file config.xml.";
    die(-1);
}

?>
