<?php

// Set the default timezone
date_default_timezone_set('US/Eastern');

require('pyXMLConfig.php');
require('../log4php/Logger.php');
require('../utilities/session.php');
Logger::configure($logpropdir.'log4php.properties');

// override the default logging option
if ($logdest != "") {
    Logger::getRootLogger()->getAppender('default')->setFile($logdest,true);
    Logger::getRootLogger()->getAppender('default')->activateOptions();
}

// Database
$dbBackend = "beacon";
$userBackend = "beacon";
$passBackend = "beaconAdmin";
$typeBackend = "MySQL";


// NOTE: We get the file configuration settings from the python piece using
//       simple xml -- See pyXMLConfig.php
$baseFileDir = $beaconDir;
$fileSrcDir = $sdir;
$fileDestDir = $ddir;

// Accepted MimeTypes
$FILETYPES = array(
   "application/x-tar",
   "application/zip",
   "application/bzip2",
   "application/x-bzip2",
   "application/octet-stream",
   "application/gzip",
   "application/x-gzip",
   "application/tar"
);

// Signup confirmation message
$signupSuccessMessagePartOne = "Your personal account for Beacon is now setup!
To log in, proceed to the following address:

   http://beacon.oktud.com/

Your personal login ID and password are as
follows:

   Login: ";

$signupSuccessMessagePartTwo = "
   Password: ";

$signupSuccessMessagePartThree = "

You aren't stuck with this password! You can
change it at any time after you have logged in.

If you have any problems, feel free to contact the support
team at beacon@dsnrg.org.

Sincerely,

   Adam M. Dutko
{Beacon Creator}
";


// In place signup confirmation message
$inPlaceSignupSuccess = "
Your personal account for Beacon is now setup!<br /><br />
Please check your e-mail for login instructions!<br /><br />
<a class=\"redo\" onclick=\"loginAfterSignup();\">Login</a>";


?>
