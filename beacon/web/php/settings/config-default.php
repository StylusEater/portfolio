<?php

require('pyXMLConfig.php');

// Database
$dbBackend = "CHANGEME";
$userBackend = "CHANGEME";
$passBackend = "CHANGEME";
$typeBackend = "MySQL";


// NOTE: We get the file configuration settings from the python piece using
//       simple xml -- See pyXMLConfig.php
$baseFileDir = $beaconDir;
$fileSrcDir = $sdir;
$fileDestDir = $ddir;


// Signup confirmation message
$signupSuccessMessagePartOne = "Your personal account for Beacon is now setup!
To log in, proceed to the following address:

   http://beacon.thefuturegrid.org/

Your personal login ID and password are as
follows:

   Login: ";

$signupSuccessMessagePartTwo = "
   Password: ";

$signupSuccessMessagePartThree = "

You aren't stuck with this password! You can
change it at any time after you have logged in.

If you have any problems, feel free to contact the support
team at beacon@burningriversoftware.com.

Sincerely,

   Adam M. Dutko
{Beacon Creator}
";


// In place signup confirmation message
$inPlaceSignupSuccess = "
Your personal account for Beacon is now setup!
<p>Please check your e-mail for login instructions!</p>
<p><a style=\"color: #ffffff; a:hover: none; text-decoration: none;\" href=\"/\">Login</a></p>";


?>