<?php

session_start();

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

// Database modification handle
$searchectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$searchectBackend->setup();

// We're connected to the data store
$handle = $searchectBackend->getHandle();

if ($handle) {

    // Construct query to retrieve user data and display it
    $getsearch = $getUserssearchBegin . $_SESSION['user'] . $getUserssearchEnd;

    // DEBUG
    //echo($getsearch);

    $searchCount = 0;

    echo("<p id=\"searchTitle\">SEARCH</p>");
    echo("<div id=\"searchMgmt\">");
    echo("</div>");

    ## DISPLAY PROJECTS
    echo("<div id=\"searchFormDiv\" style=\"margin: auto; padding: 0.75em; background-color: #8B8682;\">");
    echo("<fieldset>");
    echo("<LEGEND ACCESSKEY=I>SEARCH</LEGEND>");
    echo("<table style=\"border: 1px solid black; background-color: grey;\" id=\"searchs\">");
    echo("<tr id=\"SearchTitleBar\">");
    echo("<td style=\"padding: 4px;\"><label><center><u>ID</u></center></label></td>");
    echo("<td style=\"padding: 4px;\"><label><center><u>Name</u></center></label></td>");
    echo("<td style=\"padding: 4px;\"><label><center><u>Owner</u></center></label></td>");
    echo("<td style=\"padding: 4px;\"><label><center><u>Created</u></center></label></td>");
    echo("<td style=\"padding: 4px;\"><label><center><u>Updated</u></center></label></td>");
    echo("</tr>");
    foreach ($handle->query($getsearch) as $search)
    {

        $searchOwner = $search['owner'];
        $searchCreated = $search['created'];
        $searchUpdated = $search['updated'];
        $searchName = $search['name'];
        $searchID = $search['id'];

        $searchCount++;
        // TODO
        // Clean up to add good css formatting
        echo("<tr id=\"searchectInfo$searchCount\">");
        echo("<td style=\"padding: 4px; background: transparent; color: black;\" alt=\"Edit Graph\" onclick=\"editGraph('$searchID','$searchName');\"><center>$searchID</center></td>");
        echo("<td style=\"padding: 4px; background: transparent; color: black;\"><center>$searchName</center></td>");
        echo("<td style=\"padding: 4px; background: transparent; color: black;\"><center>$searchOwner</center></td>");
        echo("<td style=\"padding: 4px; background: transparent; color: black;\"><center>$searchCreated</center></td>");
        echo("<td style=\"padding: 4px; background: transparent; color: black;\"><center>$searchUpdated</center></td>");
        echo("</tr>");
        
    };
    echo("</table>");
    echo("<br />");
    echo("</fieldset>");
    echo("</div>");
    

    // Always remember to cleanup our database connection
    $searchBackend->teardown();

    // Really make sure cleanup is done
    $searchBackend = NULL;

} else {

    // Couldn't connect for some reason.
    echo('<p id="message">Cannot connect to the backend datastore.  Please contact the administrator.</p>');
}

?>
