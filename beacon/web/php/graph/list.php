<?php

session_start();

require('../utilities/connection.php');
require('../settings/config.php');
require('../utilities/queries.php');

// Database modification handle
$graphectBackend = new Database($dbBackend,$userBackend,$passBackend,$typeBackend);

// Setup backend connection
$graphectBackend->setup();

// We're connected to the data store
$handle = $graphectBackend->getHandle();

if ($handle) {

    // Construct query to retrieve user data and display it
    $getGraphs = $getUsersGraphsBegin . $_SESSION['user'] . $getUsersGraphsEnd;

    // DEBUG
    //echo($getGraphs);

    $graphCount = 0;

    echo("<p id=\"graphTitle\">GRAPH MANAGEMENT</p>");
    echo("<div id=\"graphMgmt\">");
    echo("</div>");

    ## DISPLAY PROJECTS
    echo("<div id=\"graphFormDiv\" style=\"\">");
    echo("<fieldset>");
    echo("<LEGEND ACCESSKEY=I>Graphs</LEGEND>");
    echo("<table style=\"\" id=\"graphs\">");
    echo("<tr id=\"GraphTitleBar\">");
    echo("<td style=\"\"><label><center><u>ID</u></center></label></td>");
    echo("<td style=\"\"><label><center><u>Name</u></center></label></td>");
    echo("<td style=\"\"><label><center><u>Owner</u></center></label></td>");
    echo("<td style=\"\"><label><center><u>Created</u></center></label></td>");
    echo("<td style=\"\"><label><center><u>Updated</u></center></label></td>");
    echo("</tr>");
    $allGraphs = $handle->query($getGraphs);
    if ($allGraphs) {
        foreach ($allGraphs as $graph)
        {

            $graphOwner = $graph['owner'];
            $graphCreated = $graph['created'];
            $graphUpdated = $graph['updated'];
            $graphName = $graph['name'];
            $graphID = $graph['id'];

            $graphCount++;
            // TODO
            // Clean up to add good css formatting
            echo("<tr id=\"graphectInfo$graphCount\">");
            echo("<td style=\"\" alt=\"Edit Graph\" onclick=\"editGraph('$graphID','$graphName');\"><center>$graphID</center></td>");
            echo("<td style=\"\"><center>$graphName</center></td>");
            echo("<td style=\"\"><center>$graphOwner</center></td>");
            echo("<td style=\"\"><center>$graphCreated</center></td>");
            echo("<td style=\"\"><center>$graphUpdated</center></td>");
            echo("</tr>");

        };
    } else {
        echo("<tr id=\"graphectInfo0\">");
        echo("<td style=\"\"><center>NO</center></td>");
        echo("<td style=\"\"><center>GRAPHS</center></td>");
        echo("<td style=\"\"><center>FOUND</center></td>");
        echo("<td style=\"\"><center>FOR</center></td>");
        echo("<td style=\"\"><center>YOU</center></td>");
        echo("</tr>");
    }
    echo("</table>");
    echo("<br />");
    echo("</fieldset>");
    echo("</div>");
    

    // Always remember to cleanup our database connection
    if (is_object($graphBackend)) {
        $graphBackend->teardown();
    }

    // Really make sure cleanup is done
    $graphBackend = NULL;

} else {

    // Couldn't connect for some reason.
    echo('<p id="message">Cannot connect to the backend datastore.  Please contact the administrator.</p>');
}

?>
