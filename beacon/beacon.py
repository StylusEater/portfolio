#!/usr/bin/env python
import parser
from db.Connection import Connection
from db.Schema import Schema
from Config import Config
from helpers.Crawler import Crawler
from helpers.file.Operations import Operations
from optparse import OptionParser
from os.path import join, isdir, isfile
from os import listdir, mkdir, walk, O_CREAT, remove
from pygments import highlight
from pygments.lexers import CLexer
from pygments.formatters import HtmlFormatter
from shutil import move, rmtree
from sys import exit
from sqlalchemy import Index
import types

__author__="Adam M. Dutko"
__email__="SET ME"
__cdate__ ="$Jan 8, 2009 7:56:54 PM$"
__license__ = "GPL v3 or later"
__include__ = "http://hrnjad.net/src/6/scriptutil.py.html#ffindgrep_func"

if __name__ == "__main__":

    ########################
    ## Parse CMD Options   #
    ########################
    parser = OptionParser()
    parser.add_option("-x", "--expand", dest="expand",
                  help="Expand an uploaded archive.", action="store_true")
    parser.add_option("-d", "--detect", dest="detect",
                  help="Detect the type of uploaded archive.", action="store_true")
    parser.add_option("-c", "--catalog", dest="catalog",
                  help="Catalog the archive or repository.", action="store_true")
    parser.add_option("-r", "--run", dest="run",
                  help="Run registered beacons.", action="store_true")
    parser.add_option("--clean", dest="clean",
                  help="Remove the contents of the expanded archive from the destination directory.",                   action="store_true")
    parser.add_option("--project", dest="project",
                  help="The name of the project stored in the destination directory and database.")
    parser.add_option("--revision", dest="revision",
                  help="The directory that corresponds to our revision of the project.")
    parser.add_option("--owner", dest="owner",
                  help="The owner of the project.")
    parser.add_option("--file", dest="filename",
                  help="Source code archive name, not path.", metavar="linux-2.6.28.tar.bz2")
    parser.add_option("--baseDir", dest="basedir",
                  help="Base directory for Beacon project.")
    parser.add_option("--repoURL", dest="repoURL", default="",
                  help="Repository URL")
    parser.add_option("--repoType", dest="repoType", default="",
                  help="Repository Type")
    parser.add_option("--config", dest="config", default="config.xml",
                  help="Configuration file", metavar="config.xml")
    parser.add_option("--schema", dest="schema", default="db/schemata.xml",
                  help="Schema file", metavar="db/schemata.xml")
    parser.add_option("--setupDB", dest="setupDB",
                  help="Install the database schema", action="store_true",
                  default=False)
    parser.add_option("--dropDB", dest="dropDB",
                  help="Drop the database", action="store_true",
                  default=False)

    (options, args) = parser.parse_args()

    ########################
    ## Configuration Logic #
    ########################
    configFile = options.config

    ## State if we want to drop the database
    ## before schema application.
    ##
    ## Default behavior is to drop the database.
    dropDB = options.dropDB

    ## Are we going to create the database?
    ##
    setupDB = options.setupDB

    ## Are we expanded an uploaded archive?
    ##
    expand = options.expand

    ## Are we detecting the type of uploaded archive?
    ##
    detect = options.detect

    ## Are we running beacon?
    ##
    run = options.run

    ## Are we cataloging a project?
    ##
    catalog = options.catalog

    ## Help us to determine if options are selected.
    ##
    noOptionSelected = True

    ## Set options defined in the configuration file
    if options.basedir != None:
        baseDir = options.basedir + '/'
    else:
        baseDir = defaultConfig.beaconDir + '/'

    ## Configuration Params Creation Logic
    defaultConfig = Config(baseDir + configFile)
    defaultConfig.getConfigOptions()


    ## TODO: Clean this up to do a more strict check.
    if not setupDB and not dropDB:
        if options.owner == None:
            print "You must give me an owner."
            exit(-1)
        ## TODO: Clean this up to do a more strict check.
        if options.project == None:
            print "You must give me a project."
            exit(-1)
        
        bDir = baseDir + defaultConfig.bdir + '/' + options.owner + '/' + options.project + '/'
        dDir = baseDir + defaultConfig.ddir + '/' + options.owner + '/'  + options.project + '/'
        archive = bDir + defaultConfig.archive

    if options.schema:
        schemaFile = options.schema
    else:
        schemaFile = defaultConfig.schemaFile

    ## Probably want to get away from this manual set in the future
    if defaultConfig.db == "":
        defaultConfig.db = 'beacon'

    ## Use the configuration parameters defined in the config.xml
    dbParams = {'user':defaultConfig.user,'passwd':defaultConfig.passwd, \
                'db':defaultConfig.db,'host':defaultConfig.host}

    ## Access config params and general SQL methods
    database = Connection(defaultConfig.dbType,dbParams)
    dbConn = database._mysql_connect()

    if options.filename != None:
        ## Check to see if the user supplied us with a file.
        ## If not we use the default found in the configuration file.
        if not isfile(bDir + options.filename):
            print "ERROR: " + bDir + options.filename + " is not a valid file."
            exit(-1)
        else:
            archive = bDir + options.filename

    if options.clean:
        ## make sure they gave us a project name
        if options.project == None:
            print "ERROR: We need a project name as specified by --project to continue..."
            exit(-1)
        else:
            ## Let's delete the expanded archive directory
            if isdir(dDir + options.project):
                try: 
                    noOptionSelected = False
                    rmtree(dDir + options.project)
                    print "Deleted the " + dDir + options.project + " project."
                    dbConn.query("delete from projects where name like '%" + (options.project).lower() + "%'") 
                    dbConn.commit()
                    ### NEED TO FIX THIS TO DELETE NODES
                except IOError:
                    print IOError
                    exit(-1)
            elif isfile(dDir + options.project):
                try: 
                    noOptionSelected = False
                    remove(dDir + options.project)
                    print "Deleted the " + dDir + options.project + " project file."
                    dbConn.query("delete from projects where name like '%" + (options.project).lower() + "%'") 
                    dbConn.commit()
                except IOError:
                    print IOError
                    exit(-1)
            else:
                print "ERROR: " + dDir + options.project + " is not a valid project directory."
                exit(-1)
            ## TODO: Implement the schema deletion for the project as well... 

 
    #################
    ## Schema Logic #
    #################

    ## Access schema creation and dissoluation routines
    schemaCreator = Schema(database.params(dbParams), schemaFile)


    if dropDB:
        noOptionSelected = False
        print "Attempting to drop the database..."
        ## Only drop the database if it exists
        if schemaCreator._database_exists() == 1:
            schemaCreator._drop_database()
            print "Database dropped."
        else:
            print "The database doesn't exist so I can't drop it."

    ## Setup the database if told to do so by the options parser
    ##
    if setupDB:
        noOptionSelected = False
        ## Create the database if it doesn't exist
        if schemaCreator._database_exists() == 0:
            ## Attempt to create the database used to hold our schema
            schemaCreator._create_database()
            print "Created database."

        ## Create the engine used to apply our schema
        schemaCreator._create_engine()
        ## Find the active schema defined in schemaFile
        activeSchemas = schemaCreator._get_active_schemas()

        ## Compare each schema starting with the first to 
        ## determine if we've redefined tables and if we 
        ## have check to see if we're dropping it and if 
        ## we are drop it, else apply the alterations
        allTables = []
        tables = {} 
        uniqueTables = []
        index = 0
        for eachSchema in activeSchemas:
            ## Find all active table definitions in the schema
            allTables.append(schemaCreator._get_all_tables(eachSchema))
        tableEntities = schemaCreator._flatten_table_list(allTables)     
        ## List of table names that correspond to the indices of newAllTables
        tableNames = schemaCreator._get_table_names(tableEntities)
        numTables = len(tableEntities)
        print "We have " + str(numTables) + " table definitions."
        while index < numTables:
            if tableNames[index] in tables.keys():
                if isinstance(tables[tableNames[index]], types.ListType):
                    tables[tableNames[index]].append(tableEntities[index])
                else: 
                    currentEntry = tables[tableNames[index]] 
                    newEntry = [currentEntry,tableEntities[index]]
                    tables[tableNames[index]] = newEntry
            else:
                tables[tableNames[index]] = tableEntities[index]
            index = index + 1
       
        ## FIXME - 10/19/10 
        ## I need to move on to other things on 10/19/10 so I'm 
        ## going to take the big hammer approach.  
        ##
        ## Look at the lists for each key and if they are size 
        ## one then create the table.  If they are size greater
        ## than one take the last definition in the ordered list
        ## and create it if it isn't set to delete.  
        ## Later I need to come back and do column compares and
        ## check delete/drop status of the table. 
        for eachTableList in tables.values():
                if isinstance(eachTableList, list):
                    ## This is where we deal with tables that have multiple
                    ## definitions.  This is most likely from changes to a 
                    ## table by adding a new schema with the change.  It is
                    ## possible the user deleted a table in a later schema. 
                    
                    ## The list of tables is in order based on where they 
                    ## were defined in the schema file so if the last one
                    ## has delete set we attempt to delete the table from 
                    ## the database and don't run any of the others.
                    lastTable = eachTableList[len(eachTableList)-1]
                    needsDrop = lastTable.getAttribute("drop")
                    tableName = lastTable.getAttribute("name")
                    if needsDrop == "true":
                        ## drop the table
                        schemaCreator._drop_mysql_table(tableName) 
                        continue

                    ## After passing through the delete portion, we move to
                    ## determining differences between existing tables and 
                    ## adjust the tables appropriately using the latest 
                    ## definition and the definition in the database.

                    ## FIXME - 08/20/10
                    uniqueTables.append(eachTableList[len(eachTableList)-1])
                else:
                    ## We only have one definition or this table so let us
                    ## create it. 
                    uniqueTables.append(eachTableList)

        newTableObjectList = []
        for eachTable in uniqueTables:
            newTableMetaData = schemaCreator._get_table_metadata(eachTable)
            newTableObject = schemaCreator._generate_table(newTableMetaData)
            newTableObjectList.append(newTableObject)

        tablesToDrop = schemaCreator._get_tables_marked_droppable(eachSchema)
        for eachTable in tablesToDrop:
            print "Removing " + eachTable.getAttribute("name") + " because there is a newer definition."
            newTableMetaData = schemaCreator._get_table_metadata(eachTable)
            newTableObject = schemaCreator._generate_table(newTableMetaData)
            schemaCreator._remove_table(newTableObject)

        while(False):
            for eachNewTable in newTableObjectList:
                ## Generate delayed indexes
                for eachIndexToGenerate in schemaCreator.colsNeedIndexes:
                    if eachNewTable.name == eachIndexToGenerate[0]:
                        print "Generating delayed index " + eachIndexToGenerate[1] + " for " + eachIndexToGenerate[2] + "."
                        lastIndex = Index(eachIndexToGenerate[1],eachNewTable.c[eachIndexToGenerate[3]])
                        schemaCreator._add_index(lastIndex)

        properlySortedTables = schemaCreator.metadata.sorted_tables  
        #for eachNewTable in newTableObjectList:
        for eachNewTable in properlySortedTables:
            schemaCreator._add_table(eachNewTable)

    if detect or expand:
        ## Our default file operations instance
        fileOperator = Operations()
        fileOptions = {'dest':dDir}

    def detectArchive():
        #############################
        ## ARCHIVE DETECTION LOGIC ##
        #############################

        ## EXAMPLE:
        ##  python beacon.py -d --file=1-linux-1.0.tar.gz --baseDir=/opt/beacon
        ##                   --owner=dutko.adam@gmail.com --project=Linux
        ##

        fileType = fileOperator.identify_file(archive)
        print archive + " has a filetype of " + fileType + "."

    if detect:
        noOptionSelected = False
        detectArchive()

    def expandArchive():
        #############################
        ## ARCHIVE EXPANSION LOGIC ##
        #############################

        ## EXAMPLE:
        ##  python beacon.py -x --file=1-linux-1.0.tar.gz --baseDir=/opt/beacon
        ##                   --owner=dutko.adam@gmail.com --project=Linux
        ##

        ## Loop over file to expand it
        srcFileName = "BLANK"
        destFileName = "UNSET"
        fileOperations = []
        while True:
            if srcFileName == "BLANK":
                srcFileName = archive
            if destFileName == "UNSET":
                destFileName = srcFileName 
            fileType = fileOperator.identify_file(srcFileName)
            if fileType == "":
                ## NOTE: This is how we break out after 
                ##       totally decompressing the file.
                ##       Before doing so we need to copy.

                ## Let's move the unpacked source from src to dest
                pathVals = destFileName.split('src')
                oldDest = pathVals[0] + 'dest/' + pathVals[1]
                destDir = pathVals[0] + 'dest/'
                
                if not isdir(destDir):
                    mkdir(destDir)

                if isdir(oldDest):
                    rmtree(oldDest)
		
                move(destFileName,oldDest)

                ##print "Archive detection and expansion complete."
                print srcFileName;
                break 
            fileOp = fileOperator.get_operation(fileType) 
            fileOperations.append(fileOp)
            destFileName = fileOperator.get_fileName(srcFileName, fileOperations) 
            fileOperator(fileOp, srcFileName, destFileName)
            srcFileName = destFileName

    if expand:
        noOptionSelected = False
        expandArchive()

    def catalogArchive():
        ##############################
        ## ARCHIVE CATALOGING LOGIC  #
        ##############################

        ## Dictionary for holding tree structure
        baseTree = {}
        ## List of all branching points in the tree
        nodeList = []
        ## Project 
        proj = options.project

        ## Our default Crawler instance
        baseCrawlerDir = dDir + options.revision
        archiveCrawler = Crawler(baseCrawlerDir)
        projDir = archiveCrawler.bDir

        print "Attempting to startup cataloging for " + baseCrawlerDir
        
        ## Get all nodes starting at the base directory.
        nodeEngine = Schema(database.params(dbParams),schemaFile)._get_engine_type()
        if nodeEngine != 'mysql':
            raise Exception("Beacon is not yet integrated with " + nodeEngine + ".")

        ## Check if Project is registered in the database
        ## If it is then we bail and ask the user to delete it
        ## If it isn't we continue
        dbConn.query("select id,name from projects where name like '%" + proj + "%'")
        projectCheckExistsResult = dbConn.store_result()
        projectCheckExistsResultValues = projectCheckExistsResult.fetch_row()
        
        if len(projectCheckExistsResultValues) == 1:
            print "NOTICE: Cataloging project... " + proj
            dbConn.query("update projects set updated = now() where name = '" + proj + "'")
            dbConn.commit()
            dbConn.query("select id from projects where name ='" + proj + "'")
            projID = str(dbConn.store_result().fetch_row()[0][0])
            dbConn.query("insert into jobs (projectid,file,status,description,created) values ('" + projID + "','" + archive + "','1','" + proj + "', now())")
            dbConn.commit();
        else:
            print "Project doesn't exist: " + proj
            exit(-1)

        for root, dirs, files in walk(projDir):
            ## Discover all nodes in the dirs located in the base directory
            # the root is the parent for all node in the "root" dirs
            #
            print "Searching for nodes at " + archiveCrawler.bDir
              
            ## Just files? 
            if len(dirs) == 0:
                archiveNodes = files
                nodeType = "file"
            else:
                ## Directories and files
                archiveNodes = dirs
                nodeType = "directory"
                
            for node in archiveNodes:
                if node[0] == '/':
                    node = node[1:]
                fullpath = join(root,node)
                if nodeType == "directory":
                    if fullpath[-1] != '/':
                        fullpath = fullpath + '/'
                print "Adding the node " + fullpath + " to the catalog."
                # build insert for each node into the nodes table
                path = fullpath
                parent = root
                name = node
                # insert values
                dbConn.query("insert into nodes (projectid,path,parent,name,created,updated) values ('" + projID + "','" + path + "','" + parent + "','" + name + "', now(), now())")
                dbConn.commit()
                if archiveCrawler.is_not_hidden_dir(fullpath):
                    if fullpath not in nodeList:
                        nodeList.append(fullpath)
                        actualLeaf = []
                        if isdir(fullpath):
                            for eachPotentialLeaf in listdir(fullpath):
                                if isfile(join(fullpath,eachPotentialLeaf)):
                                    actualLeaf.append(eachPotentialLeaf)
                        baseTree[fullpath] = actualLeaf

        dbConn.query("update jobs set status = '2', updated = now() where projectid = '" + projID + "' and file = '" + archive + "'")
        dbConn.commit();
        print "Cataloging complete."

    if catalog:
        noOptionSelected = False
        if options.revision == None:
            print "ERROR: We need a revision name as specified by --revision to continue..."
            exit(-1)

        if not isdir(dDir + options.revision):
            print "ERROR: Did you expand the revision to a local directory?"
            print "SUGGESTION: Maybe check the name of your revision as specified in --revision?"
            exit(-1) 

        if database.login():
            catalogArchive()
        else:
            print "Cannot login to database."

    if run:
        noOptionSelected = False
        #############################
        ## Signature Tracking Logic #
        #############################
        ###
        # This is the core of the project.
        # Based on our config we either create every leaf or we create select
        # leaves for the files we want to track.  We then pull in dependencies
        # and search for our signature in the file and or the dependencies and
        # then push the signature information into the database so we can track
        # changes.
        for baseNode in baseTree.keys():
            for eachLeaf in baseTree[baseNode]:
                if kernCraw.is_src_or_header(eachLeaf):
                    tdDir = baseNode.replace(kernCraw.bDir,'')
                    docNode = join(kernCraw.dDir,tdDir)
                    kernCraw._mkdir(docNode)
                    fullLeafPath = join(baseNode,eachLeaf)
                    leafContents = file(fullLeafPath).read()

                    # we don't check for which we're dealing with
                    # because the replace simply falls through
                    eachLeaf = eachLeaf.replace('.h','.html')
                    eachLeaf = eachLeaf.replace('.c','.html')

                    targetLeafPath = join(docNode,eachLeaf)
                    print targetLeafPath
                    formattedLeaf = open(targetLeafPath,'w',O_CREAT)
                    leafFormatter = HtmlFormatter(encoding='utf-8',linenos=True, cssclass="source", full=True)
                    newLeafContent = highlight(leafContents, CLexer(), leafFormatter)
                    formattedLeaf.write(newLeafContent)
                    formattedLeaf.close()
                else:
                    # skip the file
                    continue


## If we haven't specified any options then call the help menu.
##
if noOptionSelected:
    parser.print_help()
