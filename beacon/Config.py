from xml.dom.minidom import parse

__author__="Adam M. Dutko"
__email__="dutko.adam@gmail.com"
__cdate__ ="$Feb 25, 2009 6:56:46 AM$"
__license__ = "GPL v3 or later"

class Config:
    """
        Pull in the configuration options stored in config.xml.
    """

    def __init__(self, configFile):
        """
            Define and initialize all valid configuration options.
        """
        self.schemaFile = ''
        self.bdir = ''
        self.ddir = ''
        #self.kernel = ''
        self.archive = ''
        self.configFile = configFile

    
    def getConfigOptions(self):
        """
            Get the configuration options from the config.xml file defined
            through self.configFile and set all options in the class to those
            values.
        """
        config = parse(self.configFile)

        ## DB PARAMETERS ##
        self.user = str(config.getElementsByTagName("user")[0].getAttribute("value"))
        self.host = str(config.getElementsByTagName("host")[0].getAttribute("value"))
        self.passwd = str(config.getElementsByTagName("passwd")[0].getAttribute("value"))
        self.db = str(config.getElementsByTagName("db")[0].getAttribute("value"))
        self.dbType = str(config.getElementsByTagName("dbType")[0].getAttribute("value"))

        ## PROJECT PARAMETERS
        self.schemaFile = str(config.getElementsByTagName("schema")[0].getAttribute("value"))
        self.bdir = str(config.getElementsByTagName("bdir")[0].getAttribute("value"))
        self.ddir = str(config.getElementsByTagName("ddir")[0].getAttribute("value"))
        self.archive = str(config.getElementsByTagName("archive")[0].getAttribute("value"))
        self.beaconDir = str(config.getElementsByTagName("beaconDir")[0].getAttribute("value"))
        self.archive = str(config.getElementsByTagName("archive")[0].getAttribute("value"))
