## compatibility checks
import sqlalchemy
if sqlalchemy.__version__ > "0.6.0":
    from sqlalchemy.dialects.mysql.base import BIGINT as MSBigInteger
    from sqlalchemy.dialects.mysql.base import ENUM as MSEnum
else:
    from sqlalchemy.databases.mysql import MSBigInteger,MSEnum

from sqlalchemy import Column, create_engine, Table, MetaData
from sqlalchemy.types import INT,CHAR,VARCHAR,NCHAR,TEXT,FLOAT,NUMERIC,DECIMAL
from sqlalchemy.types import TIMESTAMP,DATETIME,CLOB,BLOB,BOOLEAN,SMALLINT,DATE,TIME,Text
from sqlalchemy import ForeignKey
from sqlalchemy import Index
from xml.dom.minidom import parse
from Exceptions.programming import Error as ProgrammingError
from MySQLdb import connect,MySQLError

__author__="Adam M. Dutko"
__email__="dutko.adam@gmail.com"
__cdate__ ="$Feb 2, 2009 9:52:40 PM$"
__license__ = "GPL v3 or later"

class Schema:
    """
        I tried to embody as much functionality as was pertinent in this class
        regarding various actions performed on "schemas" defined in the schemata.xml
        file.  I put schemas in double-quotes because each schema isn't really
        a wholly representative version of a full schema but rather parts of one.
        Whenever you want to make a change you can add new tables to a new
        schema and they should be picked up by the table creator.  I am currently
        investigating what code it would take to detect changes to existing tables
        and apply them without manual intervention.
    """

    def __init__(self,params,file="schemata.xml"):
        """
        """
        self.engine = ''
        self.schemata = file
        self.engineType = ''
        self.params = params
        self.sch = parse(self.schemata)
        ## FIX ME - need to seperate MySQL out from others
        ##          so we can use this approach in a configurable manner.
        self.engineSubType = 'innodb' 
        self.metadata = MetaData()
        self.colsNeedIndexes = []

    def _create_engine(self,engType='mysql'):
        """

        """

        if engType != '':
            self.engineType = engType
        else:
            engType = self.sch.firstChild.getAttribute('type')
            self.engineType = engType

        if self.engineType == '':
            raise(ConfigurationException(engType,'engType'))
            sys.exit(-1)

        try:
            engString = engType + '://' + self.params['user'] + ':' + \
                self.params['passwd'] + '@' + self.params['host'] + '/' + self.params['db']
        except KeyError:
            engString = engType + '://' + self.params['user'] + ':' + \
                self.params['passwd'] + '@' + self.params['host']
        self.engine = create_engine(engString, echo=True)

    def _get_active_schemas(self):
        schemas = self.sch.getElementsByTagName("schema")
        validSchemas = []
        for eachSchema in schemas:
            if eachSchema.getAttribute("active") == 'true':
                validSchemas.append(eachSchema)
        return validSchemas
        
    def _get_all_tables(self, selectedSchema):
        """
            Get all the table xml entities we're going to create from the config file
            and return them as an array.
        """
        return selectedSchema.getElementsByTagName("table")

    def _flatten_table_list(self, allTables):
        """
            From a list of lists generate a list.
        """
        newAllTables = []
        for tableSet in allTables:
            for eachTable in tableSet:
                newAllTables.append(eachTable)
        return newAllTables

    def _get_table_names(self, newTableList):
        """
            Return a list of table names.
        """
        tableNames = []
        for eachNewTable in newTableList:
            tableNames.append(eachNewTable.getAttribute("name"))
        return tableNames
    
    def _generate_table(self, tableConfig):
        """
            Add the table object to our cache after constructing it from the
            XML representation.
        """
        for eachTable in tableConfig.keys():
            if self.engineType == 'mysql':
                tbl = Table(eachTable, self.metadata, useexisting=True, mysql_engine=self.engineSubType)
            else:
                ## NOT really tested/implemented
                ##
                tbl = Table(eachTable, self.metadata, useexisting=True)
            for eachColumn in tableConfig[eachTable].keys():
            # Required column attributes
                name = tableConfig[eachTable][eachColumn]['name']
                ## Defined type
                dtype = tableConfig[eachTable][eachColumn]['type']

                # Optional column attributes
                valKeys = tableConfig[eachTable][eachColumn].keys()
                if 'length' in valKeys:
                    collen = int(tableConfig[eachTable][eachColumn]['length'])
                if 'precision' in valKeys:
                    colprec = tableConfig[eachTable][eachColumn]['precision']
                else:
                    colprec = 10
                if 'timezone' in valKeys:
                    coltzone = tableConfig[eachTable][eachColumn]['timezone']
                else:
                    coltzone = False
                if 'convertunicode' in valKeys:
                    colconvuni = tableConfig[eachTable][eachColumn]['convertunicode']
                else:
                    colconvuni = False
                if 'assertunicode' in valKeys:
                    colassuni = tableConfig[eachTable][eachColumn]['assertunicode']
                else:
                    colassuni = None
                if 'asdecimal' in valKeys:
                    colasdeci = tableConfig[eachTable][eachColumn]['asdecimal']
                else:
                    colasdeci = False
                if 'scale' in valKeys:
                    colscale = tableConfig[eachTabe][eachColumn]['scale']
                else:
                    colscale = 2
                if 'enum' in valKeys:
                    colenumvals = tableConfig[eachTable][eachColumn]['enum']
                elif 'enumeration' in valKeys:
                    colenumvals = tableConfig[eachTable][eachColumn]['enumeration']
                else:
                    colenumvals = ()

                # Find a match and set it to selected type
                stype = ''
                if dtype == ('int' or 'integer'):
                    stype = INT(length=collen)
                elif dtype == 'char':
                    stype = CHAR(length=collen,convert_unicode=colconvuni,assert_unicode=colassuni)
                elif dtype == 'varchar':
                    stype = VARCHAR(length=collen,convert_unicode=colconvuni,assert_unicode=colassuni)
                elif dtype == ('nchar' or 'text'):
                    stype = NCHAR(length=collen)
                elif dtype == 'float':
                    stype = FLOAT(precision=colprec,asdecimal=colasdeci)
                elif dtype == 'numeric':
                    stype = NUMERIC(precision=colprec,scale=colscale,asdecimal=True,length=collen)
                elif dtype == 'decimal':
                    stype = DECIMAL(precision=colprec,scale=colscale,asdecimal=True,length=collen)
                elif dtype == 'timestamp':
                    stype = TIMESTAMP(timezone=coltzone)
                elif dtype == 'datetime':
                    stype = DATETIME(timezone=coltzone)
                elif dtype == 'clob':
                    stype = CLOB(length=collen,convert_unicode=colconvuni,assert_unicode=colassuni)
                elif dtype == 'blob':
                    stype = BLOB(length=collen)
                elif dtype == ('boolean' or 'bool'):
                    stype = BOOLEAN()
                elif dtype == 'smallint':
                    stype = SMALLINT()
                elif dtype == 'date':
                    stype = DATE()
                elif dtype == 'time':
                    stype = TIME(timezone=coltzone)
                elif dtype == ('enumeration' or 'enum'):
                    stype = MSENUM(colenumvals.toList())
                elif dtype == 'bigint':
                    stype = MSBigInteger()
                elif dtype == 'text':
                    stype = Text(length=collen) 

                ## Make sure we have the required column attributes
                attrs = {'name':name,'type':stype}
                col = Column(name, stype)
                col.name = attrs['name']
                col.type = attrs['type']

                # Is the column nullable?
                if 'null' in valKeys:
                    if tableConfig[eachTable][eachColumn]['null'] == 'false':
                        col.nullable = False
                # Do we auto-increment the column?
                if 'auto' in valKeys:
                    if tableConfig[eachTable][eachColumn]['auto']:
                        col.auto = True
                # Is it a binary column?
                if 'binary' in valKeys:
                    if tableConfig[eachTable][eachColumn]['binary']:
                        col.binary = True
                # Do we have a default?
                if 'default' in valKeys:
                    if tableConfig[eachTable][eachColumn]['default'] != '':
                        #col.server_default = tableConfig[eachTable][eachColumn]['default']
                        col.server_default = tableConfig[eachTable][eachColumn]['default']
                # Do we have a primary key?
                if 'primary' in valKeys:
                    if tableConfig[eachTable][eachColumn]['primary']:
                        col.primary_key = True
                # Do we have a unique constraint?
                if 'unique' in valKeys:
                    if tableConfig[eachTable][eachColumn]['unique']:
                        col.unique = True
                if 'index' in valKeys:
                    if tableConfig[eachTable][eachColumn]['index']:
                        col.index = True

                tbl.append_column(col)
                if col.index:
                    Index("idx_col" + col.name,tbl.c[col.name])

                ## EXCEPTION
                ## Foreign Key Definition Appended to Table definition
                ## SEE: http://www.sqlalchemy.org/docs/05/metadata.html#defining-foreign-keys
                ##
                ## NOTE: We use the first entry in the schema file to determine
                ## what column to add the foreign key to...
                ##
                ## For each column involved in a FK relationship we need to generate an index.
                ##
                if 'foreign' in valKeys:
                    fkAttrs = tableConfig[eachTable][eachColumn]['foreign']
                    if 'ondelete' in valKeys:
                        ondeleteAction = tableConfig[eachTable][eachColumn]['ondelete']
                    if fkAttrs:
                        fkParts = list(fkAttrs.split(','))
                        ### DATA BREAKDOWN
                        ## fkParts[0] -- first column
                        ## fkParts[1] -- second column
                        ## fkParts[2] -- first table
                        ## fkParts[3] -- second table
                        if col.name == fkParts[0]:
                            print "Working on foreign key for " + tbl.name + "." + col.name + "."
                        
                        print "Generating index for " + fkParts[2] + "." + fkParts[0]
                        idx0_name = "idx0_" + fkParts[2][0] + fkParts[0][0]
                        Index(idx0_name, tbl.c[fkParts[0]])
                        
                        ## NOTE: We add this member variable to keep track of indexes we can't 
                        ##       generate locally because we don't have the table object.
                        #print " We need to generate an index for " + fkParts[3] + "." + fkParts[1]
                        #idx1_name = "idx1_" + fkParts[3][0] + fkParts[1][0]
                        #self.colsNeedIndexes.append([fkParts[3],idx1_name,fkParts[3] + "." + fkParts[1],fkParts[1]])

                        if ondeleteAction:
                            fk = ForeignKey(fkParts[3]+'.'+fkParts[1],ondelete=ondeleteAction)
                        else:
                            fk = ForeignKey(fkParts[3]+'.'+fkParts[1])
                        tbl.c[col.name].append_foreign_key(fk)

            return tbl

    def _add_table(self,tableToAdd):
        self.engine.create(tableToAdd)

    def _add_index(self,indexToAdd):
        self.engine.create(indexToAdd)

    def _remove_table(self,tableToDrop):
        """
            Those tables marked with a drop="true" in the schemata.xml file
            will be removed from our schema cache.
        """
        self.engine.drop(tableToDrop)

    def _get_tables_marked_droppable(self,selectedSchema):
        """
            Get all tables marked with a drop true in the schema.
        """
        tables = selectedSchema.getElementsByTagName("table")
        droppableTables = {}
        for eachTable in tables:
            if eachTable.getAttribute("drop") == 'true':
                droppableTables[eachTable] = eachTable
        return droppableTables  

    def _create_mysql_schema(self):
        """
            After all of the table configs are associated with the metadata
            we "sync" all confgs to the metadata and create the tables through
            the database engine.
        """
        self.metadata.create_all(self.engine)

    def _drop_mysql_schema(self):
        """
            Drop the schema specificed by version for beacon
        """
        self.metadata.drop_all(self.engine)

    def _drop_mysql_table(self, table):
        """
            Drop the table passed.
        """
        host = self.params['host']
        user = self.params['user']
        password = self.params['passwd']
        dbName = ''
        try:
            dbName = self.params['db']
            dbConn = connect(host=host,user=user,passwd=password,db=dbName)
        except MySQLError, me:
            print "Error %d: %s" % (me.args[0], me.args[1])
            ## Nothing to do but skip the parameter 
            dbConn = connect(host=host,user=user,passwd=password,db="")
            dbName = 'beacon'

        dbCursor = dbConn.cursor()

        try:
            dbCursor.execute("drop table " + dbName + "." + table + ";")
            dbCursor.close()
            dbConn.close()
        except MySQLError, me:
            ProgrammingError(me)
            dbExists = 0
        except Exception, ex:
            ProgrammingError(ex)


    def _database_exists(self):
        """
            Check if the given database defined in the schema exists.
        """
        dbExists = 1
        host = self.params['host']
        user = self.params['user']
        password = self.params['passwd']
        dbName = ''
        try:
            dbName = self.params['db']
            dbConn = connect(host=host,user=user,passwd=password,db=dbName)
        except MySQLError, me:
            print "Error %d: %s" % (me.args[0], me.args[1])
            ## Nothing to do but skip the parameter 
            dbConn = connect(host=host,user=user,passwd=password,db="")
            dbName = 'beacon'

        dbCursor = dbConn.cursor()

        try:
            dbCursor.execute("use " + dbName)
            dbCursor.close()
            dbConn.close()
        except MySQLError, me:
            ProgrammingError(me)
            dbExists = 0
        except Exception, ex:
            ProgrammingError(ex)
            dbExists = 0

        return dbExists

        
    def _create_database(self):
        """
            SQLAlchemy independent database creator.
        """
        host = self.params['host']
        user = self.params['user']
        password = self.params['passwd']
        dbName = 'beacon'
        try:
            dbName = self.params['db']
            dbConn = connect(host=host,user=user,passwd=password,db=dbName)
        except MySQLError, me:
            print "Error %d: %s" % (me.args[0], me.args[1])
            dbConn = connect(host=host,user=user,passwd=password,db='')
        dbCursor = dbConn.cursor()

        try:
            dbCursor.execute("create database " + dbName)
            dbCursor.commit()
            dbCursor.close()
            dbConn.close()
            return True
        except Exception, ex:
            return False

        
    def _drop_database(self):
        """
           SQLAlchemy independent database dropper.
        """
        host = self.params['host']
        user = self.params['user']
        password = self.params['passwd']
        dbName = ''

        try:
            dbName = self.params['db']
        except KeyError:
            dbName = 'beacon' 
        dbConn = connect(host=host,user=user,passwd=password)
        dbCursor = dbConn.cursor()

        try:
            dbCursor.execute("drop database " + dbName)
            dbCursor.close()
            dbConn.close()
            return True
        except Exception, ex:
            return False

    def _get_engine_type(self):
        """
            Get the engine type from the schemata file.
        """
        try:
            schema = parse(self.schemata)
            db = schema.getElementsByTagName("database")
            engineType = str(db[0].getAttribute('type'))
            return engineType
        except IOError, ie:
            print ie

    def _get_table_metadata(self,metaTable):
        ## Core table creation logic
        ##
        ## We first create a container for all valid tables then lookup
        ## each valid column defined in our schema and add to the table
        ## and finally create our schema using the schemaCreator
        tableData = {}
        columnConfig = {}
        atable = metaTable.getElementsByTagName("column")
        for columns in atable:
            # columns --> DOM Element
            columnData = {}
            columnCount = len(columns.attributes)
            for theColumnIndex in range(0,columnCount):
                columnData[columns.attributes.item(theColumnIndex).name] = columns.attributes.item(theColumnIndex).value
            columnConfig[columnData['name']] = columnData
        tableData[str(metaTable.getAttribute("name"))] = columnConfig
        return tableData


    def _sync(self):
        if self.engineType == 'mysql':
            self._create_mysql_schema()
