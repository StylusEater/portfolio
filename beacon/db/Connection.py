from MySQLdb import connect
from MySQLdb import Error as MySQLError
from sys import exit as sexit
from hashlib import sha1

__author__="Adam M. Dutko"
__email__="dutko.adam@gmail.com"
__cdate__ ="$Jan 29, 2009 10:06:05 PM$"
__license__ = "GPL v3 or later"

__all__ = ('Connection','login','_mysql_connect','_postgres_connect',
           '_create_mysql_schema','_drop_mysql_schema','Params')

class Params(dict):
    """
        Default connection parameters.
    """
    
    def __init__(self,args=''):
        self.options = args
        if len(self.options.keys()) != 0:
            if 'host' in args.keys():
                self.host = args['host']
                self.options['host'] = args['host']
            else:
                self.host = 'localhost'
                self.options['host'] = 'localhost'
            if 'user' in args.keys():
                self.user = args['user']
                self.options['user'] = args['user']
            else:
                self.user = 'beacon'
                self.options['user'] = 'beacon'
            if 'passwd' in args.keys():
                self.passwd = args['passwd']
                self.options['passwd'] = args['passwd']
            else:
                self.passwd = 'beaconAdmin'
                self.options['passwd'] = 'beaconAdmin'
            if 'db' in args.keys():
                self.db = args['db']
                self.options['db'] = args['db']
        else:
            self.host = 'localhost'
            self.user = 'beacon'
            self.passwd = 'beaconAdmin'

    def __call__(self,args={}):
        self.__init__(args)
        try:
            db = self.db
            return {'host':self.host,'user':self.user,'passwd':self.passwd,'db':self.db}
        except AttributeError:
            return {'host':self.host,'user':self.user,'passwd':self.passwd}
        
    def __encryptPass__(self,plainPass):
        passOne = sha1(plainPass).digest()
        passTwo = sha1(passOne).hexdigest() 
        return "*" + passTwo.upper() 
        

class Connection(object):
    """
        Default connection class for MySQL and Postgresql.
    """

    def __init__(self,backend='MySQL',params={}):
        """

        """
        self.dbType = backend
        self.params = Params(params)
        
    def login(self,dbType='MySQL'):
        """
            Default login procedure that returns handle.
        """
        if dbType == "MySQL":
            try:
                import MySQLdb
            except ImportError, ie:
                print ie
                sexit(-1)

            try:
                connection = self._mysql_connect()
                return connection
            except MySQLError, me:
                print "Error %d: %s" % (me.args[0], me.args[1])
                sexit(-1)
        elif dbType == "PostgreSQL":
            try:
                import PyGreSQL
            except ImportError, ie:
                print ie
                sexit(-1)

    def _mysql_connect(self):
        """
            MySQL connection function.
        """
        try:
            db = self.params.db
            mysql_connection = connect( self.params.host,
                                        self.params.user,
                                        self.params.passwd,
                                        db )
            return mysql_connection
        except MySQLError, me:
            print "Error %d: %s" % (me.args[0], me.args[1])
            try:
                mysql_connection = connect( self.params.host,
                                            self.params.user,
                                            self.params.passwd )
                return mysql_connection
            except MySQLError, me:
                print "Error %d: %s" % (me.args[0], me.args[1])
                sexit(-1)
        
    def _postgres_connect(self,params):
        """
            PostgreSQL connection function.
        """
        return False
