#!/usr/bin/env python

__author__ = "Adam M. Dutko"
__email__ = "dutko.adam@gmail.com"

print "Thank you for using Beacon!\n"
print "###############################"
print "# You can contact the author: #"
print "#  " + __author__ + "              #"
print "#         at                  #"
print "#  " + __email__ + "       #"
print "###############################\n" 

print "Remember each column defined in the schema" + \
      "must define name,type,length"

print "You'll need to easy_install: " \
      "   sqlachemy    " \
      "   mysql-python " \
      "   pygments     " \

# Useful link: http://docs.python.org/library/xml.dom.html
# Useful link: http://www.sqlalchemy.org/docs/05/reference/sqlalchemy/types.html#types
# Useful link: http://www.sqlalchemy.org/docs/05/reference/sqlalchemy/schema.html
# Useful link: http://www.devshed.com/c/a/PHP/Creating-a-Secure-PHP-Login-Script/
# Useful link: http://wiki.github.com/madrobby/scriptaculous
# Useful link: http://wiki.github.com/madrobby/scriptaculous/tabs
# Useful link: http://webhelp.esri.com/arcgisserver/9.3/java/index.htm#geoprocessing/unzip_python_script.htm
# Useful link: http://docs.python.org/library/optparse.html#module-optparse
# Useful link: http://www.tonymarston.net/php-mysql/databaseobjects.html
# Useful link: http://www.marksanborn.net/programming/highlight-form-focus-with-prototype/