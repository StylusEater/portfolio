Beacon
=========

#### A Complementum Testing (CT) Utility ####

Beacon is a testing utility designed to help software developers keep track of
interesting areas of a code base.  It offers a web interface for easy project
creation and a powerful backend for project tracking and signature management.

### Targeted platforms ###

Beacon currently works on Debian Linux using MySQL 5.x.  Future integration with
PostgreSQL, other Linux distributions and support for BSD and OpenSolaris are
in the works.

Using Beacon
---------------

To use Beacon you must first create an account using the signup form located on
the front page of the web site(<http://beacon.oktud.com>).  Once you
are signed up you may modify your account settings using the Account tab.  You
may also start to register projects but must first be activated by the
administrator before you can start consuming backend computing resources.

### Installing Beacon ###

Web Front End
---------------
setup apache

<VirtualHost SOMEIP:80>
        ServerAdmin webadmin@oktud.com
        DocumentRoot /documentroot/
        ServerName targethost.targetdomain.tld
        ServerAlias *.targethost.targetdomain.tld
        DirectoryIndex index.html

        ## Logging
        LogLevel warn
        ErrorLog /logroot/targethost.targetdomain.tld-error_log
        CustomLog /logroot/targethost.targetdomain.tld-access_log combined

</VirtualHost>

scp -r web/* targethost.targetdomain.tld:/documentroot/
cd /documentroot
ln -s static/html/index.html index.html
modify passwords in php/settings/config.php
change path to python xml config file in php/settings/pyXMLConfig.php

*make sure php.ini file and upload settings are adjusted to 512M to accomodate 
rather large file uploads.

Backend
---------
mkdir /opt/beacon
chown you:somegroup /opt/beacon
scp -r {*.py,db,*.xml,docs,helpers,scratch} targethost.targetdomain.tld:/opt/beacon/
apt-get install python-mysqldb python-pygments mariadb-server php5-mysql
mysql -u privilegeduser -p
create user 'beacon'@'localhost' identified by 'password';
create database beacon;
grant select,insert,update,delete,alter,create,drop,index on beacon.* to 'beacon'@'localhost';

WARNING -- the following will drop your database by default if you don't want
           that to happen then do a -h or --help for more information.
python beacon.py

Contributing to Beacon
-------------------------

Check out the Beacon source with

    $ hg clone https://hg.metro-six.com/hgroot/beacon
    $ cd beacon

Find out how to contribute: <http://beacon.oktud.com/contribute>.

Documentation
-------------

## As of 02/01/11 you need to use SQLAlchemy 0.6.6 or greater.
