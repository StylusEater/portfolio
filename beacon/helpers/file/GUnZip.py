from gzip import GzipFile 
from exceptions import IOError

__author__="Adam M. Dutko"
__email__="dutko.adam@gmail.com"
__cdate__ ="$Jan 2, 2010 22:06:02 PM$"
__license__ = "GPL v3 or later"

class GUnZip:
    """


    """
    def __init__(self,srcFile,destFile):
        self.__call__(srcFile,destFile)

    def __call__(self,srcFile,destFile):
        gzipFile = GzipFile(srcFile,'r')

        try:
            newFile = open(destFile,'w')
        except IOError:
            raise

        try:
            while True:
                data = gzipFile.read()
                if not data:
                    break
                else:
                    newFile.write(data)
        except IOError:
            raise
        
        gzipFile.close()
        newFile.close()
