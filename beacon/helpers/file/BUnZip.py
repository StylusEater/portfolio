from bz2 import BZ2File
from exceptions import IOError

__author__="Adam M. Dutko"
__email__="dutko.adam@gmail.com"
__cdate__ ="$Jan 2, 2010 22:06:02 PM$"
__license__ = "GPL v3 or later"

class BUnZip:
    """


    """
    def __init__(self,srcFile,destFile):
        self.__call__(srcFile,destFile)

    def __call__(self,srcFile,destFile):
        bzipFile = BZ2File(srcFile,'r')

        try:
            newFile = open(destFile,'w')
        except IOError:
            raise

        try:
            while True:
                data = bzipFile.read()
                if not data:
                    break
                else:
                    newFile.write(data)
        except IOError:
            raise
        
        bzipFile.close()
        newFile.close()
