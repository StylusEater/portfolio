from tarfile import TarFile
from os.path import isdir
from shutil import rmtree

__author__="Adam M. Dutko"
__email__="dutko.adam@gmail.com"
__cdate__ ="$Jan 2, 2010 23:35:02 PM$"
__license__ = "GPL v3 or later"

class UnTar:
    """


    """
    def __init__(self,srcFile,destFile):
        self.__call__(srcFile,destFile)

    def __call__(self,srcFile,destFile):
        archive = TarFile(srcFile,'r')
        archive.extractall(destFile)
        archive.close()
