from UnTar import UnTar
from UnZip import UnZip
from BUnZip import BUnZip
from GUnZip import GUnZip
from os.path import exists,isfile,splitext

__author__="Adam M. Dutko"
__email__="dutko.adam@gmail.com"
__cdate__ ="$Jan 1, 2010 13:33:14 PM$"
__license__ = "GPL v3 or later"

class Operations:
    """
        File maintenance class.  Meant to be a wrapper around the file utilities
        for manipulating code archives.

    """
    def __init__(self):
        return

    def __call__(self,operation,srcFile,destFile):
        """
            Self explanatory.
        """
        if operation.isalpha:
            if exists(srcFile):
                print srcFile + " exists."
                if isfile(srcFile):
                    print srcFile + " is a file."
                    print "Trying to " + operation + " " + srcFile + "."

                    ## Perform requested operation on the file
                    if operation == "untar":
                        UnTar(srcFile,destFile)
                    elif operation == "unzip":
                        UnZip(srcFile,destFile)
                    elif operation == "bunzip":
                        BUnZip(srcFile,destFile)
                    elif operation == "gunzip":
                        GUnZip(srcFile,destFile)
                    else:
                        raise Exception(operation + " is not a valid operation.")
                else:
                    raise Exception(file + " is not a valid file.")
            else:
                ## We should get here when we're done so it's not an error.
                return
        else:
            raise Exception(operation + " is not defined in file.Operations")

    def identify_file(self,srcFile):
        """
            Determine the type of file.  Should be called multiple times until
            it determines the file
        """
        ## WARNING!!!
        ## Cheaters way of identifying files...not as accurate as
        ## determining the actual file type.  Poses a potential security
        ## problem.

        ## Will return an extension or blank
        splitFile = splitext(srcFile)
        fileName = splitFile[0]
        extension = splitFile[1]
        if extension == ".tar":
            return "tar" 
        elif extension == ".zip":
            return "zip"
        elif extension == ".bz2":
            return "bzip" 
        elif extension == ".gz":
            return "gzip"
        elif extension == "":
            return "" 
        else:
            print extension + " is an unknown file extension."
            return ""

    def get_operation(self,fileType):
        if fileType == "tar":
            return "untar"
        elif fileType == "zip":
            return "unzip"
        elif fileType == "bzip":
            return "bunzip"
        elif fileType == "gzip":
            return "gunzip"
        else:
            return "" 

    def get_fileName(self,srcFile,completedOperations):
        fileName = ""
        if len(completedOperations) == 0:
            return srcFile
        else:
           for eachOperation in completedOperations:
               if eachOperation == "untar":
                   extension = ".tar"
               elif eachOperation == "unzip":
                   extension = ".zip"
               elif eachOperation == "bunzip":
                   extension = ".bz2"
               elif eachOperation == "gunzip":
                   extension = ".gz"
               fileName = srcFile.split(extension)[0]
        return fileName 
