import hashlib
from os.path import exists

class hash:
    def __init__(self,fPath,hAlg="sha256"):
        if fPath is "":
            print "We need to have a file path to hash." 
        if not exists(fPath):
            print fPath + " does not exist."
        self.filePath = fPath
        self.digest = ""
        self.hashalg = hAlg
        if hAlg == "sha256":
            self.hashobj = hashlib.sha256()
        elif hAlg == "md5":
            self.hashobj = hashlib.md5()
        elif hAlg == "sha1":
            self.hashobj = hashlib.sha1()
        elif hAlg == "sha224":
            self.hashobj = hashlib.sha224()
        elif hAlg == "sha384":
            self.hashobj = hashlib.sha384()
        elif hAlg == "sha512":
            self.hashobj = hashlib.sha512()
        else:
            print hAlg + " is not a supported hashing algorithm."
         
    def generate(self):
        try:
            fileHandle = open(self.filePath,'r')
            contents = fileHandle.read()
            self.hashobj.update(contents)
            self.digest = self.hashobj.digest()
            fileHandle.close()
        except IOError, ie:
            print ie
        except OSError, oe:
            print oe

    def compare(self,type,path="",revision=""):
        if type == "file":
            print "Attempting to compare local files."
            ## The local copies are on the filesystem.
        elif type == "repo":
            print "Attempting to compre repository files."
            ## We have the initial checkout from the project 
            ## stored on the local filesystem from which we 
            ## compare file versions.
        else:
            print type + " is not a supported resource type."    
