from os.path import isdir, split, isfile
from os import mkdir
from re import compile

__author__="Adam M. Dutko"
__email__="dutko.adam@gmail.com"
__cdate__ ="$Feb 5, 2009 6:19:57 PM$"
__license__ = "GPL v3 or later"

class Crawler(object):
    """
        Default crawler class containing methods to help recursively
        discover the contents of a directory, and return

    """
    def __init__(self,baseDir):
        self.bDir = baseDir

    def is_not_hidden_dir(self,node):

        # find all hidden directories
        hiddenDirs = compile('\.+\w*')

        if hiddenDirs.match(node):
            return False
        else:
            return True

    def is_src_or_header(self,leaf):

        # regex for .h files
        is_header_file = compile('\w*\.h$')

        # regex for .c files
        is_source_file = compile('\w*\.c$')

        if is_header_file.match(leaf):
            return True
        elif is_source_file.match(leaf):
            return True
        else:
            return False

    def _mkdir(self,node):
        if isdir(node):
            pass
        elif node == self.bDir:
            pass
        elif isfile(node):
            raise OSError(node + " is a file.")
        else:
            head, tail = split(node)
            if head and not isdir(head):
                self._mkdir(head)
            print "_mkdir %s" % repr(head)
            if tail:
                mkdir(node)
