#!/bin/bash
#
# dgworks	Bring DegreeWorks up and down.
#
### BEGIN INIT INFO
# Provides: $dgworks
# chkconfig: 3 80 10
# description: Bring DegreeWorks up and down.
### END INIT INFO
#
# 02/05/10 - Created by Adam M. Dutko
#
#
# To add as a service on RedHat (and like) systems:
# 
#      As root 'chkconfig --add dgworks'
#      then configure each runlevel accordingly.
#
# To enable runlevels:
#      
#      chkconfig --level 2345 dgworks on
#
# To disable runlevels:
#     
#      chkconfig --level 2345 dgworks off
#
#
# NOTE: I usually only enable such scripts for runlevel
#       3 like 'chkconfig --level 3 dgworks on' and
#       'chkconfig --level 245 dgworks off'.
#
#
# Include useful init functions
. /etc/init.d/functions

############
## CONFIG ##
############
SCRIPT=dgworks
USER=ADMINUSER
BASE=LOCATIONOFTHISSCRIPT
START=dapstart
STOP=dapstop
RESTART=daprestart
STATUS=dapshow
WEBSTART=webstart
WEBSTOP=webstop
WEBRESTART=webrestart
WEBSTATUS=webshow
VALEUID=0
CUREUID=$(id -u)
#INITLOG=/home/adutko/dwinit.log
INITLOG=/root/dwinit.log

#################################
## DO NOT EDIT BELOW THIS LINE ##
#################################

if [ $CUREUID -ne $VALEUID ]
then
  echo "START : $(date)" >> $INITLOG
  echo "You do not have proper permissions to run $SCRIPT."
  echo "$CUREUID failed to run the $SCRIPT init script." >> $INITLOG
  echo "END : $(date)" >> $INITLOG
  echo "" >> $INITLOG
  
  exit 1
fi

start() {
	echo "$SCRIPT start called at $(date)." >> $INITLOG
	su - $USER -c $BASE$START >> $INITLOG 2>&1 & 
        sleep 5
	su - $USER -c $BASE$WEBSTART >> $INITLOG 2>&1 & 
        sleep 5
	echo "$SCRIPT start finished successfully." >> $INITLOG
	echo "$SCRIPT start finished successfully."
}

stop() {
	echo "$SCRIPT stop called at $(date)." >> $INITLOG
	su - $USER -c $BASE$STOP >> $INITLOG 2>&1 & 
        sleep 5
	su - $USER -c $BASE$WEBSTOP >> $INITLOG 2>&1 & 
        sleep 5
	echo "$SCRIPT stop finished successfully." >> $INITLOG
	echo "$SCRIPT stop finished successfully."
}

case "$1" in
	start)
		start
	;;

	stop)
		stop
	;;

	reload)
		stop
		start
	;;

	restart)
		echo "$SCRIPT restart called at $(date)." >> $INITLOG
		su - $USER -c $BASE$RESTART >> $INITLOG 2>&1 & 
		su - $USER -c $BASE$WEBRESTART >> $INITLOG 2>&1 & 
		echo "$SCRIPT restart finished successfully." >> $INITLOG
		echo "$SCRIPT restart finished successfully."
	;;

	status)
		dap08_cnt=`ps -elf | grep -i dap08 | grep -v grep | wc -l`
		dap09_cnt=`ps -elf | grep -i dap09 | grep -v grep | wc -l`
                echo "dap08 has $dap08_cnt process(es)."
		echo "dap09 has $dap09_cnt process(es)."
		echo "$SCRIPT status called at $(date)." >> $INITLOG
                echo "dap08 has $dap08_cnt processes." >> $INITLOG
		echo "dap09 has $dap09_cnt processes." >> $INITLOG
		echo "" >> $INITLOG
		echo ""
		echo "STATUS : " >> $INITLOG
		su - $USER -c $BASE$STATUS >> $INITLOG
		echo "WEBSTATUS : " >> $INITLOG
		su - $USER -c $BASE$WEBSTATUS >> $INITLOG
		echo "STATUS : " >> $INITLOG
		su - $USER -c $BASE$STATUS
		echo "WEBSTATUS : " >> $INITLOG
		su - $USER -c $BASE$WEBSTATUS
	;;

	*)
	echo "Usage: $0 {start|stop|restart|reload}"
	exit 1
esac

exit 0
