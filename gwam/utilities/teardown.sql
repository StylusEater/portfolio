#!/bin/bash

## NOTE:
# ./teardown.sql 1 -- will drop the user and database
# ./teardown.sql -- will only drop the user

ROOT_PASS='p1$$w30rd'
DB_PASS=mind3m0rk
ADMIN=gwam_admin
DB=gwam
if [ $1 ]; then
	DROP_DB=$1
else
	DROP_DB=0
fi

## Setup user
echo "ROOT: Drop User $ADMIN" 
mysql -u root --password="$ROOT_PASS" -h localhost -e "drop user $ADMIN@'localhost';"

## Do we want to drop the database?
if [ "$DROP_DB" -eq 1 ]; then
	echo "ROOT: Drop Database $DB"
	mysql -u root --password="$ROOT_PASS" -h localhost -e "drop database $DB;"
fi
