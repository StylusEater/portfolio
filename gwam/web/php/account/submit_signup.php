<?php

require "../db/connection.php";
require "../db/query.php";
require "filter.php";
require "../mail/mailer.php";

$login = $_POST['email'];
$pass = $_POST['secret'];
$role = $_POST['role'];

//$DEBUG = 1;

// Global mailer for signup
$mail = new Mailer();

// These need to be corrected from hard coded values
if ($role == "true") {
	$role = "1";
} else {
	$role = "2";
}

// DEBUG
if ($DEBUG) {
	print "ROLE: " . $role . "<br>";
}

// filter parameters
$filter = new Filter();

// $loginstatus is an array comprised of $valid and $msg from Class::Filter
$loginStatus = $filter->validateLogin($login);
// Don't set false to true because that case is used for getting a sanitized password
$passStatus = $filter->validatePass($pass,false);

// DEBUG
if ($DEBUG) {
	print("LOGIN STATUS: " . $loginStatus[0] . "<br>");
	print("LOGIN MSG: " . $loginStatus[1] . "<br>"); 
	print("PASS STATUS: " . $passStatus[0] . "<br>");
	print("PASS MSG: " . $passStatus[1] . "<br>");
	print("PASS GET: " . $passStatus[2] . "<br>");
}

if ($loginStatus[0] && $passStatus[0]) {
	$q = new Query();
	$signup_params = array(":role_id" => $role,":login" => $login,":password" => $pass);

	// Is the user already signed up?
	$alreadyMember = $q->select("check_dup_signup",array(":login" => $login));
	if ($alreadyMember[0]) {
		$signup = false;
		// Query ran OK
		if (count($alreadyMember[1][0]) == 4) {
			// we have values for the check signup query otherwise we don't
			if ($alreadyMember[1][0]["id"] > 0) {
				print "Sorry but you seem to have an account already.<br>";
				if ($alreadyMember[1][0]["active"] == 1) {
					print "It is active so maybe you need to reset your password?<br>";
				} else {
					print "It doesn't seem to be active.  Maybe bug the administrator?<br>";
				}
			}
		} else if (count($alreadyMember[1][0]) > 4) {
			$emsg = "Please contact the administrator as there is a bad account in the database.<br>";
			print $emsg;
			$mail->send("","GWAM: Error in submit_signup.php!","","$login attempted to signup but ran into an issue.  hElP! \r\n$emsg",true);
			die();
		} else {
			$signup = true;
		}

		if ($DEBUG) {
			print "MEMBER ID: " . $alreadyMember[1][0]["id"] . "<br>";
			print "MEMBER STATUS: " . $alreadyMember[1][0]["active"] . "<br>";
			// We die to prevent us from inserting bad juju during debugging
			die();
		}

		// We signup if there isn't an account already with the same login
		if ($signup) { 
			// Try to signup 
			$signupRunStatus = $q->insert("signup",$signup_params);

			if ($signupRunStatus[0]) {
				$emsg = "     Congratulations, you are now signed up for the YOURORGANIZATION GWAM!\n\nThis confirms your signup.  If you signed up for an administrative account, the administrator will be in touch shortly with more details.\nYour account should be activiated within the hour.  If it isn't activated within the hour, please contact the administrator by replying to this e-mail.\n ";
				$smsg = "Congratulations, you are now signed up for the YOURORGANIZATION GWAM!  <br><br>Please check your e-mail for more information.";
				$mail->sendNewAccountNoticeUser($login,"GWAM: Signup Pending",$emsg);
				$mail->sendNewAccountNoticeAdmin("GWAM: New Signup Pending", "Please login and activate $login.");
				print $smsg;
			} else {
				$mail->send("","GWAM: Failed Signup","","$login attempted to signup but ran into an issue.  hElP!",true);
				print "An error occured with the signup process.  Please contact the administrator.";
			}
		}
	} 
} else {

	if (!$loginStatus[0]) {
		print $loginStatus[1];
	} else if (!$passStatus[0]) {
		if (!$passStatus[2]) {
			print $passStatus[1];
		}
	}
}
?>
