<?php

require "../db/connection.php";
require "../db/query.php";
require "filter.php";
require "../mail/mailer.php";

$email = $_POST['user'];
$secret = $_POST['pass'];

// Toggle me to debug 
$DEBUG = false;

// Global mailer for signup
$mail = new Mailer();

// filter parameters
$filter = new Filter();

// $loginstatus is an array comprised of $valid and $msg
$loginStatus = $filter->validateLogin($email);
// $passStatus is an array comprised of $valid,$secret,true
$passStatus = $filter->validatePass($secret,true);
$secret = $passStatus[1];

// DEBUG
if ($DEBUG) {
	print("LOGIN: " . $email . "<br>");
        print("LOGIN STATUS: " . $loginStatus[0] . "<br>");
        print("LOGIN MSG: " . $loginStatus[1] . "<br>");
        print("PASS STATUS: " . $passStatus[0] . "<br>");
        print("PASS SECRET: " . $passStatus[1] . "<br>");
        print("PASS GET: " . $passStatus[2] . "<br>");
}

// the login and password pass the strictness parameters
// this should always be the case if the user is a valid signed up user 
// NOT REALLY NEEDED since they're signed up 
// if ($loginStatus[0] && $passStatus[0]) {

$valid = false;
$msg = "";
$q = new Query();
$auth_params = array(":login" => $email,":password" => $secret);
$role = "";

// Is the user already signed up?
$login = $q->select("login",$auth_params);
if ($login[0]) {
	// Query ran OK
	if (count($login[1][0]) == 8) {
		if ($login[1][0]["active"] != 1) {
			$msg = "Your account is not active.  Please contact the administrator for help.<br>";
		$mail->send("","GWAM: Login Error","",$login[1][0]["login"] . " attempted to login but failed.  Account is not active!",true);
		header("Location: http://" . $_SERVER['HTTP_HOST'] . "/php/account/failed.php?msg=$msg");
		} else if ($login[1][0]["active"] == 1) {
	
			session_start();
	
			$valid = true;
			$host = $_SERVER['HTTP_HOST'];
			$admin = "/php/admin/";
			$user = "/php/user/";
			$email = $login[1][0]["login"];
			$role = $login[1][0]["role_id"];
			$id = $login[1][0]["id"];
			$msg = $msg . $email . " logged in.";
	
			$_SESSION['login'] = $email;
			$_SESSION['role'] = $role;
			$_SESSION['user_id'] = $id;
			$_SESSION['message'] = $msg;
			
			// Which page do we send them to?
			if ( $role == "1" ) {
				// the user is an admin
				if (!$DEBUG) {
					header("Location: http://$host$admin");
				}
			} else if ($role == "2") {
				// the user is a regular user
				if (!$DEBUG) {
					header("Location: http://$host$user");
				}
			} else {
				// How the heck did we get here?
				$mail->send("","GWAM: Error in submit_login.php!","","$email authenticated but has an odd role of $role_id.  Help! \r\n$msg",true);
			}
		}
    $mail->send("","GWAM: User Login","",$login[1][0]["login"] . " logged in to the site.",true);

	} else if (count($login[1][0]) > 8) {
			$emsg = "Please contact the administrator as there is a bad account in the database.<br>";
			$mail->send("","GWAM: Error in submit_login.php!","",$login[1][0]["login"] . " attempted to authenticate but ran into an issue.  hElP! \r\n$emsg",true);
			$msg = $msg . $emsg;
	} else {

		// wrong password or login
		$msg = $msg . "Sorry but you either typed the wrong password or used an invalid YOURORGANIZATION email address.  Please try again.";
		if ($login[1][0]["login"]) {
			$login = $login[1][0]["login"];
		} else {
			if ($email == "") {
				$email = "failed_login@YOURORGANIZATION.com";
			}
			$login = $email;
		}

		$mail->send("","GWAM: Login Error","",$login . " attempted to login but failed.",true);
		header("Location: http://" . $_SERVER['HTTP_HOST'] . "/php/account/failed.php?msg=$msg");
	}	

	// DEBUG
	if ($DEBUG) {
		print "MEMBER ID: " . $login[1][0]["id"] . "<br>";
		print "MEMBER STATUS: " . $login[1][0]["active"] . "<br>";
		print "MEMBER NAME: " . $login[1][0]["login"] . "<br>";
		print "MEMBER ROLE: " . $role . "<br>";
		var_dump($_SESSION) . "<br>";
		die();
	}

}


?>
