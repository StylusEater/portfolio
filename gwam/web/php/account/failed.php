<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
            "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
<title>GWAM: Failed Login</title>
<script src="../../js/login.js" type="text/javascript"></script>
<link rel="stylesheet" type="text/css" href="../../css/failed_login.css"> 
</head>
<body>
<div id="failed_login" alt="GWAM Failed Login">
    <p id="failed_login_msg">
        <?php 
            echo $_GET['msg'];
        ?>
    </p>
    <a href="http://gwam.YOURORGANIZATION.com">
        <div id="failed_login_button" alt="Failed Login Button">
            Try Again
        </div>
    </a>
</div> 
</body>
</html>
