<div id="signup_form_holder" alt="Holder for GWAM Signup Form">
   <!-- p id="signup_title">YOURORGANIZATION GWAM Signup</p -->
   <form id="signup" alt="GWAM Signup Form" method="post" action="javascript:submit_signup($('signup')['email'].value,$('signup')['secret'].value,$('signup')['isadmin'].checked)"> 
   <!-- action="php/account/submit_signup.php" -->
      <label id="email" alt="GWAM E-Mail Label">Username </label>
      <input id="email" name="email" type="text" alt="GWAM EMail Field" onclick="clear('email');"/>
      <br /><br />
      <label id="secret" alt="GWAM Secret Label">Password </label>
      <input id="secret" name="secret" type="password" alt="GWAM Secret Field" onclick="clear('secret');"/>
      <br /><br />
      <input type="button" id="cancel_button" value="Cancel" alt="GWAM Cancel Signup Button" onclick="cancel_signup();"/>
      <input type="submit" id="signup_button" value="Signup" alt="GWAM Signup Button" />
      <label id="isadmin" alt="GWAM Admin Label">Admin? </label>
      <input type="checkbox" name="role" id="isadmin" alt="GWAM Admin Checkbox" value="no"/>
   </form>
</div>
