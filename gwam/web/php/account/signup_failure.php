<div id="failuremsg" alt="GWAM Failure Message Box">
</div>
<br /><br />
<div id="after_signup_links" alt="GWAM Signup and Password Reset Links">
   <a href="/" id="login" alt="Login to GWAM">Login</a> |
   <a href="javascript:resetPassword();" id="forgot_pass" alt="Forgot Password Link">Forgot Password</a> | 
   <a href="javascript:signup();" id="signup" alt="Signup Link">Signup</a>
</div>
