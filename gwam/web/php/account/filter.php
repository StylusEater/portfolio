<?php

if (!class_exists('SiteSettings')) {
    require_once '../conf/settings.php';
}

class Filter {

    private $SETTINGS;

    function __construct() {
	$this->SETTINGS = new SiteSettings();	
    }

    function __destruct() {
    }

    function validateLogin($login) {
	// Adapted from http://www.linuxjournal.com/article/9585?page=0,1
	$valid = true;
	$index = strrpos($login, "@");
	$msg = "$login is valid.";
	
	// Verify we didn't just get an @ sign
	if (is_bool($index) && !$index) {
		$valid = false;
	} else {
		// Break the address into parts
		$domain = substr($login,$index+1);
		$local = substr($login,0,$index);	
		$localLen = strlen($local);
		$domainLen = strlen($domain);

		if ($local == "youremail") {
			// Did they just use the default?
	        	$valid = false;
			$msg = "Please don't just use the default username 'youremail@YOURORGANIZATION.com'.";
		} else if ($domain != "YOURORGANIZATION.com") {
			// Make sure they're from YOURORGANIZATION 
			$valid = false;
			$msg = "Only YOURORGANIZATION users can use this portal.";
		} else if ($localLen < 1 || $localLen > 64) {
			// Violates RFC 2822 for username length
			$valid = false;
			$msg = "The username you supplied is too short or too long.";
		} else if ($domainLen < 1 || $domainLen > 255) {
			// Violates RFC 2822 for domain name length
			$valid = false;
			$msg = "The domain you supplied is too short or too long.";
		} else if ($local[0] == '.' || $local[$localLen-1] == '.') {
			// Violates RFC 2822 for valid start characters in username
			$valid = false;
			$msg = "The username you supplied starts or ends with a period.";
		} else if (preg_match('/\\.\\./',$local)) {
			// Violates RFC 2822 because username cannot have two consecutive dots
			$valid = false;
			$msg = "The username you supplied has two consecutive dots.";
		} else if (!preg_match('/^[A-Za-z0-9\\-\\.]+$/',$domain)) {
			// Violates RFC 2822 for valid characters in the domain name
			$valid = false;
			$msg = "The domain name you supplied contains invalid characters."; 
		} else if (preg_match('/\\.\\./',$domain)) {
			// Violates RFC 2822 because domain name cannot have two consecutive dots
			$valid = false;
			$msg = "The domain you supplied has two consecutive dots.";
		} else if (!preg_match('/^(\\\\.|[A-Za-z0-9!#%&`_=\\/$\'*+?^{}|~.-])+$/',str_replace("\\\\","",$local))) {
			if (!preg_match('/^"(\\\\"|[^"])+"$/',str_replace("\\\\","",$local))) {
				$valid = false;
				$msg = "You gave a character that needs to be quoted in order to be used.";
			}
		}

		// We probably won't ever get this error unless yourorganization.com ceases to be a valid domain
		if ($valid && !(checkdnsrr($domain,"MX") || checkdnsrr($domain,"A"))) {
			$valid = false;
			$msg = "The domain you supplied doesn't have a valid MX record or isn't a valid domain.";
		}

		// DEBUG
		if ($DEBUG) {
			if ($valid) {
				print $msg;
			} else {
				print $msg;
			}	
		}	

		return array($valid, $msg);
	}	

    }

    function validatePass($secret,$sanitize) {
	$valid = true;
	$msg = "Password passes complexity rules.";

	// These rules are somewhat "arbitrary" and can be adjusted with 
	// the following parameters:
	//$minLen = "8";
	$minLen = $this->SETTINGS->PLENGTH;
	//$minNum = "1";
	$minNum = $this->SETTINGS->PNUMS;
	//$minChars = "1";
	$minChars = $this->SETTINGS->PCHARS;
	$valNums = array("0","1","2","3","4","5","6","7","8","9");
	$valChars = array("!","\"","#","\$","%","&","'","(",")","*","+",",","-",".","/","\\",":",";","<","=",">","?","@","[","]","^","_","`","{","}","|","~");
	//$minUniq = "6";
	$minUniq = $this->SETTINGS->PUNIQ;

        // Remove special chars from password
	// as referenced at http://php.net/manual/en/function.trim.php
	$secret = trim($secret);

	// Is the password at least length $minLen?
	if (strlen($secret) < $minLen) {
		$valid = false;
		$msg = "The password you selected is not long enough.  It needs to be at least $minLen long.";
	}

	// Does the password contain at least $minNum number(s)?
	$numCount = count_chars($secret,1);
	$numKeys = array_keys($numCount);
	$numsInPass = 0;
	foreach ($numKeys as $num) {
		if (in_array(chr($num),$valNums)) {
			$numsInPass++;
		}
	}
	if ($numsInPass < $minNum) {
		$valid = false;
		$msg = "The password you selected does not have at least $minNum number(s).";	
	}

	// Does the password contain at least $minChars non alphanumeric characters?
	$charCount = count_chars($secret,1);
	$charKeys = array_keys($charCount);
	$charsInPass = 0;
	foreach ($charKeys as $char) {
		if (in_array(chr($char),$valChars)) {
			$charsInPass++;
		}
	}
	if ($charsInPass < $minChars) {
		$valid = false;
		$msg = "The password you selected does not have at least $minChars special character(s).";
	}		

	// Does the password reuse too many characters?
	if (count(count_chars($secret,1)) < $minUniq) {
		$valid = false;
		$msg = "You repeated too many characters and/or numbers in your password.";
	}

	// Bad juju to couple but we use this function twice to clean the 
	// password before sending it to the DB	
	if ($sanitize) {
		$results = array($valid,$secret,true);
	} else {
		$results = array($valid,$msg,false);
	}

	return $results;	
	
    }

}

?>
