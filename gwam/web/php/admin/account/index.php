<?php
if (!class_exists('SiteSettings')) {
    require '../../conf/settings.php';
}
$settings = new SiteSettings();

// Validate they are logged in
if (!session_is_registered('login')) {
	Header("Location: http://" . $_SERVER['HTTP_HOST']);
} else if ($_SESSION['role'] != 1) {
	if ($_SESSION['role'] == 2) {
		Header("Location: http://" . $_SERVER['HTTP_HOST'] . "/php/user/");
	} else {
		Header("Location: http://" . $_SERVER['HTTP_HOST']);
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
            "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
   <title>ADMIN ::. <?php echo $settings->STITLE ?></title>
   <link rel="stylesheet" href="/css/main.css" type="text/css" />
   <link rel="stylesheet" href="/css/admin.css" type="text/css" />
   <script type="text/javascript" src="/js/prototype.js"></script>
   <script type="text/javascript" src="/js/account.js"></script>
</head>
<body>
<div id="menu">
   <a id="gwam_user_mgmt" href="/php/admin/panel/">admin panel</a>
   <a id="admin_logout" href="/php/admin/logout.php">logout</a>
   <a id="gwam_panel" href="/php/admin/">gwam panel</a> 
</div>
<p id="login_title">YOURORGANIZATION GWAM</p>

<div class="settings_content">
   <fieldset>
   <legend id="user_mgmt_legend">Account Settings</legend>
   <form id="user_mgmt_form" alt="GWAM: User Account Settings Form">
        <div id="user_mgmt_errors" >
        </div>
	<p>
           <label id="login" name="login">Login</label>
	   <input type="text" id="login" name="login" value="<?php echo $_SESSION['login']; ?>" disabled>
	</p>
        <p>
	   <label id="password" name="password">Password</label>
	   <input type="password" id="password" name="password" value="password">
	<p>
	</p>
	<p>
	   <label id="confpassword" name="confpassword">Confirm Password</label>
	   <input type="password" id="confpassword" name="confpassword" value="password">
	</p>
	<p>
	   <input type="button" value="submit" onclick="javascript:updateAccount();">
        </p>
   </form>
   </fieldset>
</div>

<?php echo $FOOTER ?>
</body>
</html>
