<?php
if (!class_exists('SiteSettings')) {
    require '../../conf/settings.php';
}
$settings = new SiteSettings();

// Validate they are logged in
if (!session_is_registered('login')) {
	Header("Location: http://" . $_SERVER['HTTP_HOST']);
} else if ($_SESSION['role'] != 1) {
	if ($_SESSION['role'] == 2) {
		Header("Location: http://" . $_SERVER['HTTP_HOST'] . "/php/user/");
	} else {
		Header("Location: http://" . $_SERVER['HTTP_HOST']);
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
            "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
   <title>ADMIN ::. <?php echo $settings->STITLE ?></title>
   <link rel="stylesheet" href="/css/main.css" type="text/css" />
   <link rel="stylesheet" href="/css/admin.css" type="text/css" />
   <link rel="stylesheet" href="/css/admin_panel.css" type="text/css" /> 
   <script type="text/javascript" src="/js/prototype.js"></script>
   <script type="text/javascript" src="/js/admin_panel.js"></script>
</head>
<body>
<div id="menu">
   <a id="acct_mgmt" href="/php/admin/account/">settings</a>
   <a id="admin_logout" href="../logout.php">logout</a>
   <a id="gwam_panel" href="/php/admin/">gwam panel</a> 
</div>
<p id="login_title">YOURORGANIZATION GWAM</p>

<div class="admin_panel_content">
   <fieldset>
   <legend id="site_settings_legend">Admin Panel</legend>
   <form id="site_settings_form" alt="GWAM: Admin Panel Form">
	<p>
	   <label class="title">Site Settings</label>
        </p>
        <p id="site_errors" style="display: none;"></p>
        <br>
	<p>
           <label id="stitle" name="stitle">Site Title</label>
	   <input type="text" size="60" id="stitle" name="stitle" value="<?php echo $settings->STITLE; ?>">
	</p>
	<br>
	<p>
	   <label class="title">Guest Settings</label>
        </p>
        <p id="guest_errors" style="display: none;"></p>
        <br>
	<p>
           <label id="gprefix" name="gprefix">Guest Prefix</label>
	   <input type="text" id="gprefix" name="gprefix" value="<?php echo $settings->GPREFIX; ?>">
	</p>
        <p>
	   <label id="gmax" name="gmax">Guest Limit</label>
	   <input type="text" id="gmax" name="gmax" value="<?php echo $settings->GMAX; ?>">
	<p>
	</p>
        <br>
        <p>
           <label class="title">Password Settings</label>
        </p>
        <p id="pass_errors" style="display: none;"></p>
        <br>
	<p>
	   <label id="plength" name="plength">Required Length</label>
	   <input type="text" id="plength" name="plength" value="<?php echo $settings->PLENGTH; ?>">
	</p>
	<p>
	   <label id="pnums" name="pnums">Required Numbers</label>
	   <input type="text" id="pnums" name="pnums" value="<?php echo $settings->PNUMS; ?>">
	</p>
	<p>
	   <label id="pchars" name="pchars">Required Special Chararacters</label>
	   <input type="text" id="pchars" name="pchars" value="<?php echo $settings->PCHARS; ?>">
	</p>
        <p>
	   <label id="puniq" name="puniq">Required Unique Characters</label>
	   <input type="text" id="puniq" name="puniq" value="<?php echo $settings->PUNIQ; ?>">
	</p>
	<p>
	   <input class="submt" type="button" value="submit" onclick="javascript:editSettings();">
        </p>
   </form>
   </fieldset>
</div>

<?php echo $FOOTER ?>
</body>
</html>
