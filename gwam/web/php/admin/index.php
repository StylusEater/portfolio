<?php
if (!class_exists('SiteSettings')) {
    require '../conf/settings.php';
} 
$settings = new SiteSettings();

// Validate they are logged in
if (!session_is_registered('login')) {
	Header("Location: http://" . $_SERVER['HTTP_HOST']);
} else if ($_SESSION['role'] != 1) {
	if ($_SESSION['role'] == 2) {
		Header("Location: http://" . $_SERVER['HTTP_HOST'] . "/php/user/");
	} else {
		Header("Location: http://" . $_SERVER['HTTP_HOST']);
	}
}

?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
            "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
   <title>ADMIN ::. <?php echo $settings->STITLE ?></title>
   <link rel="stylesheet" href="/css/main.css" type="text/css" />
   <link rel="stylesheet" href="/css/admin.css" type="text/css" />
   <link rel="stylesheet" href="/css/wizard.css" type="text/css" />
   <link rel="stylesheet" href="/css/guest.css" type="text/css" />
   <script type="text/javascript" src="/js/prototype.js"></script>
   <script type="text/javascript" src="/js/wizard.js"></script>
</head>
<body>
<div id="wizardHolder" style="display: none;">
</div>
<div id="menu">
   <a id="gwam_user_mgmt" href="panel/">admin panel</a>
   <a id="acct_mgmt" href="account/">settings</a>
   <a id="admin_logout" href="logout.php">logout</a>
</div>
<p id="login_title">YOURORGANIZATION GWAM</p>

<div class="content_menu">
   <a href="javascript:openWizard();" id="wizard">Add Guest</a>
</div>

<!-- EXAMPLE div class="content" id="0">
   <ul class="item" id="0">
      <a href="" class="edit">Edit</a>
      <a href="" class="delete">Delete</a>
   </ul>
   <p class="content" id="0">
      YOURORGANIZATION_Guest_00
   </p>
</div -->

<?php include "../mgmt/show_guests.php";

if (count($GUESTS) == 0) {
    print "No active guest accounts.";
} else {
    $numGuests = count($GUESTS);
    $curGuest = 0;
    for ($curGuest = 0; $curGuest < $numGuests; $curGuest++) {
        $guestID = $curGuest; 
        $curGuestName = $GUESTS[$curGuest]["name"];
        $curGuestPass = $GUESTS[$curGuest]["pass"];
        $curGuestFname = $GUESTS[$curGuest]["fname"];
        $curGuestLname = $GUESTS[$curGuest]["lname"];
        echo "<div class=\"content\" id=\"$guestID\">
                  <ul class=\"item\" id=\"$guestID\">
                      <a href=\"javascript:editGuest('$curGuestName','$guestID');\" class=\"edit\">Edit</a>
                      <a href=\"javascript:confirmUserDelete('$curGuestName');\" class=\"delete\">Delete</a>
                  </ul>
                  <p class=\"content\" id=\"$guestID\">
                      <b>Guest</b>: &nbsp;" .
                      $curGuestFname . " " . $curGuestLname .
                      "<br><br>
                      <b>Account</b>: &nbsp;" .  
                      $curGuestName  .
                      "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" .
                      "<b>Password</b>: &nbsp;" .
                      $curGuestPass .
                  "</p>
              </div>
              <div style=\"display: none;\" class=\"editcontent\" id=\"edit$guestID\">
	      </div>
             ";
    }
}

?>

<?php echo $settings->SCOPY ?>
</body>
</html>
