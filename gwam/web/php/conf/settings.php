<?php

$BDIR = "/var/www/gwam";

if (!class_exists('Connection')) {
    require $BDIR . '/php/db/connection.php';
}

if (!class_exists('Query')) {
    require $BDIR . '/php/db/query.php';
}

// AS OF 3/15/10 these manual settings are deprecated in favor of the database settings held in the settings table
//$TITLE="YOUR ORGANIZATION - Guest Wireless Access Manager (GWAM)";
//$FOOTER="<p id='footer'>YOUR ORGANIZATION<br />YOUR ORGANIZATION<br />&#169; 2010 - AMD<br /><br /></p>";

// guest users
//$GUEST_TEMPLATE = "lccguest";
//$GUEST_MAX = 20;

// We want to return an array with the following fields:
// stitle
// gprefix
// gmax
// plength
// pnums
// pchars 

class SiteSettings {

    public $STITLE;
    public $SCOPY;
    public $GPREFIX;
    public $GMAX;
    public $PLENGTH;
    public $PNUMS;
    public $PCHARS;
    public $PUNIQ;

    function __construct() {
         
        $defSettings = new Query();
        $settings_params = array();
        $getDefSettings = $defSettings->select("get_settings",$settings_params);

	$defSettingValues = array();
        $numSettings = count($getDefSettings[1]);
        for ($i = 0; $i < $numSettings; $i++) {
	    $key = $getDefSettings[1][$i]["option_key"];
	    $val = $getDefSettings[1][$i]["option_value"];
	    
            if ($key == "stitle") {
		$this->STITLE = $val;
	    } else if ($key == "gprefix") {
		$this->GPREFIX = $val;
	    } else if ($key == "gmax") {
		$this->GMAX = $val;
	    } else if ($key == "plength") {
		$this->PLENGTH = $val;
	    } else if ($key == "pnums") {
		$this->PNUMS = $val;
	    } else if ($key == "pchars") {
	 	$this->PCHARS = $val;
	    } else if ($key == "puniq") {
		$this->PUNIQ = $val;
	    } else if ($key == "scopy") {
		$this->SCOPY = $val;
	    }
	}
    }


}

?>
