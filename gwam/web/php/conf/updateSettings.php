<?php

if (!class_exists('Connection')) {
    require '../db/connection.php';
}
if (!class_exists('Query')) {
    require '../db/query.php';
}

// Values from form 
$stitle = $_POST['stitle'];
$gprefix = $_POST['gprefix'];
$gmax = $_POST['gmax'];
$plen = $_POST['plength'];
$pchar = $_POST['pchars'];
$pnum = $_POST['pnums'];
$puniq = $_POST['puniq'];

$settings = new Query();
$settings_params_title = array(":stitle" => $stitle);
$settings_params_prefix = array(":gprefix" => $gprefix);
$settings_params_max = array(":gmax" => $gmax);
$settings_params_length = array(":plength" => $plen);
$settings_params_chars = array(":pchars" => $pchar);
$settings_params_nums = array(":pnums" => $pnum);
$settings_params_uniq = array(":puniq" => $puniq);
$updateSettingsTitle = $settings->update("update_settings_title",$settings_params_title);
$updateSettingsGprefix = $settings->update("update_settings_prefix",$settings_params_prefix);
$updateSettingsGmax = $settings->update("update_settings_max",$settings_params_max);
$updateSettingsPlength = $settings->update("update_settings_length",$settings_params_length);
$updateSettingsPnums = $settings->update("update_settings_nums",$settings_params_nums);
$updateSettingsPchars = $settings->update("update_settings_chars",$settings_params_chars);
$updateSettingsPuniq = $settings->update("update_settings_uniq",$settings_params_uniq);

echo "Done";

?>
