<?php

class MailSettings {

	public $DEF_FROM;
	public $DEF_TO;
	public $DEF_SUBJECT;
	public $DEF_MSG;
	public $DEF_HDRS;
	public $DEF_FULL_HDRS;

	function __construct() {
		$this->DEF_FROM = "adutko@YOURORGANIZATION.com";
		$this->DEF_TO = "adutko@YOURORGANIZATION.com";
		$this->DEF_SUBJECT = "GWAM: Generic Message";
		$this->DEF_MSG = "This is a generic message from the YOURORGANIZATION GWAM system.";
		$this->DEF_HDRS = 'X-Mailer: PHP/' . phpversion();
		$this->DEF_FULL_HDRS = 'From: ' . $this->DEF_FROM . "\r\n" . 'Reply-To: ' . $this->DEF_FROM . "\r\n" . $this->DEF_HDRS;
	}

	function __destruct() {
	}

}

?>
