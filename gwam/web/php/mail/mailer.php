<?php

// We define the following in settings.php:
// 	$DEF_FROM
//      $DEF_TO
//      $DEF_SUBJECT
//	$DEF_MSG
//	$DEF_HDRS -- just has the X-Mailer piece in it.
//	$DEF_FULL_HDRS -- also has From:, Reply-To pieces seperated by "\r\n"
require_once 'settings.php';

    class Mailer {
	
	private $DEF_TO_;
	private $DEF_FROM_;
	private $DEF_MSG_;
	private $DEF_SUBJECT_;
	private $DEF_HDRS_;
	private $DEF_FULL_HDRS_;	
	
	function __construct() {
		$msettings = new MailSettings();		
		$this->DEF_TO_ = $msettings->DEF_TO; 
		$this->DEF_FROM_ = $msettings->DEF_FROM; 
		$this->DEF_MSG_ = $msettings->DEF_MSG; 
		$this->DEF_SUBJECT_ = $msettings->DEF_SUBJECT; 
		$this->DEF_HDRS_ = $msettings->DEF_HDRS;
		$this->DEF_FULL_HDRS_ = $msettings->DEF_FULL_HDRS;
	}

	function __destruct() {
	}

	function sendNewAccountNoticeUser($user,$subj,$notice) {

		// could rely on polymorphism but bah...this is fine for now
		$this->send($user,$subj,"",$notice,true);
		
	}

	function sendNewAccountNoticeAdmin($subj,$notice) {
	
		// could rely on polymorphism but bah...this is fine for now
		$this->send("",$subj,"",$notice,true);
		
	} 

	function send($to="",$subj="",$from="",$msg="",$defhdrs) {
		// $defhdrs is boolean

		// Did the person define a destination?
		if ($to == "") {
			$to = $this->DEF_TO_;
		}  

		// Did the person define a sender? 
		if ($from == "") {
			$from = $this->DEF_FROM_;
		}

		// Did the person define a message?
		if ($msg == "") {
			$msg = $this->DEF_MSG_;
		}

		// Did the person define a subject?
		if ($subj == "") {
			$subj = $this->DEF_SUBJECT_;
		}

		// Did the person give us headers?
		if ($defhdrs) {
			$hdrs = $this->DEF_FULL_HDRS_;
		} else {
			$hdrs = 'From: ' . $from . "\r\n" . 'Reply-To: ' . $from . "\r\n" . $this->DEF_HDRS_;
		}

		if ($DEBUG) {	
			print "TO: " . $to . "\n";
			print "SUBJ: " . $subj . "\n";
			print "MSG: " . $msg . "\n";
			print "HDRS: " . $hdrs . "\n";
		}

		mail($to,$subj,$msg,$hdrs); 
		 
	}

    }

?>
