<?php

require_once '../db/connection.php';
require_once '../db/query.php';


// form id=editGuest
$gwamuser = $_POST["gwamname"];
$fname = $_POST["fname"];
$lname = $_POST["lname"];
$email = $_POST["email"];
$dvalid = $_POST["dvalid"];
$editrowid = $_POST["editrow"];

$editGuest = new Query();
$guest_edit_params = array(":gwamname" => $gwamuser, ":fname" => $fname, ":lname" => $lname, ":email" => $email, ":dvalid" => $dvalid);
$editGuestStatus = $editGuest->update("update_guest",$guest_edit_params);

if ($editGuestStatus[0]) {

    $response = "
        <div id=\"guestInfo\">
            Successfully edited guest.  
            <p>
                <input type=\"button\" id=\"guestEditDoneButton\" value=\"Done\" onclick=\"javascript:doneEditGuest('$editrowid');\">
            </p>
        </div>
    ";

} else {

    $response = "
        <div id=\"guestInfo\">
            Failed to edit guest.  
            <p>
                <input type=\"button\" id=\"guestEditDoneButton\" value=\"Done\" onclick=\"javascript:doneEditGuest('$editrowid');\">
            </p>
        </div>
    ";

}

echo $response;
	
?>
