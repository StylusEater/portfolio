<?php

require "../db/connection.php";
require "../db/query.php";

$gwamuser = $_POST['guest'];

$deactivateGuest = new Query();
$guest_params = array(":gwam_login" => $gwamuser, ":active" => 0);
$guestDeactivated = $deactivateGuest->update("deactivate_guest",$guest_params);

if ($guestDeactivated[0]) {

    echo "<div id=\"wizbg\">
          <div id=\"wizFrmHolder\">
          <fieldset id=\"wizField\">
          <legend id=\"wizLegend\">Guest Deleted</legend>
          <form id=\"wizard\" alt=\"GWAM: Delete Guest Confirmation\" >
             <p>$gwamuser deleted successfully.</p> 
             <p><input id=\"cancel\" type=\"button\" value=\"Done\" onclick=\"javascript:closeWizard();\"/>
          </form>
          </fieldset> 
          </div>
          </div> 
         ";

} else {

    echo "<div id=\"wizbg\">
          <div id=\"wizFrmHolder\">
          <fieldset id=\"wizField\">
          <legend id=\"wizLegend\">Guest Delete Failed</legend>
          <form id=\"wizard\" alt=\"GWAM: Delete Guest Failure\" >
             <p>Failed to delete $gwamuser.</p> 
             <p><input id=\"cancel\" type=\"button\" value=\"Done\" onclick=\"javascript:closeWizard();\"/>
          </form>
          </fieldset> 
          </div>
          </div> 
         ";

}

?>
