<?php

require_once "../db/connection.php";
require_once "../db/query.php";

class Wizard {

    private $GUEST_PREFIX; 

    function __construct($guest="lccguest") {
	$this->GUEST_PREFIX = $guest;
    }

    function __destruct() {
    }

    function canWeAddMoreGuests($maxGuest) {
	$guestCount = new Query();
	$count_params = array(":active" => 1);
	$count = $guestCount->select("get_active_guests",$count_params);

	if (count($count) != 2) {
            // result looks weird
	    return false;
	} else {
	    $numActiveGuests = $count[1][0]["cnt"];
	    if ($numActiveGuests < $maxGuest) {
		return false;
	    } else {
                return true;
            }
	}
    }

    function genUser($params) {
        // get next id for user in db
        // use the id column as guest number then append to lccuser
	$fname = $params["fname"];
        $lname = $params["lname"];
        $admin = $_SESSION["login"];
        $login = $this->GUEST_PREFIX;
        $email = $params["email"];
	$password = $this->genPass();
	$daysvalid = $params["dvalid"];
        $maxActiveGuests = $params["max"];

        // first we need to check if we have max active guests in the db
	// if we don't then we create a new user
	$maxActiveGuestsReached = $this->canWeAddMoreGuests($maxActiveGuests); 

	if ($maxActiveGuestsReached) {
	    return array("FAILED");
        } else {
            $newGuest = new Query();
            $guest_params = array(":admin" => $admin, ":login" => $login, ":fname" => $fname, ":lname" => $lname, ":email" => $email, ":password" => $password, ":validfor" => $daysvalid); 

            $addedGuest = $newGuest->insert("add_guest",$guest_params);
	    // $addedGuest[0] = status
	    // $addedGuest[1] = message 
	    // $addedGuest[2] = lastid 
	    if ($addedGuest[0]) {
		$updateNewGuest = new Query();
		$update_guest_params = array(":login" => $login . $addedGuest[2], ":id" => $addedGuest[2]);
		$updatedGuest = $updateNewGuest->update("update_add_guest",$update_guest_params);
	    }

	    return array($login . $addedGuest[2], $password);
        }

    }

    function genPass() {
        // init password
        $pass = "";
	// password length
	$maxLen = 8;
	// Valid Characters
  	$valChars = "0123456789bcdfghjkmnpqrstvwxyz!@#$%^&*()-+=_"; 
   
        $i = 0; 
        while ($i < $maxLen) { 
	    $nextChar = substr($valChars, mt_rand(0, strlen($valChars)-1), 1);
    	    if (!strstr($pass, $nextChar)) { 
                $pass .= $nextChar;
                $i++;
            }
        }

        return $pass;

    }

    

}

?>
