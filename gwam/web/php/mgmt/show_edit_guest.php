<?php

require_once "../db/connection.php";
require_once "../db/query.php";

// form id=editGuest
$gwamuser = $_GET['guest'];
$editrowid = $_GET['id'];

$guest_params = array("gwamname" => $gwamuser);
$guestInfo = new Query();
$getGuestInfo = $guestInfo->select("get_guest_info",$guest_params);

$fname = $getGuestInfo[1][0]["fname"];
$lname = $getGuestInfo[1][0]["lname"];
$email = $getGuestInfo[1][0]["email"];
$dvalid = $getGuestInfo[1][0]["dvalid"];
$created = $getGuestInfo[1][0]["created"];

// calculate time left
$seconds_valid = $dvalid * 60 * 60 * 24;
$curtime = time();
$seconds_left = ($created + $seconds_valid) - $created;

// we're not really precise with leftovers and time
if (($seconds_left < (60*60*24)) && ($seconds_left > 0)) {
    $days_left = 1;
} else {
    //$remainder = $seconds_left % (60*60*24);
    $days_left = round($seconds_left / (60*60*24));
    if ($days_left == 0) {
        $days_left = 1;
    } 
}

?>

<form class="guest" id="guest<?php echo $editrowid; ?>" alt="GWAM: Edit Guest Form" >
   <p id="guestErrors" style="display: none;"></p>
   <p><label id="FirstName">First Name: </label></p>
   <p><input type="text" id="fname" name="fname" value="<?php echo $fname ?>" /></p> 
   <p><label id="LastName">Last Name: </label></p>
   <p><input type="text" id="lname" name="lname" value="<?php echo $lname ?>" /></p> 
   <p><label id="E-Mail">E-Mail Address: </label></p>
   <p><input type="text" id="email" name="email" value="<?php echo $email ?>" /></p> 
   <p><label id="Duration">Days Left: </label></p>
   <p><input type="text" id="dvalid" name="dvalid" size="3" maxlength="3" value="<?php echo $days_left ?>" /></p> 
   <p><input id="cancel" type="button" value="Cancel" onclick="javascript:cancelEditGuest('<?php echo $editrowid ?>');"/>
      <input id="submit" type="button" value="Save" onclick="javascript:submitEditGuest('<?php echo $gwamuser ?>','<?php echo $editrowid ?>');"/></p>
</form>
