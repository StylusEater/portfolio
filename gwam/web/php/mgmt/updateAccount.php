<?php

require_once '../db/connection.php';
require_once '../db/query.php';


// form id=user_mgmt_form
$acct = $_POST["login"];
$pass = $_POST["pass"];

$editAccount = new Query();
$account_edit_params = array(":login" => $acct, ":password" => $pass);
$editAcctStatus = $editAccount->update("update_account",$account_edit_params);

if ($editAcctStatus[0]) {
    $response = "
        <br>
        <div id=\"accountInfo\">
            Successfully edited account.  
            <p>
                <input type=\"button\" id=\"accountEditDoneButton\" value=\"Done\" onclick=\"javascript:cancelEditAccount();\">
            </p>
        </div>
    ";

} else {

    $response = "
        <br>
        <div id=\"accountInfo\">
            Failed to edit account.  
            <p>
                <input type=\"button\" id=\"accountEditDoneButton\" value=\"Done\" onclick=\"javascript:cancelEditAccount();\">
            </p>
        </div>
    ";

}

echo $response;
	
?>
