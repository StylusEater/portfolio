<?php

if (!class_exists('SiteSettings')) {
    require '../conf/settings.php';
}
require 'wizard.php';

$settings = new SiteSettings();

// form id=wizard
$fname = trim($_POST['fname']);
$lname = trim($_POST['lname']);
$email = trim($_POST['email']);
$dvalid = trim($_POST['dvalid']);

// guest settings from ../conf/settings.php
$guestPrefix = $settings->GPREFIX;
$guestMax = $settings->GMAX;

// if (ParametersAreGood <-- done with JS) {
//    Generate User
//    Generate Password
//    Insert information into openldap
//    if (openldapWorked) {
//       Insert User and Password into guest table
//       Display Information to User with close button
//    } else {
//       Display error to user
//    }
// }

$wizard = new Wizard($guestPrefix);

$guestParams = array("fname" => $fname, "lname" => $lname, "email" => $email, "dvalid" => $dvalid, "max" => $guestMax);

// generate gwam temporary user
$guestInfo = $wizard->genUser($guestParams);

if ($guestInfo[0] == "FAILED") {

    $response = "
        <div id=\"guestInfo\">
            Failed to create user because there are too many active guest accounts.  Delete some?  
            <p>
                <input type=\"button\" id=\"wizDoneButton\" value=\"Done\" onclick=\"javascript:closeWizard();\">
            </p>
        </div>
    ";

} else {
    $newGuestName = $guestInfo[0];
    $newGuestPass = $guestInfo[1];

    $response = "
        <div id=\"guestInfo\">
            <form id=\"guestAcct\">
                <p id=\"gAInfo\">
                    <p><label id=\"gATitle\">Guest Account:</label></p>
                    <p><label id=\"gAVal\">$newGuestName</label></p>
                </p>
		<br>
                <p id=\"gPInfo\">
                    <p><label id=\"gPTitle\">Guest Password: </label></p>
                    <p><label id=\"gPVal\">$newGuestPass</label></p>
                </p>
                <br>
                <p>
                    <input type=\"button\" id=\"wizDoneButton\" value=\"Done\" onclick=\"javascript:closeWizard();\">
                </p>
            </form>
        </div>
    "; 

}

echo $response;
	
?>
