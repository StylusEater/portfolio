<?php
    $guest = $_GET['guest'];
?>

<div id="wizbg">
<div id="wizFrmHolder">
<fieldset id="wizField">
<legend id="wizLegend">Delete Guest</legend>
<form id="wizard" alt="GWAM: Delete Guest Wizard" >
   <p id="wizErrors" style="display: none;"></p>
   <p>Delete <?php echo $guest ?>?</p> 
   <p><input id="cancel" type="button" value="Cancel" onclick="javascript:closeWizard();"/>
      <input id="submit" type="button" value="Delete" onclick="javascript:deleteGuest('<?php echo $guest ?>');"/></p>
</form>
</fieldset> 
</div>
</div>
