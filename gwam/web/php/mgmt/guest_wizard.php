<div id="wizbg">
<div id="wizFrmHolder">
<fieldset id="wizField">
<legend id="wizLegend">Add Guest</legend>
<form id="wizard" alt="GWAM: Add Guest Wizard" >
   <p id="wizErrors" style="display: none;"></p>
   <p><label id="FirstName">First Name:* </label></p>
   <p><input type="text" id="fname" name="fname" /></p> 
   <p><label id="LastName">Last Name:* </label></p>
   <p><input type="text" id="lname" name="lname" /></p> 
   <p><label id="EMail">E-Mail Address:* </label></p>
   <p><input type="text" id="email" name="email" /></p> 
   <p><label id="Duration">Days Valid:* </label></p>
   <p><input type="text" id="dvalid" name="dvalid" size="3" maxlength="3"/></p> 
   <p><input id="cancel" type="button" value="Cancel" onclick="javascript:closeWizard();"/>
      <input id="submit" type="button" value="Create" onclick="javascript:addGuest();"/></p>
</form>
<p id="wizReqFlds">* - indicates a required field</p>
</fieldset> 
</div>
</div>
