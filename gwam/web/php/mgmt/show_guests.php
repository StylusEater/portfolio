<?php

require_once "../db/connection.php";
require_once "../db/query.php";

// EXAMPLE: Data Structure
//
// $GUESTS = array("0" => array("name" => "lccguest0", "pass" => "lccpassword"),
//                "1" => array("name" => "lccguest1", "pass" => "lccpass2"));

$guestList = new Query();
$guest_list_params = array(":active" => 1);
$getGuestList = $guestList->select("get_guests",$guest_list_params);

$GUESTS = $getGuestList[1];

?>
