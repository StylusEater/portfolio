<?php
if (!class_exists('SiteSettings')) {
    require 'php/conf/settings.php';
} 
$settings = new SiteSettings();

if (session_is_registered('login') && $_SESSION['role'] == '1') {
	Header("Location: http://" . $_SERVER['HTTP_HOST'] . "/php/admin/");
} else if (session_is_registered('login') && $_SESSION['role'] == '2') {
	Header("Location: http://" . $_SERVER['HTTP_HOST'] . "/php/user/");
}
?>
<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01//EN"
            "http://www.w3.org/TR/html4/strict.dtd">
<html>
<head>
   <title><?php echo $settings->STITLE ?></title>
   <link rel="stylesheet" href="css/main.css" type="text/css" />
   <link rel="stylesheet" href="css/login.css" type="text/css" />
   <link rel="stylesheet" href="css/signup.css" type="text/css" />
   <script type="text/javascript" src="js/prototype.js"></script>
   <script type="text/javascript" src="js/login.js"></script>
   <script type="text/javascript" src="js/signup.js"></script>
   <script type="text/javascript" src="js/main.js"></script>
</head>
<body>
<p id="login_title">YOURORGANIZATION GWAM</p>
<div id="login_errors"></div>
