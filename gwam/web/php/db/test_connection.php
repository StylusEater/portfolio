<?php
require "connection.php";

// Create a generic connection using default settings
$foo = new Connection();

// Print out the settings
print "__toString:  $foo";

// Attempt to open a database connection
// Should fail because open is private 
// Make 0 a 1 if you want to run it
while (0) {
    try {
        $foo->open();
        print "open(): Success! \n";
    } catch(Exception $e) {
        // We catch PDO errors in open()
        print $e->getMessage() . "\n";
    }
}

// Attempt to close the connection 
// Should fail because close is private 
// Make 0 a 1 if you want to run it
while (0) {
    try {
        $foo->close();
        print "close(): Success! \n";
    } catch(Exception $e) {
        // We catch PDO errors in the above
        print $e->getMessage() . "\n";
    }
}

// Attempt to get a db handle
try {
    $handle = $foo->getHandle();
    print "getHandle(): Success! \n";
} catch(Exception $e) {
    // We catch PDO errors in the above
    print $e->getMessage() . "\n";
}
 
// Attempt to close a db handle
try {
    $foo->closeHandle();
    print "closeHandle(): Success! \n";
} catch(Exception $e) {
    // We catch PDO errors in the above
    print $e->getMessage() . "\n";
}

?>
