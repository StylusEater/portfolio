<?php

    class QParts {

	function __construct() {

	}

	function __destruct() {
	
	}

	function get($partName,$db="gwam",$tbl) {

		// This is a general function to get parts of a query using their "name"
		$parts = array(
			"signup" => "insert into $db.$tbl (role_id,login,password,created) values (:role_id,:login,password(:password),NULL);",
			"check_dup_signup" => "select id,active from $db.$tbl where login = :login;",
			"login" => "select id,role_id,login,active from $db.$tbl where login = :login and password = password(:password);",
                        "add_guest" => "insert into $db.$tbl (admin,gwamname,fname,lname,email,active,password,days_valid,created) values (:admin,:login,:fname,:lname,:email,'1',:password,:validfor,NULL);",
			"update_add_guest" => "update $db.$tbl set gwamname = :login where id = :id;",
			"get_guests" => "select id,gwamname as name,password as pass,fname,lname from $db.$tbl where active = :active order by id desc;",
			"get_active_guests" => "select count(id) as cnt from $db.$tbl where active = :active;",
			"get_guest_info" => "select fname,lname,email,days_valid as dvalid,password,unix_timestamp(created) as created from $db.$tbl where gwamname = :gwamname;",
			"deactivate_guest" => "update $db.$tbl set active = :active where gwamname = :gwam_login;",
			"update_guest" => "update $db.$tbl set fname = :fname, lname = :lname, email = :email, days_valid = :dvalid where gwamname = :gwamname;",
			"update_account" => "update $db.$tbl set password = password(:password) where login = :login;",
                        "get_settings" => "select * from $db.$tbl;", 
			"update_settings_title" => "update $db.$tbl set option_value = :stitle, modified = NULL where option_key = 'stitle';", 
			"update_settings_prefix" => "update $db.$tbl set option_value = :gprefix, modified = NULL where option_key = 'gprefix';", 
			"update_settings_max" => "update $db.$tbl set option_value = :gmax, modified = NULL where option_key = 'gmax';", 
			"update_settings_length" => "update $db.$tbl set option_value = :plength, modified = NULL where option_key = 'plength';", 
			"update_settings_chars" => "update $db.$tbl set option_value = :pchars, modified = NULL where option_key = 'pchars';", 
			"update_settings_nums" => "update $db.$tbl set option_value = :pnums, modified = NULL where option_key = 'pnums';", 
			"update_settings_uniq" => "update $db.$tbl set option_value = :puniq, modified = NULL where option_key = 'puniq';", 
		);

		return $parts[$partName];
	}

    }

?>
