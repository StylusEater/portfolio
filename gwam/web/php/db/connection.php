<?php

include_once "dbsettings.php";

class Connection {

    private $db_user;
    private $db_pass;
    private $db_schem;
    private $db_host;
    private $db_type;
    private $db_persist;
    private $SETTINGS;
    public $handle;

    function __construct($user="",$pass="",$schem="",$host="",$type="",$persist="") {

	$this->SETTINGS = new DBSettings($user,$pass,$schem,$host,$type,$persist);

        $this->db_user = $this->SETTINGS->USER;
	$this->db_pass = $this->SETTINGS->PASS;
        $this->db_schem = $this->SETTINGS->DB;
	$this->db_host = $this->SETTINGS->HOST;
	$this->db_type = $this->SETTINGS->TYPE;
	$this->db_persist = $this->SETTINGS->PERSIST;
    }

    function __destruct() {
        $this->db_user = NULL;
        $this->db_pass = NULL;
        $this->db_schem = NULL;
        $this->db_host = NULL;
        $this->db_type = NULL;
        $this->db_persist = NULL;
        $this->SETTINGS = NULL;
        $this->handle = NULL;
    }

    function __toString() {
        $connectString = strtoupper($this->db_type) . ":host=" . $this->db_host . ";dbname=" . $this->db_schem . "," . $this->db_user . "," . $this->db_pass . "\n";
        return $connectString;
    }

    private function getConnectString() {
        return $this->db_type . ":host=" . $this->db_host . ";dbname=" . $this->db_schem;
    }

    private function open() {
        try {
            $this->handle = new PDO($this->getConnectString(),$this->db_user,$this->db_pass,
                                    array(PDO::ATTR_PERSISTENT => $this->db_persist));
	} catch(PDOException $pdoError) {
	    print "ERROR: DB::CONNECTION::open() <br>";
            print $pdoError->getMessage() . "<br>";
            die();
	}  	
    }

    private function close() {
        try {
            $this->handle = null;
        } catch(PDOException $pdoCloseError) {
            print $pdoCloseError->getMessage() . "\n"; 
            die();
        }
    }

    public function getHandle() {
        try {
            $this->open();
            return $this->handle;
        } catch(Exception $getHandleError) {
	    print "ERROR: DB::CONNECTION::getHandle() \n";
            print $getHandleError->getMessage() . "\n";
            die();
        }
    }

    public function closeHandle() {
        try {
            $this->close();
        } catch(Exception $closeHandleError) {
            print $closeHandleError->getMessage() . "\n";
            die();
        }
    }

}

?>
