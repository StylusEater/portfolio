<?php

require "query_parts.php";

    class Query {

        function __construct() {

        }
 
        function __destruct() {

        }

	function insert($action,$params) {
		$status = true;
		$msg = "Query ran successfully.";
		// get the query from the query_parts.php file
		// build the query with the passed parameters
		$qparts = new QParts();
		if ($action == "signup") {
			$query = $qparts->get("signup","gwam","acct");
		}
                if ($action == "add_guest") {
                        $query = $qparts->get("add_guest","gwam","guest");
                }

		// Setup a new connection and bind the params
		$db = new Connection();
		$hndl = $db->getHandle();
		$sth = $hndl->prepare($query);
                $lastID = $hndl->lastInsertId();
		try {
			$hndl->beginTransaction();
			$sth->execute($params);
                	$lastID = $hndl->lastInsertId();
			$hndl->commit();
		} catch(PDOException $insertError) {
			$hndl->rollBack();
			$msg = $insertError->getMessage();
			$status = false;
		}
		
		return array($status,$msg,$lastID);
	}

	function select($action,$params) {
		$status = true;
		$vals = "";
		$qparts = new QParts();
		if ($action == "check_dup_signup") {
			$query = $qparts->get("check_dup_signup","gwam","acct");
		}
		if ($action == "login") {
			$query = $qparts->get("login","gwam","acct");
		}
		if ($action == "get_guests") {
			$query = $qparts->get("get_guests","gwam","guest");
		}
		if ($action == "get_active_guests") {
			$query = $qparts->get("get_active_guests","gwam","guest");
		}
		if ($action == "get_guest_info") {
			$query = $qparts->get("get_guest_info","gwam","guest");
		}
		if ($action == "get_settings") {
			$query = $qparts->get("get_settings","gwam","settings");
		}

		// Setup a new connection and bind the params
		$db = new Connection();
		$hndl = $db->getHandle();
		$sth = $hndl->prepare($query);
		try {
			$sth->execute($params);
			$vals = $sth->fetchAll();
		} catch(PDOException $selectError) {
			throw $selectError->getMessage();
		        $status = false;
		}
		return array($status,$vals);	
	}

	function update($action,$params) {
		$status = true;
		$vals = "";
		$qparts = new QParts();
		if ($action == "update_add_guest") {
			$query = $qparts->get("update_add_guest","gwam","guest");
		}
		if ($action == "deactivate_guest") {
                	$query = $qparts->get("deactivate_guest","gwam","guest");
                }
		if ($action == "update_guest") {
			$query = $qparts->get("update_guest","gwam","guest");
		}
		if ($action == "update_account") {
			$query = $qparts->get("update_account","gwam","acct");
		}
		if ($action == "update_settings_title") {
			$query = $qparts->get("update_settings_title","gwam","settings");
		}
		if ($action == "update_settings_prefix") {
			$query = $qparts->get("update_settings_prefix","gwam","settings");
		}
		if ($action == "update_settings_max") {
			$query = $qparts->get("update_settings_max","gwam","settings");
		}
		if ($action == "update_settings_length") {
			$query = $qparts->get("update_settings_length","gwam","settings");
		}
		if ($action == "update_settings_chars") {
			$query = $qparts->get("update_settings_chars","gwam","settings");
		}
		if ($action == "update_settings_nums") {
			$query = $qparts->get("update_settings_nums","gwam","settings");
		}
		if ($action == "update_settings_uniq") {
			$query = $qparts->get("update_settings_uniq","gwam","settings");
		}

		$db = new Connection();
		$hndl = $db->getHandle();
		$sth = $hndl->prepare($query);
		try {
			$hndl->beginTransaction();
			$sth->execute($params);
		        $hndl->commit();
		} catch(PDOException $updateError) {
			$hndl->rollBack();
			$msg = $updateError->getMessage();
			$status = false;
		}
		
		return array($status,$msg);
	}	

    }

?>
