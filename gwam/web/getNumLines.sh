#!/bin/bash
find `pwd` -type f -exec wc -l {} \; | awk '{ SUM +=$1 } END { print SUM-2 }' ## remove 2 lines for this script
