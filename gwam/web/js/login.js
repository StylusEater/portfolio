function raiseLoginError(content) {
    var error = content;
    $('login_errors').update(error);
    var status = $('login_errors').style.display;
    if ( status == "none" ) {
       $('login_errors').toggle();
    } else {
       $('login_errors').style.display = "block";
    }
}

function isLoggedIn() {

}

function showLoginHints() {
    var login_form = $('login');

    // Initialize and focus user area
    var user = login_form['user'];
    $(user).clear();
    $(user).value = "youremail@YOURORGANIZATION.com";
    $(user).style.color = "grey";
    $(user).focus();

    // Initialize password area
    var pass = login_form['pass'];
    $(pass).clear();
    $(pass).value = "password";
    $(pass).style.color = "grey";
}

function clear(field) {
    var fld = field;
    var login_form = $('login');
    
    // Clear the form field when clicked
    var fld_to_clr = login_form[fld];
    $(fld_to_clr).clear;
}
    

function signup() {
    new Ajax.Request('/php/account/signup.php',
        {
            method: 'get',
            onSuccess: function(transport) {
		var pageContents = transport.responseText;
		$('form_holder').update(pageContents);
                // called from signup.js
                showSignupHints();
            },
            onFailure: function(transport) {
                var pageError = transport.responseText;
		raiseLoginError(pageError);
            }
	})
}

// Abandon for now - AMD - 03/02/10
function login() {
    var login_form = $('login');	
    var email = login_form['user'].getValue();
    var secret = login_form['pass'].getValue();

    new Ajax.Request('/php/account/submit_login.php',
	{
	    method: 'get',
	    onSuccess: function(transport) {
		var pageContents = transport.responseText;
	    }
	})	
}

function resetPassword() {
    alert("Not yet implemented.");
}

function tryagain() {
    window.location = "http://gwam.YOURORGANIZATION.com/";    
}
