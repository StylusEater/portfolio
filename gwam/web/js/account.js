function updateAccount() {
	var accountForm = $('user_mgmt_form');
	var login = accountForm['login'].value;
	var pass = accountForm['password'].value;
	var cpass = accountForm['confpassword'].value;

	if (pass == cpass) {

		new Ajax.Request('/php/mgmt/updateAccount.php',
		{
			method: 'post',
			parameters: "&login=" + login + "&pass=" + pass,
			onSuccess: function(transport) {
				var acctUpdateResp = transport.responseText;
				$('user_mgmt_legend').update("Account Update Status");
				$('user_mgmt_form').update(acctUpdateResp);
			},
			onFailure: function(transport) {
				var acctUpdateResp = transport.responseText;
				$('user_mgmt_legend').update("Account Update Failed");
				$('user_mgmt_form').update(acctUpdateResp);
			}
		})

        } else {

		$('user_mgmt_errors').style.color = 'red';
		$('user_mgmt_errors').style.font.weight = 'bold';
		$('user_mgmt_errors').update("Passwords do not match.");
            
        }

}

function cancelEditAccount() {
	window.location = "http://" + window.location.hostname + "/php/user/account/";
}
