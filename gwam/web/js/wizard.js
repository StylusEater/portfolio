function openWizard() {
    new Ajax.Request('/php/mgmt/guest_wizard.php',
    {
	method: 'get',
	onSuccess: function(transport) {
		var wizardForm = transport.responseText;
		$('wizardHolder').update(wizardForm);
    		$('wizardHolder').style.display = "";
	}
    })
}

function closeWizard() {
    $('wizardHolder').style.display = "none";
    window.location = "http://" + window.location.hostname;    
}

function confirmUserDelete(guest) {
    var gwamguest = guest;
    new Ajax.Request('/php/mgmt/guest_delete_confirm.php',
    {
        method: 'get',
	parameters: "guest=" + gwamguest, 
        onSuccess: function(transport) {
		var deleteGuestForm = transport.responseText;
		$('wizardHolder').update(deleteGuestForm);
		$('wizardHolder').style.display = "";
	},
	onFailure: function(transport) {
		var errorPage = transport.responseText;
		$('wizardHolder').update(errorPage);
		$('wizardHolder').style.display = "";
	}
    })
}

function editGuest(guest,id) {
    var gwamguest = guest;
    var gwamrowid = 'edit' + id;
    new Ajax.Request('/php/mgmt/show_edit_guest.php',
    {
        method: 'get',
        parameters: "guest=" + gwamguest + "&id=" + gwamrowid,
        asynchronous: false,
        onSuccess: function(transport) {
        	var successfulDeleteGuestForm = transport.responseText;
		$(gwamrowid).update(successfulDeleteGuestForm);
		$(gwamrowid).style.display = "";
		gwamguest = "";
		gwamrowid = "";
	},
	onFailure: function(transport) {
		var errorPage = transport.responseText;
		$(gwamrowid).update(errorPage);
		$(gwamrowid).style.display = "";
		gwamguest = "";
		gwamrowid = "";
	}
    })
}

function submitEditGuest(guest,id) {
    var gwamguest = guest;
    var guestEditId = id;
    var guestf = "guest" + guestEditId;
    var guestForm = $(guestf);
    var fname = guestForm['fname'].value;
    var lname = guestForm['lname'].value;
    var email = guestForm['email'].value;
    var dvalid = guestForm['dvalid'].value;

    var invalid = false;
    var field = false;
    var msg = false;

    var minFnameLen = 2;
    var minLnameLen = 2;

    if (fname == "") {
        msg = "First Name cannot be blank.";
        field = "fname";
        invalid = true;
    } else if (fname.length < minFnameLen) {
        msg = "First Name must be " + minFnameLen + " characters or more.";
        field = "fname";
        invalid = true;
    } else if ((lname == "") && !(invalid)) {
        msg = "Last Name cannot be blank.";
        field = "lname";    
        invalid = true;
    } else if (lname.length < minLnameLen) {
        msg = "Last Name must be " + minLnameLen + " characters or more.";
        field = "lname";
        invalid = true;
    } else if ((email == "") && !(invalid)) {
        msg = "E-Mail Address cannot be blank.";
        field = "email";
        invalid = true;
    } else if (!validEmail(email)) {
        msg = "E-Mail Address is not formatted properly.";
        field = "email";
        invalid = true;
    } else if (!(invalid)) {
        if (dvalid == "") {
            msg = "Days Valid cannot be blank.";
            field = "dvalid";
            invalid = true;
        }

        if (!invalid) {
            var valChars = "0123456789";
            var dvalLen = dvalid.length;
            var valNum = true;
            var chr;

            for (i = 0; i < dvalLen && valNum == true; i++) {
                chr = dvalid.charAt(i);
                if (valChars.indexOf(chr) == -1) {
                    valNum = false;
                    invalid = true;
                    field = "dvalid";
                    msg = "Days Valid can only contain numbers.";
                }
            } 
        }
    }

    if (invalid) {
        $('guestErrors').update(msg);
        $('guestErrors').style.color = "red";
        $('guestErrors').style.display = "";
        $('fname').style.border = "1px solid black";
        $('lname').style.border = "1px solid black";
        $('email').style.border = "1px solid black";
        $('dvalid').style.border = "1px solid black";
        $(field).style.border = "2px solid red";
    } else {

        new Ajax.Request('/php/mgmt/edit_guest.php',
        {
                method: 'post',
                parameters: "fname=" + fname + "&lname=" + lname + "&email=" + email + "&dvalid=" + dvalid + "&gwamname=" + gwamguest + "&editrow=" + guestEditId,
                onSuccess: function(transport) {
                        var editGuestResp = transport.responseText;
                        $(guestf).style.display = "";
                        $(guestEditId).update(editGuestResp);
                },
                onFailure: function(transport) {
                        $(guestEditId).update(transport.responseText);
                }
        })
    }   
}

function cancelEditGuest(id) {
    var gwamrowid = id;
    $(gwamrowid).style.display = "none";
}

function doneEditGuest(id) {
    var gwamrowid = id;
    var win = window.location.pathname.toString();
    var niw = win.split("/",3);
    var role = niw[2];
    $(gwamrowid).style.display = "none";
    window.location = "http://" + window.location.hostname + "/php/" + role;
}

function deleteGuest(guest) {
    var gwamguest = guest;
    new Ajax.Request('/php/mgmt/guest_delete.php',
    {
        method: 'post',
        parameters: "guest=" + gwamguest,
        onSuccess: function(transport) {
        	var deleteGuestForm = transport.responseText;
		$('wizardHolder').update(deleteGuestForm);
	},
	onFailure: function(transport) {
		var errorPage = transport.responseText;
		$('wizardHolder').update(errorPage);
		$('wizardHolder').style.display = "";
	}
    })
}

function validEmail(address) {
    var email = address;
    var valid = false;
    var lenvalid = false;
    var atvalid = false;
    var addyvalid = false;
    var domvalid = false;

    // assume basic e-mail address of a@a.us
    if (email.length > 6) {
        lenvalid = true;
    }

    // make sure we have an @ sign present
    var char;
    for (var i = 0; i < email.length; i++) {
        if (email.charAt[i] = "@") {
            atvalid = true;
        }
    }

    // does it "look" like an e-mail address?
    // we don't do exhaustive regex matching like we should
    var emailPartsToAt = email.split("@",2);
    var domainParts;
    if (emailPartsToAt.length < 2) {
         // DEBUG
         //alert("Parts: " + emailPartsToAt);
         addyvalid = false;
    } else { 
	 addyvalid = true;
         // DEBUG
         //alert("Parts: " + emailPartsToAt);
         domainParts = emailPartsToAt[1].split(".",2);
         if (domainParts.length < 2) {
             domvalid = false;
         } else {
             validDomains = new Array("ac","ad","ae","aero","af","ag","ai","al","am","an","ao","aq","ar","arpa","as","asia","at","au","aw","ax",
                                  "az","ba","bb","bd","be","bf","bg","bh","bi","biz","bj","bm","bn","bo","br","bs","bt","bv","bw","by","bz",
                                  "ca","cat","cc","cd","cf","cg","ch","ci","ck","cl","cm","cn","co","com","coop","cr","cu","cv","cx","cy","cz",
                                  "de","dj","dk","dm","do","dz","ec","edu","ee","eg","er","es","et","eu","fi","fj","fk","fm","fo","fr","ga","gb",
                                  "gd","ge","gf","gg","gh","gi","gl","gm","gn","gov","gp","gq","gr","gs","gt","gu","gw","gy","hk","hm","hn","hr",
                                  "ht","hu","id","ie","il","im","in","info","int","io","iq","ir","is","it","je","jm","jo","jobs","jp","ke","kg",
                                  "kh","ki","km","kn","kp","kr","kw","ky","kz","la","lb","lc","li","lk","lr","ls","lt","lu","lv","ly","ma","mc",
                                  "md","me","mg","mh","mil","mk","ml","mm","mn","mo","mobi","mp","mq","mr","ms","mt","mu","museum","mv","mw","mx",
                                  "my","mz","na","name","nc","ne","net","nf","ng","ni","nl","no","np","nr","nu","np","nr","nu","nz","om","org","pa",
                                  "pe","pf","pg","ph","pk","pl","pm","pn","pr","pro","ps","pt","pw","py","qa","re","ro","rs","ru","rw","sa","sb","sc",
                                  "sd","se","sg","sh","si","sj","sk","sl","sm","sn","so","sr","st","su","sv","sy","sz","tc","td","tel","tf","tg","th",
                                  "tj","tk","tl","tm","tn","to","tp","tr","travel","tt","tv","tw","tz","ua","ug","uk","us","uy","uz","va","vc","ve",
                                  "vg","vi","vn","vu","wf","ws","xn","ye","yt","yu","za","zm","zw");
            var numValDoms = validDomains.length;
            for (var j = 0; j < numValDoms; j++) {
                if (domainParts[1] == validDomains[j]) {
                    // DEBUG
                    // alert("MATCH --> " + validDomains[j]);
                    domvalid = true;
                }
            }
        }
    }

    if (lenvalid && atvalid && addyvalid && domvalid) {
        valid = true;
    }
    // DEBUG
    //alert("LENGTH: " + lenvalid + " AT: " + atvalid + " ADDRESS: " + addyvalid + " DOMAIN: " + domvalid);

    return valid; 

}

function addGuest() {
    var fname = $('wizard')['fname'].value;
    var lname = $('wizard')['lname'].value;
    var email = $('wizard')['email'].value;
    var dvalid = $('wizard')['dvalid'].value;

    var invalid = false;
    var field = false;
    var msg = false;

    var minFnameLen = 2;
    var minLnameLen = 2;

    if (fname == "") {
	msg = "First Name cannot be blank.";
        field = "fname";
        invalid = true;
    } else if (fname.length < minFnameLen) {
        msg = "First Name must be " + minFnameLen + " characters or more.";
	field = "fname";
	invalid = true;
    } else if ((lname == "") && !(invalid)) {
        msg = "Last Name cannot be blank.";
        field = "lname";    
        invalid = true;
    } else if (lname.length < minLnameLen) {
        msg = "Last Name must be " + minLnameLen + " characters or more.";
        field = "lname";
        invalid = true;
    } else if ((email == "") && !(invalid)) {
        msg = "E-Mail Address cannot be blank.";
        field = "email";
        invalid = true;
    } else if (!validEmail(email)) {
        msg = "E-Mail Address is not formatted properly.";
	field = "email";
	invalid = true;
    } else if (!(invalid)) {
        if (dvalid == "") {
            msg = "Days Valid cannot be blank.";
            field = "dvalid";
            invalid = true;
        }

        if (!invalid) {
	    var valChars = "0123456789";
            var dvalLen = dvalid.length;
            var valNum = true;
            var chr;

            for (i = 0; i < dvalLen && valNum == true; i++) {
	        chr = dvalid.charAt(i);
                if (valChars.indexOf(chr) == -1) {
                    valNum = false;
                    invalid = true;
                    field = "dvalid";
                    msg = "Days Valid can only contain numbers.";
                }
            } 
	}
    }

    if (invalid) {
        $('wizErrors').update(msg);
        $('wizErrors').style.color = "red";
        $('wizErrors').style.display = "";
        $('fname').style.border = "1px solid black";
        $('lname').style.border = "1px solid black";
        $('email').style.border = "1px solid black";
        $('dvalid').style.border = "1px solid black";
        $(field).style.border = "2px solid red";
    } else {
        $('wizard').style.display = "none";
        $('wizReqFlds').style.display = "none"; 
        $('wizLegend').update("Guest Info");

	new Ajax.Request('/php/mgmt/add_guest.php',
	{
                method: 'post',
                parameters: "fname=" + fname + "&lname=" + lname + "&email=" + email + "&dvalid=" + dvalid,
		onSuccess: function(transport) {
			var addGuestResp = transport.responseText;
			$('wizField').style.color = "black";
			$('wizReqFlds').update(addGuestResp);
			$('wizReqFlds').style.display = "";
		},
		onFailure: function(transport) {
			$('wizField').update(transport.responseText);
		}
	})
    }	
} 
