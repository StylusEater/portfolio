function cancel_signup() {
    new Ajax.Request('/php/account/login.php',
    {
        method: 'get',
        onSuccess: function(transport) {
            var pageContents = transport.responseText;
            $('form_holder').update(pageContents);
            // called from login.js
            showLoginHints();
        },
        onFailure: function(transport) {
            var pageError = transport.responseText;
            raiseLoginError(pageError);
        }
    })
}

function showSignupHints() {
    var signup_form = $('signup');

    // Initialize and focus user area
    var email = signup_form['email'];
    $(email).clear();
    $(email).value = "youremail@YOURORGANIZATION.com";
    $(email).style.color = "grey";
    $(email).focus();

    // Initialize password area
    var sec = signup_form['secret'];
    $(sec).clear();
    $(sec).value = "password";
    $(sec).style.color = "grey";
}

var successPage;
function buildSignupSuccessMsg() {
    new Ajax.Request('/php/account/signup_success.php',
    {
	method: 'get',
	asynchronous: false,
	onSuccess: function(transport) {
		successPage = transport.responseText;
	},
	onComplete: function() {
		return;
	}
    })
}

var failurePage;
function buildSignupFailureMsg() {
    new Ajax.Request('/php/account/signup_failure.php',
    {
	method: 'get',
	asynchronous: false,
	onSuccess: function(transport) {
		failurePage = transport.responseText;
	}
    })
}

function submit_signup(login,pass,role) {
    var email = login;
    var secret = pass;
    var perms = role;

    new Ajax.Request('/php/account/submit_signup.php',
    {
	method: 'post',
	parameters: "email=" + email + "&secret=" + secret + "&role=" + perms,
	onSuccess: function(transport) {
		buildSignupSuccessMsg();
		var signupSuccessOutput = transport.responseText;
		$('signup_form_holder').hide;
		$('form_holder').update(successPage);
		$('successmsg').update(signupSuccessOutput);
		$('successmsg').show;
		$('login_links').show;
	},
	onFailure: function(transport) {
		buildSignupFailureMsg();
		var signupFailureOutput = transport.responseText;
		$('signup_form_holder').hide;
		$('form_holder').update(failurePage); 
		$('failuremsg').update(signupFailureOutput);
	}
    })
}
