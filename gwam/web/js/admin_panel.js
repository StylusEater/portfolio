function editSettings() {
    var settingsForm = $('site_settings_form');
    var stitle = settingsForm['stitle'].value;
    var gprefix = settingsForm['gprefix'].value;
    var gmax = settingsForm['gmax'].value;
    var plength = settingsForm['plength'].value;
    var pnums = settingsForm['pnums'].value;
    var pchars = settingsForm['pchars'].value;
    var puniq = settingsForm['puniq'].value;

    var query = "";
    var error_block = "";
    var msg = "";
    var field = "";
    var noerror = true;
    if ((stitle.length == 0) && noerror) {
        error_block = 'site_errors';
        msg = "Site Title cannot be blank.";
        field = settingsForm['stitle'];
        noerror = false;
    } else if ((gprefix.length == 0) && noerror) {
        error_block = 'guest_errors';
        msg = "Guest Prefix cannot be blank.";
        field = settingsForm['gprefix'];
        noerror = false;
    } else if ((gmax.length == 0) && noerror) {
        error_block = 'guest_errors';
        msg = "Guest Limit cannot be blank.";
        field = settingsForm['gmax'];
        noerror = false;
    } else if (isNotNum(gmax) && noerror) {
        error_block = 'guest_errors';
        msg = "Guest Limit must be a number.";
        field = settingsForm['gmax'];
        noerror = false;
    } else if ((plength.length == 0) && noerror) {
        error_block = 'pass_errors';
        msg = "Required Length cannot be blank.";
        field = settingsForm['plength'];
        noerror = false;
    } else if (isNotNum(plength) && noerror) {
        error_block = 'pass_errors';
        msg = "Required Length must be a number.";
        field = settingsForm['plength'];
        noerror = false;
    } else if ((pnums.length == 0) && noerror) {
        error_block = 'pass_errors';
        msg = "Required Numbers cannot be blank.";
        field = settingsForm['pnums'];
        noerror = false;
    } else if (isNotNum(pnums)) {
        error_block = 'pass_errors';
        msg = "Required Numbers must be a number.";
        field = settingsForm['pnums'];
        noerror = false;
    } else if ((pchars.length == 0) && noerror) {
        error_block = 'pass_errors';
        msg = "Required Special Characters cannot be blank.";
        field = settingsForm['pchars'];
        noerror = false;
    } else if (isNotNum(pchars) && noerror) {
        error_block = 'pass_errors';
        msg = "Required Special Characters must be a number.";
        field = settingsForm['pchars'];
        noerror = false;
    } else if ((puniq.length == 0) && noerror) {
	error_block = 'pass_errors';
	msg = "Required Unique Characters cannot be blank.";
        field = settingsForm['puniq'];
        noerror = false;
    } else if (isNotNum(puniq) && noerror) {
        error_block = 'pass_errors';
        msg = "Required Unique Characters must be a number.";
        field = settingsForm['puniq'];
        noerror = false;
    }

    clearErrors(settingsForm);
    if (error_block.length > 0) {
        showError(error_block,msg,field);
    } else {
	new Ajax.Request('/php/conf/updateSettings.php',
	{
		method: 'post',
		parameters: 'stitle=' + stitle + '&gprefix=' + gprefix + '&gmax=' + gmax + '&plength=' + plength + '&pchars=' + pchars + '&pnums=' + pnums + '&puniq=' + puniq,
		onSuccess: function(transport) {
			var settingsUpdateResponse = transport.responseText;
			$('site_settings_form').update(settingsUpdateResponse);
			$('site_settings_legend').update("Site Settings Updated");
		},
		onFailure: function(transport) {
			var settingsUpdateResponse = transport.responseText;
			$('site_settings_form').update(settingsUpdateResponse);
			$('site_settings_legend').update("Site Settings Update Error");
	        },
	});			
    }
}

function showError(eblock,emsg,efld) {
    $(eblock).update(emsg);
    $(eblock).style.color = 'red';
    $(eblock).style.display = "";
    $(efld).style.border = "2px solid red";
}

function clearErrors(form) {
    form['stitle'].style.border = "none";
    form['gprefix'].style.border = "none";
    form['gmax'].style.border = "none";
    form['plength'].style.border = "none";
    form['pnums'].style.border = "none";
    form['pchars'].style.border = "none";
    $('site_errors').style.display = "none";
    $('guest_errors').style.display = "none";
    $('pass_errors').style.display = "none";
}

function isNotNum(fld) {
    var valChars = "0123456789";
    var dvalLen = fld.length;
    var notNum = false;
    var chr;

    for (i = 0; i < dvalLen && notNum == false; i++) {
        chr = fld.charAt(i);
        if (valChars.indexOf(chr) == -1) {
            notNum = true;
        }
    } 

    return notNum;
}
