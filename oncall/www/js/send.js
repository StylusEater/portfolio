function show_send_reminders()
{
    var process = $('#process');
    var send_reminder_form = '/htm/reminder/send.htm';
    
    $.get(send_reminder_form, 
        function(data)
        {
            process.html(data);
        }
    );
}

function validate_send_reminders()
{
    var errors_present = 0;   
    $('#error').html("");

    if (($('#sms:checked').length + $('#email:checked').length) == 0 )
    {
        $('#error').css('color','red');
        $('#error').css('font-weight','bold');
        $('#error').append("<p>You must select SMS or E-Mail or both!</p>");
        $('#sms').css('border','2px solid red');
        $('#email').css('border','2px solid red');
        errors_present = 1;
    } else {
        $('#error').css('color','#0d5686');
        $('#sms').css('border','1px solid black');
        $('#email').css('border','1px solid black');
    }

}

