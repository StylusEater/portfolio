function show_add_person()
{
    var process = $('#process');
    var add_person_form = '/htm/person/add.htm';
    
    $.get(add_person_form, 
        function(data)
        {
            process.html(data);
        }
    );
}

function validate_add_person()
{
    var errors_present = 0;   
    $('#error').html("");

    if ($('#fname').val().length == 0)
    {
        $('#fname').css('border','2px solid red');
        errors_present = 1;
    } else {
        $('#fname').css('border','1px solid black');
    }

    if ($('#mname').val().length == 0)
    {
        $('#mname').css('border','2px solid red');
        errors_present = 1;
    } else {
        $('#mname').css('border','1px solid black');
    }

    if ($('#lname').val().length == 0)
    {
        $('#lname').css('border','2px solid red');
        errors_present = 1;
    } else {
        $('#lname').css('border','1px solid black');
    }

    if (($('#send_sms:checked').length + $('#send_email:checked').length) == 0 )
    {
        $('#error').append("<p>You must select SMS or E-Mail or both!</p>");
        $('#phone').css('border','2px solid red');
        $('#email').css('border','2px solid red');
        errors_present = 1;
    } else {
        $('#phone').css('border','1px solid black');
        $('#email').css('border','1px solid black');
    }

    var phone_regex = /(([1])?([-])?([(])?([0-9]){3}([)])?([-])?([0-9]){3}([-])?([0-9]){4})$/;
    var phone = $('#phone').val();
    if ((phone.length > 0) & (phone_regex.test(phone) == 0))
    {
        $('#error').append("<p>Please check the phone number.</p>");
        $('#phone').css('border','2px solid red');
        errors_present = 1;
    }

    var email_regex = /(([a-zA-Z0-9])+([@]){1}([a-zA-Z0-9])+(.com|.org|.edu|.net|.us|.mobi|.me|.gov){1})$/;
    var email = $('#email').val();
    if ((email.length > 0) & (email_regex.test(email) == 0))
    {
        $('#error').append("<p>Please check the e-mail address.</p>");
        $('#email').css('border','2px solid red');
        errors_present = 1;
    }

    if (($('#phone').val().length == 0) & $('#send_sms:checked').length)
    {
        $('#phone').css('border','2px solid red');
        errors_present = 1;
    }

    if (($('#email').val().length == 0) & $('#send_email:checked').length)
    {
        $('#email').css('border','2px solid red');
        errors_present = 1;
    }

    if (errors_present == 1)
    {
        $('#error').append("<p>Please correct the red fields and try again!</p>");
        $('#error').show();
        return false;
    }

    if (errors_present == 0)
    {
        // Submit Data
        fname = $('#fname').val();
        mname = $('#mname').val();
        lname = $('#lname').val();
        phone = $('#phone').val();
        email = $('#email').val();
        send_sms = $('#send_sms:checked').length;
        send_email = $('#send_email:checked').length; 

        // Check if person exists?
        var check_person_script = '/php/person/exists.php';
        $.get(check_person_script,
            {
                'phone':phone,
                'email':email
            },
            function(data) 
            {
                if (data == 0)
                {
                    // Add New Person      
                    var add_person_script = '/php/person/add.php';
                    $.post(add_person_script,
                        {
                            'fname':fname,
                            'mname':mname,
                            'lname':lname,
                            'phone':phone,
                            'email':email,
                            'send_sms':send_sms,
                            'send_email':send_email
                        },
                        function(data)
                        {
                            if (data == 0)
                            {
                                $('#error').append("<p>Added successfully!</p>");
                                $('#error').css('color','green');
                                $('#error').show();
                            } else {
                                $('#error').append(data);
                                $('#error').show();
                            }
                        }
                    );
         
                } else {
                    $('#error').css('color','red');
                    $('#error').append("<p>Person already exists!</p>");
                    $('#error').show();
                }
            }
        );
   
    }
    
}
