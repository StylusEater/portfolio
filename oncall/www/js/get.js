function show_get_calendar()
{
    var process = $('#process');
    var get_calendar_list = '/php/calendar/list.php';
    
    $.get(get_calendar_list, 
        function(data)
        {
            process.html(data);
        }
    );
}
