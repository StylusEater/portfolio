function show_edit_person()
{
    var process = $('#process');
    var edit_person_list = '/php/person/list.php';
    
    $.get(edit_person_list, 
        function(data)
        {
            process.html(data);
        }
    );
}

function edit_person(id)
{
    person_id = $('#id_' + id).text();
    fname = $('#fname_' + id).text();
    mname = $('#mname_' + id).text();
    lname = $('#lname_' + id).text();
    phone = $('#pnumber_' + id).text();
    email = $('#email_' + id).text();
    send_email = $('#send_email_' + id).text();
    send_sms = $('#send_sms_' + id).text();

    var people = $('#people');
    var edit_person_form = '/htm/person/edit.htm';
    
    $.get(edit_person_form, 
        function(data)
        {
            people.html(data);
            $('#id').val(person_id);
            $('#fname').val(fname);
            $('#mname').val(mname);
            $('#lname').val(lname);
            $('#phone').val(phone);
            $('#email').val(email);
            if (send_email == "TRUE")
            {
                $('#send_email').click();
            }
            if (send_sms == "TRUE")
            {
                $('#send_sms').click();
            }
        }
    );
}

function validate_edit_person()
{
    var errors_present = 0;   
    $('#error').html("");

    if ($('#fname').val().length == 0)
    {
        $('#fname').css('border','2px solid red');
        errors_present = 1;
    } else {
        $('#fname').css('border','1px solid black');
    }

    if ($('#mname').val().length == 0)
    {
        $('#mname').css('border','2px solid red');
        errors_present = 1;
    } else {
        $('#mname').css('border','1px solid black');
    }

    if ($('#lname').val().length == 0)
    {
        $('#lname').css('border','2px solid red');
        errors_present = 1;
    } else {
        $('#lname').css('border','1px solid black');
    }

    if (($('#send_sms:checked').length + $('#send_email:checked').length) == 0 )
    {
        $('#error').append("<p>You must select SMS or E-Mail or both!</p>");
        $('#phone').css('border','2px solid red');
        $('#email').css('border','2px solid red');
        errors_present = 1;
    } else {
        $('#phone').css('border','1px solid black');
        $('#email').css('border','1px solid black');
    }

    var phone_regex = /(([1])?([-])?([(])?([0-9]){3}([)])?([-])?([0-9]){3}([-])?([0-9]){4})$/;
    var phone = $('#phone').val();
    if ((phone.length > 0) & (phone_regex.test(phone) == 0))
    {
        $('#error').append("<p>Please check the phone number.</p>");
        $('#phone').css('border','2px solid red');
        errors_present = 1;
    }

    var email_regex = /(([a-zA-Z0-9])+([@]){1}([a-zA-Z0-9])+(.com|.org|.edu|.net|.us|.mobi|.me|.gov){1})$/;
    var email = $('#email').val();
    if ((email.length > 0) & (email_regex.test(email) == 0))
    {
        $('#error').append("<p>Please check the e-mail address.</p>");
        $('#email').css('border','2px solid red');
        errors_present = 1;
    }

    if (($('#phone').val().length == 0) & $('#send_sms:checked').length)
    {
        $('#phone').css('border','2px solid red');
        errors_present = 1;
    }

    if (($('#email').val().length == 0) & $('#send_email:checked').length)
    {
        $('#email').css('border','2px solid red');
        errors_present = 1;
    }

    if (errors_present == 1)
    {
        $('#error').append("<p>Please correct the red fields and try again!</p>");
        $('#error').show();
        return false;
    }

    if (errors_present == 0)
    {
        // Submit Data
        id = $('#id').val();
        fname = $('#fname').val();
        mname = $('#mname').val();
        lname = $('#lname').val();
        phone = $('#phone').val();
        email = $('#email').val();
        send_sms = $('#send_sms:checked').length;
        send_email = $('#send_email:checked').length; 

        // Add New Person      
        var edit_person_script = '/php/person/edit.php';
        $.post(edit_person_script,
            {
                'id':id,
                'fname':fname,
                'mname':mname,
                'lname':lname,
                'phone':phone,
                'email':email,
                'send_sms':send_sms,
                'send_email':send_email
            },
            function(data)
            {
                if (data == 0)
                {
                    $('#error').append("<p>Update successful!</p>");
                    $('#error').css('color','green');
                    $('#error').show();
                } else {
                    $('#error').append(data);
                    $('#error').show();
                }
            }
        );
    }
    
}
