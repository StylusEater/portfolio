<?php

session_start();

$user = trim($_POST['username']);
$pass = trim($_POST['password']);

if (strcmp($user,"admin") == 0)
{
    if (strcmp($pass,"admin") == 0)
    {
        unset($_SESSION['LOGIN_FAILURE_MESSAGE']);
        $_SESSION['ONCALL_LOGGED_IN'] = TRUE;
    } else {
        $_SESSION['LOGIN_FAILURE_MESSAGE'] = "Wrong Password";
    }
} else {
    $_SESSION['LOGIN_FAILURE_MESSAGE'] = "Wrong Username";
}

header("Location: http://" . $_SERVER['SERVER_NAME']);

?>
