<?php

require_once('license.php');
require_once('conf/setup.php');
require_once('site/header.php');

session_start();

?>

<div id="login">

    <?php if (isset($_SESSION['ONCALL_LOGGED_IN'])) 
        {
    ?>

    <fieldset id="oncall_menu">
        <legend>OnCall Menu</legend> 
        <a href="/" id="home" class="menu">Home</a>
        <a href="#" id="add_person" class="menu" onclick="show_add_person();">Add Person</a>
        <a href="#" id="edit_person" class="menu" onclick="show_edit_person();">Edit Person</a>
        <a href="#" id="get_calendar" class="menu" onclick="show_get_calendar();">Get Calendar</a>
        <a href="#" id="send_reminders" class="menu" onclick="show_send_reminders();">Send Reminders</a>
        <a href="/php/logout.php" id="logout" class="menu">Logout</a>

        <div id="process">
            * - If you want to regenerate the calendars you need to run the cronjob because it will destroy all existing calendars.
        </div>
    </fieldset>
    
    <?php
        } else { 
            // TODO - FIX
            $person_oncall_end = "";
            $person_oncall = "";
            $oncall = $person_oncall . "Adam is on call until 11/04/11" . 
                      $person_oncall_end . ".";
            echo("<div id=\"oncall_person\">$oncall</div>");
    ?>

    <fieldset id="oncall_login">
        <legend>Lakeland OnCall</legend> 
        <form id="oncall_login" action="/php/login.php" method="POST">
            <label for="username">USERNAME</label>
            <input id="username" name="username" type="text" />
            <label for="password">PASSWORD</label>
            <input id="password" name="password" type="password" />
            <input type="submit" value="LOGIN" />
        </form>
    <?php
        if (isset($_SESSION['LOGIN_FAILURE_MESSAGE']))
        {
            echo("<div id=\"error\">" . $_SESSION['LOGIN_FAILURE_MESSAGE'] . "</div>");
        }
    ?>
    </fieldset>
    <div id="oncall_info">
        Network Operations <br />
        On Call Phone # 1-(222)-222-2222
    </div>
    <?php }?>

</div>

<?php

require_once('site/footer.php');

?>
