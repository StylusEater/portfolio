<?php

// Get Configuration Options
$settings = parse_ini_file("settings.ini",TRUE);

// Setup Includes 
ini_set('include_path',$settings['php']['include_path']);

// Set Calendar Options
define('CALENDAR_TYPE',$settings['calendar']['type']);
define('DAYS_IN_YEAR',$settings['calendar']['days_in_year']);
define('DAYS_IN_LEAP_YEAR',$settings['calendar']['days_in_leap_year']);
define('STORAGE_PATH',$settings['calendar']['storage_path']);
define('TIMEZONE',$settings['calendar']['timezone']);

// Set Database Settings
$database = $settings['database']['database'];
$host = $settings['database']['host'];
$password = $settings['database']['password'];
$port = $settings['database']['port'];
$type = $settings['database']['type'];
$username = $settings['database']['username'];

// Set OnCall Options
define('DAYS_ONCALL',$settings['oncall']['days_oncall']);
define('ONCALL_ADMIN_EMAIL',$settings['oncall']['admin_email']);
define('ONCALL_ADMIN_PHONE',$settings['oncall']['admin_phone']);
define('ONE_PERSON',$settings['oncall']['one_person']);

?>
