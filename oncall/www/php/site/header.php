<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN"
"http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
<title>OnCall! - OnCall Management Site</title>
<link href="/css/common.css" rel="stylesheet" media="screen" />
<link href="/css/site.css" rel="stylesheet" media="screen" />
<script type="text/javascript" language="javascript" src="/js/jquery/latest.js"></script>
<script type="text/javascript" language="javascript" src="/js/add.js"></script>
<script type="text/javascript" language="javascript" src="/js/edit.js"></script>
<script type="text/javascript" language="javascript" src="/js/generate.js"></script>
<script type="text/javascript" language="javascript" src="/js/get.js"></script>
<script type="text/javascript" language="javascript" src="/js/send.js"></script>
</head>
<body>
