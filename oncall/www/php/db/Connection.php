<?php

class Connection
{

    private $database = "";
    private $host = "";
    private $password = "";
    private $port = "";
    private $type = "";
    private $username = "";

    public function __construct($database,$host,$password,$port,$type,$username)
    {
        $this->database = $database;
        $this->host = $host;
        $this->password = $password;
        $this->port = $port;
        $this->type = $type;
        $this->username = $username; 
    }

    public function get()
    {
        $dsn = $this->type . ":dbname=" . $this->database .
               ";host=" . $this->host . ";port=" . $this->port; 
        return new PDO($dsn, $this->username, $this->password);
    }
}

?>
