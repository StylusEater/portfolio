<?php

class Query
{

    private $queries = "";
    private $table = "";
    

    public function __construct() 
    { 
        $this->queries = parse_ini_file('queries.ini',TRUE);
    }

    public function __destruct() { }

    public function set_table($table_name)
    {
        $this->table = $table_name;
    }
   
    public function get_query_by_name($query_name)
    {
        return $this->queries[$this->table][$query_name];
    } 

    public function prepare_query($connection,$query)
    {
        return $connection->prepare($query);     
    }
}

?>
