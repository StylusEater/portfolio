<?php

require_once('../conf/setup.php');

$no_directory = "";
if (!file_exists(STORAGE_PATH))
{
    $no_directory = TRUE;
}

$existing_calendars = "";

$existing_calendars .= "<div id=\"calendars\">";
$existing_calendars .= "<span class=\"row title_row\">";
$existing_calendars .= "<span class=\"cell cell_small first\">ID</span>" .
                       "<span class=\"cell\">Month</span>" .
                       "<span class=\"cell\">Actions</span>"; 
$existing_calendars .= "</span>";
$iteration = 0;

// TODO - FIX ME WITH YEARS
$calendars = array('0' => array('id' => '1', 'month' => 'ALL' , 'actions' => '<a href="/calendars/oncall_calendar_2011.pdf">Download</a>'));
$number_calendars = count($calendars);

if ($number_calendars)
{
    $record = "";
    foreach ($calendars as $calendar)
    {
      //print_r($calendar);
      $iteration += 1; 
      if ($iteration == $number_calendars)
      {
          $record .= "<span class=\"row last_row\">";
      } else {
          $record .= "<span class=\"row\">";
      }
      $record .= "<span id=\"id_" . $calendar['id'] . "\" class=\"cell cell_small first\">" . $calendar['id'] . "</span>"; 
      $record .= "<span id=\"month_" . $calendar['id'] . "\" class=\"cell\">" . $calendar['month'] . "</span>";
      $record .= "<span id=\"actions_" . $calendar['id'] . "\" class=\"cell cell_small\">" . $calendar['actions'] . "</span>";
      $existing_calendars .= $record . "</span>";
      $record = "";
    }
    $existing_calendars .= "</div>";
    echo $existing_calendars;
} else {

    if ($no_directory)
    {
        echo "<p style=\"color: red;\">Have you generated calendars?</p>";
    } else {
        echo "<p>No calendars exist!</p>";
    }
    
}

?>
