<?php

require_once('../conf/setup.php');
require_once('../db/Connection.php');
require_once('../db/Query.php');

// Run Query
$connection = new Connection($database,$host,$password,$port,$type,$username);
$handle = $connection->get();
$query = new Query();
$query->set_table('people');
$get_all_people = $query->get_query_by_name('people_get_all');
$statement = $query->prepare_query($handle,$get_all_people);
$statement->execute();
$people = $statement->fetchAll(PDO::FETCH_ASSOC);

//print_r($people);
//die();

$existing_people = "";
$record = "";
$number_people = count($people);
if ($number_people)
{
    $existing_people .= "<div id=\"people\">";
    $existing_people .= "<span class=\"row title_row\">";
    $existing_people .= "<span class=\"cell cell_small first\">ID</span>" .
               "<span class=\"cell\">First Name</span>" .
               "<span class=\"cell cell_small\" style=\"display: none;\">Middle Name</span>" .
               "<span class=\"cell\">Last Name</span>" .
               "<span class=\"cell cell_phone\">Phone Number</span>" .
               "<span class=\"cell\">E-Mail</span>" .
               "<span class=\"cell cell_small\">Send E-Mail</span>" .
               "<span class=\"cell cell_small last\">Send SMS</span>";
    $existing_people .= "</span>";
    $iteration = 0;
    foreach ($people as $person)
    {
      if ($person['email'] == "")
      {
          $email = "-";
          $send_email = "-";
      } else {
          $email = $person['email'];
          $send_email = $person['send_email'];
      }

      if ($person['pnumber'] == "")
      {
          $phone = "-";
          $send_sms = "-";
      } else {
          $phone = $person['pnumber'];
          $send_sms = $person['send_sms'];
      }
      if ($send_email == "")
      {
          $send_email = "-";
      }
      if ($send_sms == "") 
      {
          $send_sms = "-";
      }

      $iteration += 1; 
      if ($iteration == $number_people)
      {
          $record .= "<span class=\"row last_row\">";
      } else {
          $record .= "<span class=\"row\">";
      }
      $record .= "<span id=\"id_" . $person['id'] . "\" class=\"cell cell_small first\">" . 
                 "<a href=\"javascript: edit_person('" . 
                 $person['id'] . "');\">" . $person['id'] . "</a></span>" .
       "<span id=\"fname_" . $person['id'] . "\" class=\"cell\">" . $person['fname'] . "</span>" .
       "<span id=\"mname_" . $person['id'] . "\" class=\"cell cell_small\" style=\"display: none;\">" . $person['mname'] . "</span>" .
       "<span id=\"lname_" . $person['id'] . "\" class=\"cell\">" . $person['lname'] . "</span>" .
       "<span id=\"pnumber_" . $person['id'] . "\" class=\"cell cell_phone\">" . $phone . "</span>" .
       "<span id=\"email_" . $person['id'] . "\" class=\"cell\">" . $email . "</span>" .
       "<span id=\"send_email_" . $person['id'] . "\" class=\"cell cell_small\">" . $send_email . "</span>" .
       "<span id=\"send_sms_" . $person['id'] . "\" class=\"cell cell_small last\">" . $send_sms . "</span>";
                  
        $existing_people .= $record . "</span>";
        $record = "";
    }
    $existing_people .= "</div>";
    echo $existing_people;
} else {
    echo "<p>No people exist!</p>";
}

?>
