<?php

require_once('../conf/setup.php');
require_once('../db/Connection.php');
require_once('../db/Query.php');

// Setup Parameters
$parameters = array(
               ':pnumber'=>$_GET['phone'],
               ':email'=>$_GET['email']);

// Run Query
$connection = new Connection($database,$host,$password,$port,$type,$username);
$handle = $connection->get();
$query = new Query();
$query->set_table('people');
$check_person_exists = $query->get_query_by_name('people_check_person_exists');
$statement = $query->prepare_query($handle,$check_person_exists);
$statement->execute($parameters);
$status = $statement->fetchAll();

if (count($status))
{
    echo('1');
} else {
    echo('0');
}

?>
