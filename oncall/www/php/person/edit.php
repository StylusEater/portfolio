<?php

require_once('../conf/setup.php');
require_once('../db/Connection.php');
require_once('../db/Query.php');

// Setup Parameters
$parameters = array(
               ':id'=>$_POST['id'],
               ':fname'=>$_POST['fname'],
               ':mname'=>$_POST['mname'],
               ':lname'=>$_POST['lname'],
               ':pnumber'=>$_POST['phone'],
               ':email'=>$_POST['email'],
               ':send_email'=>$_POST['send_email'],
               ':send_sms'=>$_POST['send_sms']);

// Run Query
$connection = new Connection($database,$host,$password,$port,$type,$username);
$handle = $connection->get();
$query = new Query();
$query->set_table('people');
$edit_person = $query->get_query_by_name('people_edit_person');
$statement = $query->prepare_query($handle,$edit_person);
$statement->execute($parameters);
$status = $statement->fetchAll();

if (count($status))
{
    return '1';
} else {
    return '0';
}

?>
