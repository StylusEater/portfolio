<?php
require("PDF_USA_Calendar.php");

class Calendar extends PDF_USA_Calendar
{

function printDay($date)
{
    // add logic here to customize a day
    $this->JDtoYMD($date,$year,$month,$day);
    if (array_key_exists($month,$this->oncall_list))
    {
        if (array_key_exists($day,$this->oncall_list[$month]))
        {
            $this->SetXY($this->x, $this->y + $this->squareHeight / 2);
            $this->SetFont("Arial", "B", 10);
            $this->Cell($this->squareWidth,5,$this->oncall_list[$month][$day],0,0,"C");
        }
    }
}

function isHoliday($date)
{
    // insert your favorite holidays here
    $this->JDtoYMD($date, $year, $month, $day);

    // call the base class for USA holidays
    return parent::isHoliday($date);
}

} // class MyCalendar extends PDF_USA_Calendar

?>
