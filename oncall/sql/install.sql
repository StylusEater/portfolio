use oncall;

-- People Table
--
create table if not exists oncall_people (
  id         bigint unsigned AUTO_INCREMENT, 
  fname      varchar(255),
  mname      varchar(255),
  lname      varchar(255),
  pnumber    varchar(255),
  email      varchar(255),
  send_email enum('TRUE','FALSE') default 'TRUE',
  send_sms   enum('TRUE','FALSE') default 'FALSE',
  primary key (id)
) ENGINE=InnoDB DEFAULT CHARSET="UTF8"; 

-- Schedule Table
--
create table if not exists oncall_schedule (
  id        bigint unsigned AUTO_INCREMENT,
  sday      int unsigned,
  eday      int unsigned,
  smonth    enum('january','february','march','april',\
                 'may','june','july','august','september',\
                 'october','november','december'),
  emonth    enum('january','february','march','april',\
                 'may','june','july','august','september',\
                 'october','november','december'),
  syear     int unsigned,
  eyear     int unsigned,
  person_id bigint unsigned,
  pdf_path  varchar(255),
  primary key (id),
  index     (person_id),
  foreign key (person_id) references oncall_people(id) on delete cascade on update cascade
) ENGINE=InnoDB DEFAULT CHARSET="UTF8";
  
