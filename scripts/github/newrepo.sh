# newrepo.sh - setup a new github repository
# 
# Usage: sh newrepo.sh username token newreponame

# Create a new repository on github.com
curl -F login=$1 -F token=$2 https://github.com/api/v2/yaml/repos/create -F name=$3    

mkdir $3
cd $3
git init
touch README
git add README
git commit -m 'New Repository README'
git remote add origin git@github.com:$1/$3.git
git push -u origin master
