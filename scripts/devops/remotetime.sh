#!/bin/bash

OFFSET=$1

clear

while [ 1 ]; do
   #LOCALUTCH=`date '+%H'`
   LOCALUTCH=1
   if [ $LOCALUTCH -lt $OFFSET ]; then
       NEGNEWUTCH=`expr $LOCALUTCH - $OFFSET`
       DELTANEWUTCH=`expr -1 \* $NEGNEWUTCH`
       NEWUTCH=`expr 24 - $DELTANEWUTCH`
   else
       NEWUTCH=`expr $LOCALUTCH - $OFFSET`
   fi
   LOCALUTCM=`date '+%M'`
   LOCALUTCS=`date '+%S'`
   echo "$NEWUTCH:$LOCALUTCM:$LOCALUTCS"
   sleep 2
   clear
done
