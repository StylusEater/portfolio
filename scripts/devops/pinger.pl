#!/usr/bin/perl

use Net::Ping;

my $PREFIX = $ARGV[0];
if (!$PREFIX) {
    print "Please give me a x.x.x prefix.\n";
    exit(-1);
}
my $MAX = 254;
my $BEGIN = 1;
my $icmpObj = Net::Ping->new();

while ($BEGIN < $MAX) {
    print "Attempting to ping " . $PREFIX . "." . $BEGIN . "\n";
    if ($icmpObj->ping($PREFIX . "." . $BEGIN)) {
        print "Host $BEGIN responds.\n"
    } else {
        print "$BEGIN is down.\n";
    }
    $BEGIN++;
}

$icmpObj->close();
