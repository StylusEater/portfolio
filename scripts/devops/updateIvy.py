#!/bin/env python

#############################
#  Copyright Adam M. Dutko  #
#  Created: 03-11-09        #
#  Last Updated: 03-11-09   #
#  License: BSD             #
#############################

from optparse import OptionParser
from os import chdir,getcwd,mkdir,system

from sys import exit

if __name__ == "__main__":
    parser = OptionParser()
    parser.add_option("-s", "--system", dest="system",
                      help="Host system portion of URL.")
    parser.add_option("-d", "--directory", dest="directory",
                      help="Directory portion of URL.")
    parser.add_option("-t", "--target", dest="target",
                      help="Target directory to save remote contents locally.")
    parser.add_option("-b", "--basic", dest="basic", action="store_true", default=False,
                      help="Enable basic authentication mode.")
    parser.add_option("-u", "--user", dest="user",
                      help="User used for HTTP Basic Authentication.")
    parser.add_option("-p", "--password", dest="password",
                      help="Password used for HTTP Basic Authentication.")
    parser.add_option("-r", "--remove", dest="remove", action="store_true", default=False,
                      help="Remove existing local copy.")

    (options, args) = parser.parse_args()

    if options.target == None:
       parser.print_help()
       exit(2)

    # Rename options for easier use
    sourceHost = options.system
    sourceDir = options.directory
    localDir = options.target
    authMode = options.basic
    authUser = options.user
    authPass = options.password
    clean = options.remove
    oldpwd = getcwd()

    # If specified remove the local repository copy
    if clean:
        try:
            system("rm -rf " + localDir)
        except OSError, oe:
            print oe
            exit(2)

    # Try to create the local target dir
    try:
        mkdir(localDir)
    except OSError, oe:
        print oe
        exit(2)
 
    # Get target contents
    wgetTarget = "wget -r -l5 -R index.html -v -A .jar,.zip,.jar.md5,.jar.sha1,.xml,.md5,.sha1 -nH -P " + localDir + " "
    if authMode:
        wgetTarget = wgetTarget + "--http-user " + authUser + " " + "--http-password " + authPass + " "
    try:
        if sourceDir != None:
            system(wgetTarget + sourceHost + sourceDir)
        else:
            system(wgetTarget + sourceHost)
    except OSError, oe:
        print oe
        exit(2)  
