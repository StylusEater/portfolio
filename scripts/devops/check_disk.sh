#!/bin/bash
#
# License: BSD
#
# check_disk  Check a partition for storage usage.
#
#
###############
## CHANGELOG ##
###############
# 02/10/10 - Created - Adam M. Dutko
# 02/11/10 - Added remote run feature. - Adam M. Dutko
#

###########
## USAGE ##
###########
# 
# General Usage when e-mail available
# eg. ./check_disk.shl /u01 80 
#
# Usage when e-mail not available but ssh is...
# eg. ssh venus "/home/adutko/scripts/check_disk.shl /u01 80 1" 

############
## CONFIG ##
############
DEBUG=0   # USEFUL WHEN SETTING THIS SCRIPT UP
HOST="CHANGEME"
PEMAIL="CHANGEME"
LOGFILE=`pwd`/check_disk.log

#################################
## DO NOT EDIT BELOW THIS LINE ##
#################################

## inform the user about the log if we're in DEBUG
if [ $DEBUG == 1 ]; then
    echo "Please check $LOGFILE for errors."
fi

## catalog errors
ERRORS=""

## store remote flag
REMOTE=$3

## ideally this gives us the fqdn
FHOST=`hostname`

## What time did we start?
START="$FHOST - $(date) - Starting disk check..."
if [ $DEBUG == 1 ]; then
    echo $START
fi
echo $START >> $LOGFILE

## should have received the disk from the first param
DISK=$1
if [ ! -n "$DISK" ]; then
    ERRORS="Please give me a disk to check."
    echo -e "$ERRORS" >> $LOGFILE
    exit
fi

## should have received percentage free as the second param
PERCFREE=$2
if [ ! -n "$PERCFREE" ]; then
    ERRORS="$ERRORS \nPlease give me a percentage used threshold."
    echo -e "$ERRORS" >> $LOGFILE
    exit
fi

## check to see if path exists as a valid mount point
VALIDPATH=`df -h | grep $DISK`
if [ ! -n "$VALIDPATH" ]; then
    ERRORS="$ERRORS \n$DISK is not a valid mount point."
    echo -e "$ERRORS" >> $LOGFILE
    exit 
fi

## check results
RESULTS=""

## General parser
# df -h | grep /home | awk -f/ '{ print $1}' gets us...
# 3.9G  137M  3.6G   4% 
# then choose from Size  Used Avail Use% aka $1,$2,$3,$4
# df -h | grep /home | awk -F/ '{ print $0}' | awk '{ print $4 }'
SIZE=`df -h | grep $DISK | awk -F/ '{ print $0 }' | awk '{ print $2 }'`
USED=`df -h | grep $DISK | awk -F/ '{ print $0 }' | awk '{ print $3 }'`
AVAIL=`df -h | grep $DISK | awk -F/ '{ print $0 }' | awk '{ print $4 }'`
USE=`df -h | grep $DISK | awk -F/ '{ print $0 }' | awk '{ print $5 }'`

RESULTS="$DISK is $SIZE, with $USED/$USE used, and $AVAIL available."
echo $RESULTS >> $LOGFILE

## Check percent free against threshold
USESANITIZED=`echo $USE | sed s/%//`
PERCFREESANITIZED=`echo $PERCFREE | sed s/%//`
if [ "$PERCFREESANITIZED" -gt "$USESANITIZED" ]; then
    STOP="$FHOST - $(date) - Stopping disk check."
    echo $STOP >> $LOGFILE
    if [ $DEBUG == 1 ]; then
        echo -e "USED: $USESANITIZED."
        echo -e "FREE: $PERCFREESANITIZED."
        echo $STOP
    fi
    if [ "$REMOTE" == 1 ]; then
        echo -e "$HOST Disk OK :: \n$RESULTS"
    fi
    exit
else
    RESULTS="$RESULTS \n$FHOST is low on space."
    if [ "$REMOTE" == 1 ]; then
        echo -e "$HOST Disk Warning \n$RESULTS"
    else
        echo -e $RESULTS | mail -s "$HOST Disk Warning" $PEMAIL
    fi
    echo "$FHOST - $(date) - DISK threshold hit...sent alert.  Stopping disk check." >> $LOGFILE
    STOP="$FHOST - $(date) - Stopping disk check."
    if [ $DEBUG == 1 ]; then
        echo -e $RESULTS
        echo $STOP
    fi
    exit
fi
