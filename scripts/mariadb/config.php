<?php

// AUTHOR: Adam M. Dutko
// EMAIL: dutko {dot} adam -at- gmail
// LICENSE: GPL v3 or later

if (phpversion() >= 5.3) { 
    $hostname = gethostname();
} else {
    $hostname = php_uname('n');
}

// USER DEFAULTS
//
$defURL = "http://askmonty.org/buildbot/buildslaves";
$defBot = "adutko-ultrasparc3";

// MAIL SETTINGS
$mail    = 1; // turn mail on 1 or off 0
$to      = 'CHANGEME';
$subject = 'Buildbot Status';
$headers = 'From: buildbot@' . $hostname . "\r\n" .
           'Reply-To: buildbot@' . $hostname . "\r\n" .
           'X-Mailer: PHP/' . phpversion(); 

$DEBUG = 0;

if ($to == "CHANGEME") {
    echo "ERROR: Please set \$to in config.php so mail works.\n";
    exit();
}

?>
