#!/usr/bin/php  
<?php

// AUTHOR: Adam M. Dutko
// EMAIL: dutko {dot} adam -at- gmail
// LICENSE: GPL v3 or later

// NOTES: 
// Need php5-curl on Debian
// Some code adopted from http://www.merchantos.com/makebeta/php/scraping-links-with-php/

// Bring in the user defaults from config.php
require('config.php');

$error = 0;
$allbots = 0;
if (count($argv) >= 2) {
    $argOne = trim($argv[1]);
} else {
    $argOne = NULL;
}

function usage() {
    echo "Welcome to the MariaDB BuildBot Checker: \n";
    echo "\n";
    echo "   usage: \n";
    echo "          php checkStatus.php (uses defaults)\n";
    echo "          php checkStatus.php \${URL} \${BUILDBOT} (show a bots info)\n";
    echo "          php checkStatus.php \${URL} all (show all bots)\n";
    echo "\n";
    echo "   note: \n";
    echo "          A local file can be used in place of a URL.\n";
    echo "\n";
}

// What to do for various menu selections
if ($argc == "1") {
    $defaults = 1;
} else {
    $defaults = 0;
}

if ($argOne == "-h") {
    usage();
    exit();
}

if ($argOne == "--help") {
    usage();
    exit();
}

if ($argOne == "all") {
    usage();
    exit();
}

if ($DEBUG) {
    var_dump($argv);
}

// Use the defaults or user selected information
if ($defaults == 1) {
    $target_url = $defURL;
    $target_bb = $defBot;
} else {
    $target_url = $argv[1];
    $target_bb = $argv[2];
}

// Show all buildbots
if ($target_bb == "all") {
    $allbots = 1;
}

$userAgent = 'CheckBot/0.1 (http://litlehat.homelinux.org)';

// make the cURL request to $target_url
$ch = curl_init();
curl_setopt($ch, CURLOPT_USERAGENT, $userAgent);
curl_setopt($ch, CURLOPT_URL,$target_url);
curl_setopt($ch, CURLOPT_FAILONERROR, true);
curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
curl_setopt($ch, CURLOPT_AUTOREFERER, true);
curl_setopt($ch, CURLOPT_RETURNTRANSFER,true);
curl_setopt($ch, CURLOPT_TIMEOUT, 30);
$html= curl_exec($ch);
if (!$html) {
        echo "<br />cURL error number: " . curl_errno($ch) . "\n";
        echo "<br />cURL error: " . curl_error($ch) . "\n";
        exit;
}

// parse the html into a DOMDocument
$dom = new DOMDocument();
$dom->preserveWhiteSpace = false;
@$dom->loadHTML($html);

// grab all the on the page
$xpath = new DOMXPath($dom);
// Get the table that holds our information
//$buildbotnames = $xpath->getElementsByTagName('body')->item(0);
print_r($dom);
exit();
$buildbotitems = $dom->getElementsByTagName("tr");
// The first table row is headings

// Each subsquent table row is buildbot information
$validbuildbot = array();

// create ordered array of bots
$buildbots = array();
foreach ($buildbotnames as $buildbotname) {
    array_push($buildbots,$buildbotname->nodeValue);
}

if ($DEBUG || $allbots) {
    echo "All buildbots ... \n";
    foreach ($buildbotnames as $buildbotname) {
        echo "FOUND BOT NAME --> " . $buildbotname->nodeValue . "\n";
    }
    
    if ($allbots == 1) {
        echo "Exiting...\n"; 
        exit;
    }
}

$buildbotcnt = 0;
foreach ($buildbotitems as $buildbotitem) {
    $buildbotInfo = $buildbotitem->nodeValue;
    $info = split("\n",$buildbotInfo);

    // Get rid of wasted last line
    //array_pop($info); 

    // cleanup first line
    $fInfoLine = split("  ",$info[0]);

    // put "new" elements in front
    $ofni = array_reverse($info);
    array_push($ofni,$fInfoLine[0],$fInfoLine[1]);
    $info = array_reverse($ofni);

    // get rid of duplicate info 
    $front = array_slice($info, 0, 1);
    $back = array_slice($info, 3); 
    $info = array_merge($front,$back);

    // filter on size 
    $sizeInfo = count($info);
    if ($sizeInfo != (3 || 5)) {
        print "Array size is ... " . $sizeInfo . "\n";
        continue;
    }

    if ($DEBUG) {
        print "INFO --> " . $buildbotInfo . "\n";
        print "SIZE --> " . $sizeInfo . "\n";
    }

    $validbuildbots[$buildbots[$buildbotcnt]] = $info;
    $buildbotcnt++;
}

if ($target_bb != 1) {
    $bb = $validbuildbots[$target_bb];
    $subject = $target_bb . " Status";
}

if (count($bb) == 3) {
    $message = $target_bb . ": \n" .
               "---------------\n" .
               trim($bb[0]) . "\n" .
               trim($bb[1]) . "\n" .
               trim($bb[2]) . "\n" .
               "Please reestablish a connection.\n";    
    $error = 1;
}

if (count($bb) == 5) {
    $message = $target_bb . ": \n" .
               "---------------\n" .
               trim($bb[0]) . "\n" .
               trim($bb[1]) . "\n" .
               trim($bb[2]) . "\n" .
               trim($bb[3]) . "\n" .
               trim($bb[4]) . "\n";
}

if (count($bb) == 0) {
    $message = "Sorry but " . $target_bb . " isn't a valid buildbot.\n";
    $error = 1;
}

if ( ($mail == 1) && ($error == 1) ) {
    mail($to,$subject,$message,$headers);
} else {
    //echo $message; 
    exit();
}

?>
